/** \brief  Реализация CVoiceMailService::main()
 * CVoiceMailService_main.cpp
 *  Created on: 30.09.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */

#include <time.h>
#include <winsock2.h>
#include <windows.h>

#include "define.h"
#include "CVoiceMailService.h"
#include "CService_Call.h"
#include "utils/netutils.h"
#include "utils/xml/iniFileXML.h"

namespace AMT {

//calcAlarmTime
int calcAlarmTime(int _daysForward, int _secondsStartTime){
  //вышли за пределы времени, взвести время на след день
  time_t rawtime; struct tm * timeinfo;
  time ( &rawtime ); timeinfo = localtime ( &rawtime );
  int secondsSinceLastMidnight =
        timeinfo->tm_hour * 3600
      + timeinfo->tm_min * 60
      + timeinfo->tm_sec;

  //завести будильник на завтра
  return time(NULL) - secondsSinceLastMidnight + (_daysForward * 3600 * 24)
                                               + _secondsStartTime;
}

//isHolyDay
bool isHolyday(){
  time_t rawtime; struct tm *timeinfo;
  time ( &rawtime );
  timeinfo = localtime ( &rawtime );
  //LOG1(INFO, "The current date/time is: %s", asctime (timeinfo) );

  return ( (timeinfo->tm_wday == 7)
         ||(timeinfo->tm_wday == 0));
}

void CVoiceMailService::addBusyNumber(const string& _n){
  LOCK(&lock);
  listOfBusyNumbers.push_back(_n);
  UNLOCK(&lock);
}

void CVoiceMailService::remBusyNumber(const string& _n){
  LOCK(&lock);

  list<std::string>::iterator it_n = listOfBusyNumbers.begin();
  for(; it_n != listOfBusyNumbers.end(); it_n ++){
    if(*it_n == _n){
      listOfBusyNumbers.erase(it_n);
      break;
    }
  }
  UNLOCK(&lock);
}

bool CVoiceMailService::isBusyNumber(const string& _n){
  LOCK(&lock);
  bool res = false;

  list<std::string>::iterator it_n = listOfBusyNumbers.begin();
  for(; it_n != listOfBusyNumbers.end(); it_n ++){
    if(*it_n == _n){
      res = true;
    }
  }
  UNLOCK(&lock);
  return res;
}

//Основная потоковая функция
void CVoiceMailService::main(){
  //ждем всех
  pthread_barrier_wait(&start_barier);

  struct timespec abstime;
  abstime.tv_sec = 0;
  abstime.tv_nsec = 0;

  //не в режиме
  Sleep(5000);
  //первичный взвод таймера, распарсить время hh:mm в секунды
  int hours, minutes;

  while(1){

    //перечитать параметры уведомлений
    loadParams(1);

    //время начала оповещения
    sscanf(notifying_startTime.c_str(), "%d:%d", &hours, &minutes);
    int secondsStartTime = hours * (60*60) + minutes * 60 + 0;

    //время конца оповещения
    sscanf(notifying_stopTime.c_str(), "%d:%d", &hours, &minutes);
    int secondsStopTime = hours * (60*60) + minutes * 60 + 0;

    //интервал оповещения
    int secondsInterval = notifying_interval * 60; //в сек

    if(abstime.tv_sec == 0){
      //первый вызов
      int firstTimeCall = calcAlarmTime(0, secondsStartTime);
      if(firstTimeCall < time(NULL) ){
        //попали между стартом и стопом
        abstime.tv_sec = time(NULL);// + secondsInterval;
      }else
        abstime.tv_sec = firstTimeCall;
    }else{
      //взвести след интервал
        abstime.tv_sec = time(NULL) + secondsInterval;
    }

    //таймер оповещения
    //проверяем только в будние дни
    if(abstime.tv_sec > calcAlarmTime(0, secondsStopTime) ){
      //след интервал выйдет за пределы stopTime, взвести время на след день
      abstime.tv_sec = calcAlarmTime(1, secondsStartTime);
    }

    LOCK( &lock );
    //ждем сигнал или таймер
    int res = WAITSIGNALTIMED(&cond, &lock, &abstime);
    UNLOCK( &lock );
    if(res == ETIMEDOUT){

      if(isHolyday()){
        //выходные, перезвести на след день
        abstime.tv_sec = calcAlarmTime(1, secondsStartTime);
        continue;
      }

      if(notifyMode == 0)
        continue;

      CString str_notify(typeRec);//typeNotify

      //Последовательно обзвонить всех
      WIN32_FIND_DATA find_data;
      HANDLE hFind;

      hFind = FindFirstFile("abonents\\*", &find_data);

      if (hFind == INVALID_HANDLE_VALUE){
        LOG1(ERRR, "VMS:FindFirstFile failed, %d\n", (int)GetLastError());
        continue;
      }

      do{
        //проверяем только директории
        if(find_data.cFileName[0] == '.')
          continue;
        //            if(FindFileData.dwFileAttributes == 0)
        //              continue;
        //
        //
        CString str_filename = "abonents/";
        str_filename += find_data.cFileName; str_filename += "/abonent.xml";
        LOG1(TSTL0, "VMS:main abo filename %s\n", str_filename.c_str());

        CIniFileXML abo(str_filename);
        if(false == abo.ReadFile(10)){
          continue;
        }
        IP s_ip = abo.GetAttribute("Abonent", "sourceIP", "192.168.233.12");
        string n_digi = abo.GetAttribute("Abonent", "number", "000");
        if(n_digi == "000")
          continue;

        string str, source;
        int abo_mess = abo.GetFirstNotReadMessage(n_digi, source, str_filename);
        if(0 != abo_mess){
          //нет новых номеров обзванивать
          continue;
        }

        if(isBusyNumber(n_digi)){
          //этот номер занят иным потоком
          continue;
        }

        //есть не прочитанное сообщение - инициировать оповещение
        //1. найти свободный CService_Call
        for(int i = 0; i<poolCount; i++){
          if(pool_ofCall_in[i]->GetStatus() == CService_Call::_idle){
            //нашли! - сделать исх вызов, сценарий - уведомить
						LOG2(INFO, "VMS:Call To Abonent %s on %s\n", n_digi.c_str(), s_ip.toString(str).c_str());
						LOGTIME(INFO);
            pool_ofCall_in[i]->Setup(str_notify, n_digi, s_ip);
            Sleep(1000); //чтобы все сигналы дошли!
            break;
          }
        }

      }while(FindNextFile(hFind, &find_data) ) ;

      FindClose(hFind);

    }else if(res == EOK){
      //..ответить!
      REPLYandUNLOCK(&cond, &lock);
    }else{
      LOG(ERRR, "VMS:main:pthread_cond_timedwait\n");
      Sleep(1000);
    }

  }//while()

}


}
