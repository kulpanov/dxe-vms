/** \brief Основной класс сервера.
 * CVoiceMailService.h
 * Описание основных классов сервиса.
 *  Created on: 11.04.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */

#ifndef SMAINDATA_H_
#define SMAINDATA_H_

#include <boost/thread.hpp>
#include <crypter.h>
#include <map>
#include <string>

using boost::unique_lock;

#ifndef HAVE_REMOTE
#define HAVE_REMOTE
#endif
#include <winsock2.h>

#include <pthread.h>


#include "define.h"
#include "utils/netutils.h"
#include "sdl_sys.h"
//#include "CService_Call_in.h"
#include "utils/xml/iniFileXML.h"

///{@ Вспмогательные макросы синхринизации
///Залокировать объект
#define LOCK(lock)      pthread_mutex_lock(lock)
///Разлокировать объект
#define UNLOCK(lock)    pthread_mutex_unlock(lock)

///Послать сигнал через condvar
#define SIGNAL(cond)    pthread_cond_signal(cond)
///Заблокировать поток в ожидании ответа на сигнала
#define WAITREPLY(cond, lock) pthread_cond_wait(cond, lock)
///Послать сигнал через condvar и ждать ответа
#define SIGNALandWAITREPLY(cond, lock)    SIGNAL(cond);WAITREPLY(cond, lock);

///Заблокировать поток в ожидании сигнала
#define WAITSIGNAL(cond, lock) pthread_cond_wait(cond, lock)
///Заблокировать поток в ожидании сигнала + timeout
#define WAITSIGNALTIMED(cond, lock, abstime) pthread_cond_timedwait(cond, lock, abstime)
///Заблокировать поток в ожидании сигнала
#define LOCKandWAITSIGNAL(cond, lock) LOCK(lock); WAITSIGNAL(cond, lock)


///Оветить на сигнал
#define REPLY(cond)     pthread_cond_signal(cond)
///Оветить на сигнал
#define REPLYandUNLOCK(cond, lock)    REPLY(cond); UNLOCK(lock);

///@}

///Пространство имён AMT
namespace AMT {

class CService_Call;


/** \brief CVMS_system - класс работы с SDL-системой.
 *Предок берётся из библиотеки SDL.
 *Класс не переписывает каких либо методов.
 */
class CVMS_system: public SDL::CSDL_system {
typedef SDL::CSDL_system base;
public:
  virtual void addListener(const SDL::CSDL_Signal *_sig){
    base::addListener(_sig);
  }

public:
  CVMS_system():base(){};
};

/** \brief CVoiceMailService - Диспетчер соединений.
 * Основной класс работы сервиса голосовых сообщений.
 */
class CVoiceMailService {
protected:
  ///Структура инициализации сокетов
  WSADATA lpWSAData;

  ///Основной объект работы с SDL-системой
  CVMS_system sdl_vms;
public:
  //принимающий SDL-сообщения в vms
  void* defReceiver;
//  /// \deprecated - после проверки код надо удалить
//  ///Вспомогательная карта входящих соединений
//  map<std::string, std::string> inviteMaps;
//  ///Вспомогательная карта адресов входящих брикетов
//  map<std::string, std::string> inviteGW;
//
//  ///Вспомогательная карта входящих соединений
//  map<int, int> invitePorts;
//  ///Вспомогательная карта адресов входящих брикетов
//  map<int, int> inviteGWs;
  bool service_started;
  int err_log_level;
public:
  ///мутекс синхринизации
  pthread_mutex_t lock;
  ///condvar синхринизации
  pthread_cond_t cond;
  ///стартовый барьер для потоков
  pthread_barrier_t start_barier;
protected:
  ///XML-файл базы абонетов
  CIniFileXML db_abonents;
  ///список номеров, что сейчас заняты
  list<std::string> listOfBusyNumbers;
  CCrypter crypt;
public:
  void addBusyNumber(const string& _n);
  void remBusyNumber(const string& _n);
  bool isBusyNumber(const string& _n);
public:
  ///IP адрес компьютера
  IP myIP;
  ///Подсеть компьютера
  IP myNET;

  ///иницилизирующий SDL-сигнал
  SDL::CSDL_Signal_InitIP* initIP;

  ///порт для ввода SIP-сообщений
  IP_PORT sipPort_in;
  ///порт для вывода SIP-сообщений
  IP_PORT sipPort_out;
  ///Нижняя граница портов RTP-потоков
  IP_PORT lowBounderOfRTP;

  ///Макс кол-во аб. ящиков
  int maxMailBoxes;
  ///Макс время записи сообщения
  int maxRecordMsg;
  ///Макс кол-во записей сообщения
  int maxCountMsg;
  ///Наборный номер "Запись"
  int typeRec;
  ///Наборный номер "Воспроизведение"
  int typePlay;
  ///Наборный номер "Удалить прослушенные"
  int typeDelete;
  ///Наборный номер "Удалить всё"
  int typeClear;
  ///Наборный номер "Уведомить" -
  //int typeNotify;
public:
  ///Короткий тональный сигнал
  tVoiceMsg voice_msg_OnConnect;
  ///Короткий тональный сигнал
  tVoiceMsg voice_msg_Beep;
  ///Сообщение перед "Удалить всё"
  tVoiceMsg voice_msg_BeforeClear;
  ///Сообщение перед "Удалить прослушенные"
  tVoiceMsg voice_msg_BeforeDelete;
  ///Сообщение перед  "Запись"
  tVoiceMsg voice_msg_BeforeRecord;
  ///Сообщение перед отказом в обслуживании
  tVoiceMsg voice_msg_BeforeReject;
  ///Сообщение "След сообщение"
  tVoiceMsg voice_msg_NextMessage;
  ///Сообщение о том, что ящик пуст
  tVoiceMsg voice_msg_Empty;
  ///Запрос на удалить
  tVoiceMsg voice_msg_AfterPlay;
  ///Запрос "Уведомить о непрочитанных"
  tVoiceMsg voice_msg_Notify;
  ///Тишина!
  tVoiceMsg voice_msg_Silence;
  ///Запрос на отмену уведомлений
  tVoiceMsg voice_msg_CancelNotification;
public:
  //оповещения
  int notifyMode;
  ///Время начала - смещение от нчала суток
  string notifying_startTime;
  ///Время конца - смещение от нчала суток
  string notifying_stopTime;
  ///Интервал
  int notifying_interval;
  ///Продолжительность попытки
  int notifying_callTime;

public:
  ///выходной сокет для SIP
  int socket_sipudp_out;
  ///структура настройки сокета
  struct sockaddr_in sip_other;
public:
  ///Число потоков пула соединений
  int poolCount;
  ///Число потоков пула соединений
#define POOL_COUNT 15//254
  ///Пул потоков соединений, статичный
  CService_Call* pool_ofCall_in[POOL_COUNT];
protected:
  ///map
  std::map<std::string, boost::mutex> numbersMutexes;
  boost::mutex mutex_NumbersMutexes;

  class ScopedLockNumber: public boost::mutex::scoped_lock{
  	string number;
  public:
  	ScopedLockNumber(ScopedLockNumber&& other):
  		boost::mutex::scoped_lock(boost::move(*this)), number(boost::move(other.number))
  	{	}

  	ScopedLockNumber(boost::mutex& _mt, const std::string& _number):
  		boost::mutex::scoped_lock(_mt), number(_number)
  	{
  		LOG1(TSTL1, "Number %s: is locked\n", number.c_str());
  	}
  	~ScopedLockNumber(){
  		LOG1(TSTL1, "Number %s: is unlocked\n", number.c_str());
  	}
  };
public:
  ScopedLockNumber getNumberLocker(const std::string& _number){
  	boost::mutex::scoped_lock locker(mutex_NumbersMutexes);
  	boost::mutex& mt = numbersMutexes[_number];
  	return ScopedLockNumber(mt, _number);
  }

public:

  /** \brief Установить RTP порт входящего потока.
   * vms передаёт данные сигнали потокам - обработчикам соединения.
   * @param _id  - номер соединения
   * @param _rtp_in - RTP-порт для входящих соединений
   */
  void setRTP_in(int _id, int _rtp_in);

  /** \brief Установить RTP порт назначения исходящего потока.
   * vms передаёт данные сигнали потокам - обработчикам соединения.
   * @param _id  - номер соединения
   * @param _d_rtp - RTP-порт для исходящих соединений
   * @param _d_ip - IP для исходящих соединений
   */
  void setRTP_dst(int _id, int _d_rtp, int _d_ip);

  /** \brief Задать входящий SIP-INFO сигнал.
   * Выполнить обработку SIP INFO сигнала, приходящего после установки соед.
   * vms передаёт данные сигнали потокам - обработчикам соединения.
   * @param _id номер соединения
   * @param _data1 данные
   * @param _data2 данные
   * @param _data3 данные
   */
  void putSIP_Info(int _id, int _data1, int _data2, int _data3);

public:
  ///Загрузить параметры
  void loadParams(int type = 0);

  ///Сохранить параметры
  void saveParams();
public:
  /** \brief Старт работы системы
   * @param _opts - опции запуска, не используется
   */
  void start(std::string _opts);

  /** \brief Послать SDL-сигнал в библиотеку SDL/
   * @param _sig - сигнал
   */
  void send(SDL::CSDL_Signal* _sig){
    _sig->SetReceiver(defReceiver);

    std::string s;_sig->toString(s);
    LOG1(INFO, "VMS:%s\n", s.c_str());
    sdl_vms.send(_sig);
  };
public:
  ///поток слушающий SIPсообщения
  pthread_t sdl_listener;
protected:
  ///thread_SIP_listener
  static void* thread_SDL_listener(void* _vms){
    try{
      CVoiceMailService* vmservice = (CVoiceMailService*)_vms;
      vmservice->SDL_listener();
    }catch(P_EXC e){
    	E_CATCH2(e);
    }
    Exit(1);
    return NULL;
  }
  ///Метод-слушатель входящих SDL-сигналов
  void* SDL_listener();

public:
  ///поток слушающий SIPсообщения
  pthread_t sip_listener;
protected:
  ///thread_SIP_listener
  static void* thread_SIP_listener(void* _vms){
    try{
      CVoiceMailService* vmservice = (CVoiceMailService*)_vms;
      vmservice->SIP_listener();
    }catch(P_EXC e){
    	E_CATCH2(e);
    }
    Exit(1);
    return NULL;
  }
  ///Метод-слушатель SIP-сообщений
  void* SIP_listener();

public:
  ///thread_main
  static void* thread_main(void* _vms){
    try{
      CVoiceMailService* vmservice = (CVoiceMailService*)_vms;
      vmservice->main();
    }catch(P_EXC e){
    	E_CATCH2(e);
    }
    Exit(1);
    return NULL;
  }
  /** \brief Основная функция потока VMS
   *
   */
  void main();

public:
  /** \brief Invite - обработчик SDL-сигнала invite
   * @param Id - номер соединения
   * @param strD - номер сценария
   * @param strN - номер абонента
   * @param _s_ip - источник вызова, ip-адрес
   * @return  */
  void Invite(int Id, const std::string& strD, const std::string& strN,  const uint32_t _s_ip
      , const std::string& _strFNumber );

  /**\brief Release - обработчик SDL-сигнала release
   * @param Id  - номер соединения
   */
  void Release(int Id);

  /**\brief Connect - обработчик SDL-сигнала connect
   * @param Id  - номер соединения   */
  void Connect(int Id);

protected:
  ///конструктор
  CVoiceMailService();
public:
  ///выход из программы
  static void Exit(int code);
  ///make-функция
  static CVoiceMailService& Create();
  ///деструктор
  virtual ~CVoiceMailService();

};

}

#endif /* SMAINDATA_H_ */
