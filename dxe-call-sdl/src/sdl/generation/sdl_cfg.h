
/* Program generated by Cadvanced 4.5.0  */
#define XSCT_CADVANCED

/* "sdl_cfg.h" file generated for system VMS_SDL_system */

#ifndef XUSE_GENERIC_FUNC
#define XUSE_GENERIC_FUNC
#endif
#define XNOUSE_OPFUNCS

#ifndef XNOUSE_AUTOMATIC_OPERATOR_CFG
/* Predefined operator configuration */
#define XNOUSE_ANY
#define XNOUSE_OWN_GENERATOR
#define XNOUSE_LSTRING_GENERATOR
#define XNOUSE_GARRAY_GENERATOR
#define XNOUSE_POWERSET_GENERATOR
#define XNOUSE_GPOWERSET_GENERATOR
#define XNOUSE_BAG_GENERATOR
#define XNOUSE_UNION
#define XNOUSE_UNIONC
#define XNOUSE_MODIFY_CHARSTRING
#define XNOUSE_BITSTR_OCTET
#define XNOUSE_HEXSTR_OCTET
#define XNOUSE_MODIFY_OCTET
#define XNOUSE_FIRST_BIT_STRING
#define XNOUSE_LAST_BIT_STRING
#define XNOUSE_HEXSTR_BIT_STRING
#define XNOUSE_NOT_BIT_STRING
#define XNOUSE_AND_BIT_STRING
#define XNOUSE_OR_BIT_STRING
#define XNOUSE_XOR_BIT_STRING
#define XNOUSE_IMPL_BIT_STRING
#define XNOUSE_BITSTR_OCTET_STRING
#define XNOUSE_MKSTRING_OBJECT_IDENTIFIER
#define XNOUSE_LENGTH_OBJECT_IDENTIFIER
#define XNOUSE_FIRST_OBJECT_IDENTIFIER
#define XNOUSE_LAST_OBJECT_IDENTIFIER
#define XNOUSE_CONCAT_OBJECT_IDENTIFIER
#define XNOUSE_SUBSTRING_OBJECT_IDENTIFIER
#define XNOUSE_EMPTYSTRING_OBJECT_IDENTIFIER
#define XNOUSE_EXTRACT_OBJECT_IDENTIFIER
#define XNOUSE_MODIFY_OBJECT_IDENTIFIER
#define XNOUSE_EQ_OBJECT_IDENTIFIER
#define XNOUSE_ASSIGN_OBJECT_IDENTIFIER
#define XNOUSE_FREE_OBJECT_IDENTIFIER
#endif
/* NOT #define XMK_USE_HUGE_TRANSITIONTABLES */

