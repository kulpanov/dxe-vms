/** \brief Реализация методов CService_Call_in обработки входящих звонков и сценариев.
 * callIn_scripts.cpp
 *
 *  Created on: 14.04.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */
#include <time.h>
#include <vector>
#include <string>

#include "define.h"
#include "vms_signals.h"
#include "CService_Call.h"
#include <windows.h>

namespace AMT {
using namespace std;

void CService_Call::processCallIn(const CString& _digiD
                                , const CString& _digiN
                                , const IP& _s_ip) {

  int digiD = atoi(_digiD.c_str());

  if ((digiD == vms.typeRec) || (digiD == vms.typePlay) || (digiD == vms.typeDelete)
      || (digiD == vms.typeClear)) {
    //...
  } else {
    LOG2(WARN, "VMS:processCallIn(%d):the unknown scenario:%d\n", id, digiD);
    return; //неизвестный сценарий, игнорировать
  }
  CString dpath = "abonents/" + _digiN;
  CString fpath = dpath + "/abonent.xml";
  //по типу сценария - проверить по XML доступность абонента _digiD на операцию _digiN
  if (digiD == vms.typeRec)
    try {
      //"Записать"
     // bool newFile = false;
      { //открыть/создать ящик абоненту
        FILE* fabo = fopen(fpath.c_str(), "r");
        if (NULL == fabo) {
          //нету файла, создать каталог и файл
          if (getCountOfFilesInDir("abonents\\*") >= vms.maxMailBoxes) {
            //превысили макс колво ящиков
            LOG2(ERRR, "VMS:BeforeCall(%d):%s: exceeded the max number of mail boxes\n",
                id, _digiN.c_str());
            throw E(-1, "see prev log items"); //выйти со сценарием "игнорировать"
          }

          if (0 != mkdir(dpath.c_str())) {
            //не могу создать каталог
            int err = errno;
            LOG3(ERRR, "VMS:BeforeCall(%d):%s: unable to create dir, reason %d\n", id,
                dpath.c_str(), err);
            throw E(-1, "see prev log items");
          }

          fabo = fopen(string(dpath + "/abonent.xml").c_str(), "w");
          if (NULL == fabo) {
            //не могу прочитать файл
            int err = errno;
            LOG3(ERRR, "VMS:BeforeCall(%d):%s: unable to open file, reason %d\n", id,
                _digiN.c_str(), err);
            throw E(-1, "see prev log items");
          }LOG2(INFO, "VMS:BeforeCall(%d):%s: created succesfully\n", id, dpath.c_str());

          string str;
          //Записать "шаблон ящика"
          fprintf(fabo, "<?xml version=\"1.0\"?>\n");
          fprintf(fabo, "<Base name=\"Mail\">\n");
          fprintf(fabo,
              "\t<Abonent mail=\"1\" number=\"%s\" name=\"abonent\" sourceIP=\"%s\" />\n",
              _digiN.c_str(), _s_ip.toString(str).c_str());
          fprintf(fabo, "</Base>\n");
//          newFile = true;
        }
        fclose(fabo);
      }
      {
      CIniFileXML db(dpath + "/abonent.xml");
      if (false == db.ReadFile(10)) {
        //не могу прочитать файл
        int err = errno;
        LOG3(ERRR, "VMS:BeforeCall(%d):%s: unable to read file, reason %d\n", id,
            _digiN.c_str(), err);
        throw E(-1, "see prev log items"); //выйти со сценарием "игнорировать"
      }
      }
    } catch (P_EXC e) {
      E_CATCH(e);
      //не смогли создать новую запись
      return;
    }

  try {
	rtp_out = 0;
    //входящее соединение - разрешить
    CVMS_Signal_Connect_out allow(id);
    vms.send(&allow);

    //запись, чтение, сценарий отказа, ...
    BeforeCall(_digiN);
    {
      string str;
      LOG4(3, "VMS:Call_in(%d):gw=%s, rtp_out=%d, rtp_in=%d\n", id,
          rtp_gw.toString(str).c_str(), rtp_out, rtp_in);

      LOG3(3, "VMS:Call_in(%d):digiD=%s, digiN=%s\n", id, _digiD.c_str(), _digiN.c_str());
    }
    //сообщить, что соед установили
    playVoiceMsg(vms.voice_msg_OnConnect);

    if (vms.typeRec == digiD) {
      //запись
      record_script(_digiN, _s_ip);
    } else if (vms.typePlay == digiD) {
      //прослушка
      play_script(_digiN);
    } else if (vms.typeDelete == digiD) {
      //Удалить прослушенные
      delete_script(_digiN);
    } else if (vms.typeClear == digiD) {
      //очистить
      clear_script(_digiN);
    } else {
      //сценарий: игнорировать
      LOG1(WARN, "VMS:Invite(%d):ignore\n", id);
      //doEndOfCall();//отбой, обход проблемы зависания брикета
    }
  } catch (P_EXC e) {
    E_CATCH(e);
    //искл ситуация, отбой
  }

  doEndOfCall();

  AfterCall(_digiN);
}

//удалить
void CService_Call::delete_script(const CString& digiN)
{
//  //разрешение соединения
//  SDL::CSDL_Signal_in_connect allow(id);
//  vms.send(&allow);

  LOG1(TSTL1, "VMS:deletePlayedMessages(%d): started\n", id);
  Sleep(1000);
  //шаг 1: проиграть сообщение - уведомление о удалении
  int next_Step = playVoiceMsg(vms.voice_msg_BeforeDelete);
  if (0 > next_Step) {
    //doEndOfCall();
    return ;
  };
//  next_Step = playVoiceMsg(vms.voice_msg_Beep);
//  if (0 > next_Step) {
//    //doEndOfCall();
//    return ;
//  };
  //шаг 2: удалить прослушанные сообщения
//  LOCK(&lock);
  try{
  	auto locker = vms.getNumberLocker(digiN);
    CIniFileXML abo("abonents/" + digiN + "/abonent.xml");
    if(true == abo.ReadFile(10)){
      vector<string> files;
      abo.DeleteAllHasReadMessages(digiN, files);
      for(vector<string>::iterator it = files.begin(); it != files.end(); it++){
        remove((*it).c_str());
      }
      files.clear();
      abo.WriteFile(10);
    }
  }catch(...){
    LOG1(ERRR, "VMS:deletePlayedMessages(%d):unknown exception\n", id);
  }
//  UNLOCK(&lock);
  //конец звонка
  //doEndOfCall();
  LOG1(TSTL1, "VMS:deletePlayedMessages(%d): finished\n", id);
}

//удалить все
void CService_Call::clear_script(const CString& digiN)
{
//  //разрешение соединения
//  SDL::CSDL_Signal_in_connect allow(id);
//  vms.send(&allow);

  LOG1(TSTL1, "VMS:clearAll(%d): started\n", id);
  Sleep(1000);
  //шаг 1: проиграть сообщение - уведомление о очистке
  int next_Step = playVoiceMsg(vms.voice_msg_BeforeClear);
  if (0 > next_Step) {
    //doEndOfCall();
    return ;
  };
  next_Step = playVoiceMsg(vms.voice_msg_Beep);
  if (0 > next_Step) {
    //doEndOfCall();
    return ;
  };
  //шаг 2: удалить все сообщения
//  LOCK(&lock);
  {
  	auto locker = vms.getNumberLocker(digiN);
  	CIniFileXML abo("abonents/" + digiN + "/abonent.xml");
  	if(true == abo.ReadFile(10)){
  		vector<string> files;
  		abo.DeleteAllMessages(digiN, files);
  		for(vector<string>::iterator it = files.begin(); it != files.end(); it++){
  			remove((*it).c_str());
  		}
  		files.clear();
  		abo.WriteFile(10);
  	}
  }
 // UNLOCK(&lock);
  //конец звонка
  //doEndOfCall();
  LOG1(TSTL1, "VMS:clearAll(%d): finished\n", id);

}

//отказ
void CService_Call::release_script() {
  LOG1(TSTL1, "VMS:release_script(%d): started\n", id);

  playVoiceMsg(vms.voice_msg_BeforeReject);
  //конец звонка
  //doEndOfCall();

  LOG1(TSTL1, "VMS:release_script(%d): finished\n", id);
  return ;
}

//Сценарий "Запись"
void CService_Call::record_script(const CString& digiN, const IP& _s_ip) {
  LOG1(TSTL1, "VMS:record_script(%d): started\n", id);
  //разрешение соединения
//  SDL::CSDL_Signal_in_connect allow(id);
//  vms.send(&allow);

  //перед запиcью - зачистить
  LOCK(&lock);
  //LOG1(2, "VMS: q_rtp_in: %d\n", q_rtp_in.size());
  //if(q_rtp_in.size() > 0)
    tVoiceMsg::Clear_qmsg(q_rtp_in);
  UNLOCK(&lock);

  LOG1(TSTL1, "VMS:record_script(%d):Step1: msg before record\n", id);
  int next_Step;
  //шаг 1: проиграть сообщение - приглашение к записи
  next_Step = playVoiceMsg(vms.voice_msg_BeforeRecord);
  if (0 > next_Step) {
    //doEndOfCall();
    return ;
  };

//  next_Step = playVoiceMsg(vms.voice_msg_Beep);
//  if (0 > next_Step) {
//    //doEndOfCall();
//    return ;
//  };
  //шаг 2: записать звонок
  LOG1(TSTL1, "VMS:record_script(%d):Step2: record message\n", id);
  next_Step = recordCallMsg();
  //конец звонка
  //doEndOfCall();
  q_rtp_in.removeDuplicates();

  //Шаг3: сохранить содержимое очереди
  LOG2(TSTL1, "VMS:record_script(%d):Step3: saving, rec size %d\n", id, q_rtp_in.size());
//  LOCK(&lock);

  if(q_rtp_in.size()<60){
    //проверка на мин записанное сообщение: менее 60*60мс
    LOG2(INFO, "VMS:record_script(%d):Step3: rec size is too small(%d)\n", id, q_rtp_in.size());
  }else
  {//сохранить запись
    string fileName;
    tVoiceMsg::Get_uniqueName("abonents/" + digiN, fileName);
    LOG2(TSTL0, "VMS:record_script(%d):Step3: filename %s\n", id, fileName.c_str());
    tVoiceMsg::Save_qmsg_toWAV(q_rtp_in, fileName.c_str());
    //LOG1(2, "VMS: q_rtp_in: %d\n", q_rtp_in.size());
    {
    	auto locker = vms.getNumberLocker(digiN);
    CIniFileXML abo("abonents/" + digiN + "/abonent.xml");
    LOG2(TSTL1, "VMS:record_script(%d):Step3:AddElement to %s\n", id, abo.Path().c_str());
    if(true == abo.ReadFile(10)){
      abo.SetAttribute("Abonent", "sourceIP", _s_ip.toString().c_str());
      abo.AddElement(digiN.c_str(), digiN, fileName, vms.maxCountMsg);
      abo.WriteFile(10);
    }else
      LOG2(ERRR, "VMS:record_script(%d):Step3:Unable to perform abo.ReadFile(): %s\n", id, abo.Path().c_str());
    }
  }
 // UNLOCK(&lock);

  LOG1(TSTL1, "VMS:record_script(%d): finished\n", id);
  return ;
}

//
void CService_Call::play_script(const CString& digiN, bool _notify) {
  LOG1(TSTL1, "VMS:play_script(%d): started\n", id);

  int next_Step = 0; bool first_time = true;
  TiXmlElement* child; TiXmlElement* abonent;

  {
  	auto locker = vms.getNumberLocker(digiN);
//  try{
  CIniFileXML abo("abonents/" + digiN + "/abonent.xml");
  if (false == abo.ReadFile(10)) {
    LOG2(ERRR, "VMS:play_script(%d)::Unable to perform abo.ReadFile(): %s\n", id, abo.Path().c_str());
    goto L_empty;
  }

  abonent = abo.FindAbonent(digiN);
  if(NULL == abonent){
    LOG2(TSTL1, "VMS:play_script(%d):no abonent: %s\n", id, digiN.c_str());
    goto L_empty;
  }
  child = ((TiXmlElement*)abonent->FirstChild());
  if(NULL == child){
    LOG2(TSTL1, "VMS:play_script(%d):no child: %s\n", id, digiN.c_str());
    goto L_empty;
  }
  do{
		LOG2(INFO, "VMS:play_script(%d):try to notify abo: %s\n", id, digiN.c_str());
    if(_notify){
      //режим уведомления - только непрослушанные сообщения
      const char* has_read = child->Attribute("has_read");
      if((NULL != has_read) && ('1' == has_read[0])){
        //уже прослушанное - следующее
      	LOG1(TSTL1, "VMS:play_script(%d):notify cancel, already read, next one\n", id);
        continue;
      }
    }

    //шаг 0: проиграть "след сообщение"
    if(! first_time){
      LOG1(TSTL1, "VMS:play_script(%d):Step2: next message\n", id);
      next_Step = playVoiceMsg(vms.voice_msg_NextMessage);
      if (0 > next_Step) {
        //положили трубку
        goto L_endOfCall;
      }
    }
    first_time = false;

    //шаг1: открыть файл в очередь и проиграть сообщение
    LOG1(TSTL0, "VMS:play_script(%d):Step1: playing\n", id);
    const char* path = child->Attribute("path");

    if(NULL == path){
      LOG1(WARN, "VMS:play_script(%d): no path\n", id);
      //goto L_error;
      continue;
    }
    LOG2(TSTL1, "VMS:play_script(%d):loading:%s\n", id, path);
    {
      tVoiceMsg q_rtp_out(path);
      //tVoiceMsg::Load_qmsg(q_rtp_out, path);
//      LOG2(TSTL0, "VMS:play_script(%d):end of loading %d, play\n", id, q_rtp_out.size());
      next_Step = playVoiceMsg(q_rtp_out, _SIP_INFO);
    }
    LOG1(TSTL0, "VMS:play_script(%d):end of playing\n", id);
    if (0 > next_Step) {
      //положили трубку
      goto L_endOfCall;
    }

//    if(!_notify){
      //прослушали полностью - пометить как прочитано
    child->SetAttribute("has_read", "1");
//    }

  }while(NULL != (child = child->NextSiblingElement()));

  {
    LOG1(TSTL1, "VMS:play_script(%d):Step3: request for final action, msg after all\n", id);
    tVoiceMsg& voice_AfterPlay = _notify? vms.voice_msg_CancelNotification : vms.voice_msg_AfterPlay;

    //Спрашиваем: отметить/удалить сообщения как прослушенные?
    next_Step = playVoiceMsg(voice_AfterPlay, _SIP_INFO );
//    if(0 <= next_Step)
//      next_Step = playVoiceMsg(vms.voice_msg_Silence,  _SIP_INFO );// просто ждём и слушаем тишину

    if(+10 == next_Step){
      //Абонент ответил утвердительно
      if(_notify){
        //отметить все о прочтении
        child = ((TiXmlElement*)abonent->FirstChild());
        if(NULL == child){
          LOG2(ERRR, "VMS:notify_script(%d):no child: %s\n", id, digiN.c_str());
          goto L_empty;
        }
        do{
          child->SetAttribute("has_read", "1");
        }while(NULL != (child = child->NextSiblingElement()));

      }else{
        abo.WriteFile(100);
        goto L_endAfterDelete ;
      };
    }
  }

//  }catch(int res){
//
//  }
L_endOfCall:
  abo.WriteFile(100);
  }
  return ;
L_endAfterDelete:
	delete_script(digiN);//удалить все прочтённые
  LOG1(TSTL1, "VMS:play_script(%d): finished\n", id);
  return ;

L_empty:
  playVoiceMsg(vms.voice_msg_Empty);// пустой ящик/нету абонента
  return ;
}

//Проиграть сообщение
int CService_Call::playVoiceMsg(tVoiceMsg& _qmsg, int _add_cmd) {
  int next_Step = 0;
  LOG1(TSTL0, "VMS:playVoiceMsg:size %d\n", _qmsg.size());
  HANDLE hTimer = NULL;
  try {

    // Create an unnamed waitable timer.
    hTimer = CreateWaitableTimer(NULL, false, NULL);

    if (NULL == hTimer){
        LOG1(ALRM, "VMS:CreateWaitableTimer failed (%d)\n", (int)GetLastError());
        throw E(E_UNEXPECTED, "CreateWaitableTimer failed");
    }
    if(TIMERR_NOERROR != timeBeginPeriod(1)){
      LOG1(ALRM, "VMS:timeBeginPeriod failed (%d)\n", (int)GetLastError());
      throw E(E_UNEXPECTED, "timeBeginPeriod failed");
    }

    LARGE_INTEGER liDueTime;
    liDueTime.QuadPart = -60000*10;//-10000000LL;
    //кольцевой буфер, для баланса таймера
    double measures[8] = { 60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 60.0, 60.0};
    int measures_pos = 0;
    double measures_ave = 60.0;
    LARGE_INTEGER ms_internal_sum, ms_internal;
    QueryPerformanceCounter(&ms_internal_sum);

    LOCK(&lock);

    tVoiceMsg::iterator it = _qmsg.begin();

    if (it == _qmsg.end())
    {//пустое сообщение, след шаг
      next_Step = 1;
    }else
    do {

      if (!SetWaitableTimer(hTimer, &liDueTime, 0, NULL, NULL, 0)){
        LOG2(ALRM, "SetWaitableTimer failed (%d), %d\n", id, (int)GetLastError());
        throw E(E_UNEXPECTED, "SetWaitableTimer failed");
      }

      UNLOCK(&lock);
      if (WaitForSingleObject(hTimer, INFINITE) != WAIT_OBJECT_0){
        LOG2(ALRM, "WaitForSingleObject failed (%d), %d\n", id,(int) GetLastError());
        throw E(E_UNEXPECTED, "WaitForSingleObject failed");
      }
      LOCK(&lock);


      {//балансировка загрузки таймера, что должна делать win
        //частота процессора, для целей балансировки таймера
        LARGE_INTEGER freq;
        QueryPerformanceFrequency(&freq);

        QueryPerformanceCounter(&ms_internal);
        measures[measures_pos] =
          (double)((ms_internal.QuadPart - ms_internal_sum.QuadPart)*1000)/freq.QuadPart;//мс
        if(++measures_pos > 7) measures_pos = 0;

        ms_internal_sum = ms_internal;
        measures_ave = measures[0] + measures[1] + measures[2] + measures[3];
        measures_ave+= measures[4] + measures[5] + measures[6] + measures[7];
        measures_ave /= 8;

        if(measures_ave > 60)
          liDueTime.QuadPart = -59000*10;
        else
          liDueTime.QuadPart = -60000*10;
      }

      { //проверка внешней команды - возможно надо через WaitMultiplyObject
        //TODO: ох не нравится мне этот код, но пока копим крит кол-во замечаний.
        //внешняя команда - release, завершить звонок
        if (_release == mailBox.cmd) {
          //внешний отбой, прекратить вещать, выйти из потока
          mailBox.cmd = _ok;
          //finishScript = true;
          next_Step = -1;
          LOG0(TSTL0, "VMS:playVoiceMsg: return:-1,  _release == mailBox.cmd\n");
          continue;
        }

        //проверка доп внешних команд, что указаны в _add_cmd
        if ((_add_cmd == _SIP_INFO) && (_SIP_INFO == mailBox.cmd)) {
          if (mailBox.data_32 == 1) {//FIXME: 1 убрать в параметры
          	//пришёл запрос на удаление, обработать
            next_Step = +10;
          }else
            next_Step = -1;
          mailBox.cmd = _ok;
          LOG1(TSTL0, "VMS:playVoiceMsg: return:%d,  _SIP_INFO\n", next_Step);
          goto L_exit;
        }

        if (_ok != mailBox.cmd) {
          //был какой то сигнал, игнорировать, но ответить, разлочив клиента
          mailBox.cmd = _ok;
          REPLY(&cond);
        }
      }
//      liDueTime.QuadPart = -60000*10;//-10000000LL;
//      if (!SetWaitableTimer(hTimer, &liDueTime, 0, NULL, NULL, 0)){
//        LOG1(ALRM, "SetWaitableTimer failed (%d)\n", GetLastError());
//        throw E(E_UNEXPECTED, "SetWaitableTimer failed");
//      }
//
//      UNLOCK;
//      if (WaitForSingleObject(hTimer, INFINITE) != WAIT_OBJECT_0){
//        LOG1(ALRM, "WaitForSingleObject failed (%d), %s\n", GetLastError());
//        throw E(E_UNEXPECTED, "WaitForSingleObject failed");
//      }
//      LOCK;
      //int res = ETIMEDOUT;
      //--
      {
        //выдать порцию сообщения в сеть
        CPacket_RTP* rtp_packet = *it;
        rtp_packet->setCounts(rtp_out_count++
            , rtp_out_timeStamp);
        rtp_out_timeStamp += rtp_packet->getLengthOfPayload();

//        if(_qmsg == vms.voice_msg_Silence){
//          //молчание!
//
//        }else
        if (-1 == (sendto(socket_rtp_out, (const char*) rtp_packet->GetData(),
            rtp_packet->GetLength(), 0, (sockaddr*) &(addr_rtp_out), sizeof(sockaddr_in))))
          throw E(WSAGetLastError(), "VMS:Call_in:sendto");

        it++;

        if (it == _qmsg.end()) {//всё отговорили!
          //окончили сообщение, след шаг
          next_Step = 1;
          continue;
        }

      }
    } while (next_Step == 0);
//    LOG10(TSTL0, "VMS:play:%f,%f,%f,%f,%f,%f,%f,%f, %d,%d\n"
//        , measures[0], measures[1], measures[2], measures[3]
//        , measures[4], measures[5], measures[6], measures[7]
//        , 0 ,0);

  } catch (P_EXC e) {
    E_CATCH(e);
    next_Step = -3;//проблемы?
  }
L_exit:
  timeEndPeriod(1);
  //закрыть таймер
  CancelWaitableTimer(hTimer);
  REPLYandUNLOCK(&cond, &lock)
  //след шаг
  return next_Step;
}

//
int CService_Call::recordCallMsg() {
  timespec abstime;

  int next_Step = 0;
  try {
    abstime.tv_sec = time(NULL) + vms.maxRecordMsg;//сек
    abstime.tv_nsec = 0;

    LOCK(&lock);
    status = _record; //пошла запись!
    do {
      int res = pthread_cond_timedwait(&cond, &lock, &abstime);
      if (res == ETIMEDOUT) {
        LOG1(INFO, "VMS:recordCallMsg(%d): timeout of record\n", id);
        //окончили сообщение, след шаг
        next_Step = 1;

      } else if (res == EOK) {

        //внешняя команда
        if (_release == mailBox.cmd) {
          //внешний отбой, прекратить вещать
          //выйти из потока
          mailBox.cmd = _ok;
          //finishScript = true;
          next_Step = -1; //след шаг
        }
        //ответить клиенту
        REPLY(&cond);
      } else {
        LOG2(ERRR, "VMS:recordCallMsg(%d):pthread_cond_timedwait, code=%d\n", id, res);
        next_Step = -2; //проблемы? след шаг
      }
      //...
    } while (next_Step == 0);

    status = _busy; //стоп записи
    UNLOCK(&lock);

  } catch (P_EXC e) {
    E_CATCH(e);
    next_Step = -3;//проблемы?
    //след шаг
    status = _busy;
    REPLYandUNLOCK(&cond, &lock)
  }

  return next_Step;
}

}

