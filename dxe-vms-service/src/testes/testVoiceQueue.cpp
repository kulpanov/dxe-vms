

#include "netutils.h"

int testVoiceQueue(){
	tVoiceMsg voiceQueue;

	tVoiceMsg::Load_qmsg_fromWAV(voiceQueue, "wavs/hello.wav");
	auto it = voiceQueue.begin();
	do{
		it = voiceQueue.insert(it, new CPacket_RTP(*it));
		++it;
	}while(++it != voiceQueue.end());
	int i = voiceQueue.removeDuplicates();
	tVoiceMsg::Save_qmsg_toWAV(voiceQueue, "wavs/hello2.wav");
	i = i;
	return 0;
}
