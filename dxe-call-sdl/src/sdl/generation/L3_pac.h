
/* Program generated by Cadvanced 4.5.0  */
#define XSCT_CADVANCED

#ifndef XX_L3_pac_H
#define XX_L3_pac_H

/*************************************************************************
**                SECTION Types and Forward references                  **
*************************************************************************/

/*****
* PACKAGE L3_pac
* #SDTREF(SDL,C:\prj\amt\cool-ts-0.60\sdl\Common\L3_PAC\L3_pac.sun,4,1,9)
******/
extern XCONST struct xPackageIdStruct yPacR_z_L3_pac__L3_pac;

extern void yInit_L3_pac (void);

/*****
* PROCESS TYPE Level_3
* <<PACKAGE L3_pac>>
* #SDTREF(SDL,C:\prj\amt\cool-ts-0.60\sdl\Common\L3_PAC\L3_pac.sun(1),119(70,60),1,1)
******/
extern XCONST XSIGTYPE yPrsS_z_L3_pac_0_Level_3[];
extern XCONST xStateIdNode yPrsT_z_L3_pac_0_Level_3[];
extern XCONST struct xPrsIdStruct yPrsR_z_L3_pac_0_Level_3;
#define yPrsN_z_L3_pac_0_Level_3  (&yPrsR_z_L3_pac_0_Level_3)
#ifdef XCOVERAGE
extern long int yPrsC_z_L3_pac_0_Level_3[];
#endif
#ifndef XNOSTARTUPIDNODE
extern XCONST struct xSignalIdStruct ySigR_z_L3_pac_0_Level_3;
#define ySigN_z_L3_pac_0_Level_3  (&ySigR_z_L3_pac_0_Level_3)
#endif
typedef struct {
    SIGNAL_VARS
    STARTUP_VARS
} yPDef_z_L3_pac_0_Level_3;
typedef yPDef_z_L3_pac_0_Level_3  *yPDP_z_L3_pac_0_Level_3;
XPROCESSDEF_H(Level_3,"Level_3",z_L3_pac_0_Level_3,0,yVDef_z_L3_pac_0_Level_3)
#ifdef XBREAKBEFORE
#define ySym_z_L3_pac_0_Level_3  0
extern char * yRef_z_L3_pac_0_Level_3 (int, xSymbolType *);
#endif
typedef struct {
    PROCESS_VARS
} yVDef_z_L3_pac_0_Level_3;
typedef yVDef_z_L3_pac_0_Level_3  *yVDP_z_L3_pac_0_Level_3;
extern void yFree_z_L3_pac_0_Level_3 (void *);

/*************************************************************************
**                #CODE directives, #HEADING sections                   **
*************************************************************************/
#endif
