/** \brief  Реализация класса CVoiceMailService.
 * CVoiceMailService.cpp
 *  Created on: 11.04.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */

#include <time.h>
#include <winsock2.h>
#include <windows.h>

#include "CVoiceMailService.h"
#include "vms_signals.h"
#include "CService_Call.h"
#include "utils/netutils.h"
#include "utils/xml/iniFileXML.h"
#include <algorithm>

namespace AMT {

CVoiceMailService* vmService = NULL;

string factoryNumberCrypted;

//Общие конфиг данные
void CVoiceMailService::loadParams(int type){
    //Читаем файл настроек
    CIniFileXML cfg;
    cfg.SetPath("settings.xml");
    cfg.ReadFile(10);
    string cfg_str;
    LOCK(&lock);
    //Уровень вывода лога
    cfg_str = cfg.GetAttribute("General/logLevel","value","5");
    log_level = atoi(cfg_str.c_str());

    if((type == 0) || (type == 2)){
      //макс кол-во аб ящиков
      cfg_str = cfg.GetAttribute("Paths/maxCountMailBox", "value", "254");;//число
      maxMailBoxes = atoi(cfg_str.c_str());

      //макс кол-во секунд в сообщении
      cfg_str = cfg.GetAttribute("Paths/maxRecordMsg", "value", "30");;//секунд
      maxRecordMsg = atoi(cfg_str.c_str());

      //макс кол-во сообщений в ящике
      cfg_str = cfg.GetAttribute("Paths/maxCountMsg", "value", "30");;//кол-во
      maxCountMsg = atoi(cfg_str.c_str());
    }
    if((type == 0) || (type == 1)){
      cfg_str = cfg.GetAttribute("Events", "check", "1");
      notifyMode = atoi(cfg_str.c_str());

      cfg_str = cfg.GetAttribute("Events/timeFrom", "value", "10:00");
      notifying_startTime = cfg_str;

      cfg_str = cfg.GetAttribute("Events/timeTo", "value", "19:00");
      notifying_stopTime = cfg_str;

      //Интервал
      cfg_str = cfg.GetAttribute("Events/Interval", "value", "15");
      notifying_interval = atoi(cfg_str.c_str());

      //Продолжительность попытки
      cfg_str = cfg.GetAttribute("Events/Length", "value", "15");
      notifying_callTime = atoi(cfg_str.c_str());
    }

    if(type == 0){
      //сценарий "Запись"
      cfg_str = cfg.GetAttribute("Paths/typeRec", "value", "301");;//кнопка
      typeRec = atoi(cfg_str.c_str());

      //сценарий "Проиграть"
      cfg_str = cfg.GetAttribute("Paths/typePlay", "value", "302");;//кнопка
      typePlay = atoi(cfg_str.c_str());

      //сценарий "Удалить прослушенные"
      cfg_str = cfg.GetAttribute("Paths/typeDelete", "value", "303");;//кнопка
      typeDelete = atoi(cfg_str.c_str());

      //сценарий "Удалить все"
      cfg_str = cfg.GetAttribute("Paths/typeClear", "value", "304");;//кнопка
      typeClear = atoi(cfg_str.c_str());

      //cfg_str = cfg.GetAttribute("Paths/typeClear", "value", "300");;//кнопка
      //typeNotify = 300;//atoi(cfg_str.c_str());

    //читаем мой IP
    cfg_str = cfg.GetAttribute("General/myIP", "value", "192.168.233.1");
#ifdef WITH_CMD_THREAD
    cfg_str = ("192.168.233.1");
#endif
    myIP    = IP(cfg_str);

    //моя сеть
    cfg_str = cfg.GetAttribute("General/myNet","value","255.255.255.0");
    myNET   = IP(cfg_str);

    //Порт для входящих SIP-соединений
    cfg_str = cfg.GetAttribute("General/SIPin","value","5060");
    sipPort_in = atoi(cfg_str.c_str()); //порт для SIP-сообщений
    //Порт для исходящих SIP-соединений
    cfg_str = cfg.GetAttribute("General/SIPout","value","5060");
    sipPort_out = atoi(cfg_str.c_str());

    {
    	CIniFileXML keyFile("./license.key");
    	if(!keyFile.ReadFile(10)){
    		LOG(ERRR, "No license file found, fall to demo mode\n");
    	}else{
    		crypt.setFactoryNumber(
    						keyFile.GetAttribute("General/licenseString", "value", "nostring"));
    	}
    }
#ifdef WITH_CMD_THREAD
    sipPort_in = 5061;
    //gwIP = 0x7f000001;
#endif

    //число каналов обслуживания
    poolCount = POOL_COUNT;

    {//заполнение route_table
      IP natip(0,0,0,0), localip(0,0,0,0), destip(0,0,0,0);
      initIP = new SDL::CSDL_Signal_InitIP(myIP, myNET, destip);
      //initIP->AddToRoute(destip, localip, natip);//не используемая строка, см доку

      char sect[50];

      //по доке это не используемая строка, но на всякий случай заполняем и её
      sprintf(sect, "Route_Table/param%d", 0);
      destip = cfg_str = cfg.GetAttribute(sect, "destIP", "0");
      localip = cfg_str = cfg.GetAttribute(sect, "gateLocal", "0");
      natip = cfg_str = cfg.GetAttribute(sect, "gateNAT", "0");
      initIP->AddToRoute(destip.getIP(), localip.getIP(), natip.getIP());

      //заполняем таблицу маршрутизации
      for(int i=0; i<10; i++){
        sprintf(sect, "Route_Table/param%d", i);
        destip = cfg_str = cfg.GetAttribute(sect, "destIP", "0");
        localip = cfg_str = cfg.GetAttribute(sect, "gateLocal", "0");
        natip = cfg_str = cfg.GetAttribute(sect, "gateNAT", "0");
        initIP->AddToRoute(destip.getIP(), localip.getIP(), natip.getIP());
      }

    }

    //заполнить очереди системных голосовых сообщений, имена файлов предопределены
    {
      tVoiceMsg::Load_qmsg_fromWAV(voice_msg_Beep, "wavs/beepVMS.wav");
      tVoiceMsg::Load_qmsg_fromWAV(voice_msg_BeforeClear, "wavs/voiceBeforeClear.wav");
      tVoiceMsg::Load_qmsg_fromWAV(voice_msg_BeforeDelete, "wavs/voiceBeforeDelete.wav");

      tVoiceMsg::Load_qmsg_fromWAV(voice_msg_BeforeRecord, "wavs/voiceLeaveMessage.wav");
      tVoiceMsg::Load_qmsg_fromWAV(voice_msg_Empty, "wavs/voiceEmptyBox.wav");
      tVoiceMsg::Load_qmsg_fromWAV(voice_msg_OnConnect, "wavs/beepServerConnect.wav");

      tVoiceMsg::Load_qmsg_fromWAV(voice_msg_Silence, "wavs/voiceSilence.wav");

      tVoiceMsg::Load_qmsg_fromWAV(voice_msg_AfterPlay, "wavs/voiceAfterPlay.wav");
      tVoiceMsg::Load_qmsg_fromWAV(voice_msg_NextMessage,   "wavs/voiceNextMessage.wav");
      tVoiceMsg::Load_qmsg_fromWAV(voice_msg_BeforeReject, "wavs/voiceBeforeReject.wav");

      tVoiceMsg::Load_qmsg_fromWAV(voice_msg_Notify, "wavs/voiceAnnounce.wav");

      tVoiceMsg::Load_qmsg_fromWAV(voice_msg_CancelNotification, "wavs/voiceCancelNotification.wav");
    }

  }
  UNLOCK(&lock);
}

//сохранить параметры
void CVoiceMailService::saveParams(){
  //нет таких параметров
}

//invite
void CVoiceMailService::Invite(int _id
      , const std::string& _strTarget, const std::string& _strSource
      , const uint32_t _s_ip, const std::string& _strFNumber){
  try{
    if(_id >= poolCount)
      throw E(E_INVALIDARG, "VMS:Invite");//превышение индекса
    //todo: add check fNumber
    LOG3(INFO, "VMS: Invite: _strN=%s, _strD=%s, f_number=%s\n"
    		, _strSource.c_str(), _strTarget.c_str(), _strFNumber.c_str());
    loadParams(2);
    //обработать входящий запрос
    pool_ofCall_in[_id]->Invite(_strTarget, _strSource, _s_ip);
  }catch(...){
    //в случае искл ситуации отбить звонок
    LOG1(7, "VMS:Invite: id=%d: out of bound\n", _id);
    CVMS_Signal_Release_out release(_id);
    send(&release);
  }
}

//release
void CVoiceMailService::Release(int _id){
  try{
    if(_id >= poolCount)
      throw E(E_INVALIDARG, "VMS:start_CallIn");//превышение индекса
    //обработать входящий запрос
    pool_ofCall_in[_id]->Release();
  }catch(...){
    LOG1(7, "VMS:Release: id=%d: out of bound\n", _id);
  }
}

//release
void CVoiceMailService::Connect(int _id){
  try{
    if(_id >= poolCount)
      throw E(E_INVALIDARG, "VMS:start_CallIn");//превышение индекса
    //обработать входящий запрос
    pool_ofCall_in[_id]->Connect();
  }catch(...){
    LOG1(7, "VMS:Release: id=%d: out of bound\n", _id);
  }
}

//Старт работы
void CVoiceMailService::start(std::string _opts){

  //Залогировать старт работы
  time_t curtime = time(NULL);
  LOG2(6, "VMS started with opts '%s', at %s\n", _opts.c_str(), ctime(&curtime));
  //Загрузить параметры
  loadParams();

//  int ret; int policy;
//  struct sched_param param;
//  /* scheduling parameters of target thread */
//  ret = pthread_getschedparam(pthread_self(), &policy, &param);
//  param.sched_priority = 14;
//ret = pthread_getschedparam(sip_listener, &policy, &param);

  //инит барьер на старт потоков
  pthread_barrier_init(&start_barier, NULL, 4);

  //создаём и стартуем основной поток VMS
  pthread_create(&sdl_listener, NULL, thread_main, this);

  //создаём и стартуем поток слушающий входящие SIP-сообщения, count=1
  pthread_create(&sip_listener, NULL, thread_SIP_listener, this);
  //ret = pthread_setschedparam(sip_listener, policy, &param);
  Sleep(500);
  //создаём и стартуем поток слушающий входящие SDL-сообщения, count=2
  pthread_create(&sdl_listener, NULL, thread_SDL_listener, this);
 // ret = pthread_setschedparam(sdl_listener, policy, &param);
  Sleep(500);
  //создать пул потоков обслуживания входящих соединений
  for(int i = 0; i<poolCount; i++){
    pool_ofCall_in[i] = (new CService_Call(*this, i));
    //ret = pthread_setschedparam(pool_ofCall_in[i]->callIn_script, policy, &param);
  }

  //и Я последний, count=4, пошли работать
  pthread_barrier_wait(&start_barier);
  pthread_barrier_destroy(&start_barier);
  service_started = true;
}

//создать
CVoiceMailService& CVoiceMailService::Create(){
  if(vmService == NULL) vmService = new CVoiceMailService();
  return *vmService;
}

//закончить
void CVoiceMailService::Exit(int code){
  LOG(WARN, "VMS: system shutdown\n");
//  delete vmService;
  exit(code);
}

//собрать
CVoiceMailService::CVoiceMailService()
  :/*fileName("main.cfg")
  , */defReceiver(NULL)
  , initIP(NULL)
  , poolCount(POOL_COUNT)
{
  service_started = false;
  pthread_mutex_init(&lock, NULL);
  pthread_cond_init(&cond, NULL);
  //Иниц сокет-подсистему Windows
  if (WSAStartup(MAKEWORD(2,2), &lpWSAData)!=0) {
    throw E(WSAGetLastError(), "VMS::CVoiceMailService:WSAStartup");
  }
}

//зачистить
CVoiceMailService::~CVoiceMailService() {
  pthread_cancel(sip_listener);
  pthread_cancel(sdl_listener);
  closesocket(socket_sipudp_out);
  for(int i = 0; i<poolCount; i++)
    delete pool_ofCall_in[i];

  WSACleanup();
  pthread_mutex_destroy(&lock);
  pthread_cond_destroy(&cond);
}



}
