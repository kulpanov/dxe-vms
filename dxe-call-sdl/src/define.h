/*
 * define.h
 *
 *  Created on: 21.12.2010
 *      Author: Kulpanov
 */

#ifndef DEFINE_H_
#define DEFINE_H_

#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include "debug.h"

#define EOK 0
#define ASSERT assert

#endif /* DEFINE_H_ */
