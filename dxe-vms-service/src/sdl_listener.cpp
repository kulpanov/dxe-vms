/** sdl_listener.cpp
 * Метод-слушатель SDL-сигналов
 *  Created on: 23.03.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */
#include <string>
#include <unistd.h>
#include <algorithm>

#include "define.h"

#include "vms_signals.h"
#include "CVoiceMailService.h"

namespace AMT {

//Метод-слушатель входящих SDL-сигналов
void* CVoiceMailService::SDL_listener() {
  LOG(TSTL1, "VMS:SDL_listener started\n");
  try {
  	struct sockaddr_in sip_me;

    //создать сокет для исходящего SIP-потока
    if (-1 == (socket_sipudp_out = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)))
      throw E(WSAGetLastError(), "VMS:SDL_listener:socket");

    memset((char *) &sip_me, 0, sizeof(sip_me));
    sip_me.sin_family = AF_INET;
    sip_me.sin_port = htons(0);
    sip_me.sin_addr.s_addr = htonl(myIP.getIP());

    if (bind(socket_sipudp_out, (sockaddr *) &sip_me, sizeof(sip_me)) == -1)
      throw E(WSAGetLastError(), "VMS::SIP_listener::bind");
    LOG1(TSTL1, "VMS: for sip out I use %s\n", myIP.toString().c_str());

    //заполнить поля сокета
    memset((char *) &sip_other, 0, sizeof(sockaddr_in));
    sip_other.sin_family = AF_INET;
    sip_other.sin_port = htons(sipPort_out);
    //sip_other.sin_addr.S_un.S_addr = htonl(gwIP);
    err_log_level = TSTL1;
    //добавляем обработчики исходящих сигналов SDL
    sdl_vms.addListener(new CVMS_Signal_Err(*this));
    sdl_vms.addListener(new CVMS_Signal_wr_Receiver_Table(*this));
    sdl_vms.addListener(new CVMS_Signal_wr_Sender_Table(*this));

    //Старт системы, строка запуска зарезервирована для будущего использования
    //По старту происходит создание отдельных потока SDL-системы
    sdl_vms.start(" ");

    //ждём остальных
    pthread_barrier_wait(&start_barier);

    {///выбрать все сообщения, по старту системы
      int timeOut = 3;//секунды
      do {
        SDL::CSDL_Signal* sig = sdl_vms.receive(timeOut);
        if (NULL == sig)
          break;//timout - признак выборки всей очереди сообщений
        delete sig;
      } while (1);
    }
    //добавляем обработчиков
    sdl_vms.addListener(new CVMS_Signal_Init(*this));
    sdl_vms.addListener(new CVMS_Signal_UDPDR_long(*this));
    sdl_vms.addListener(new CVMS_Signal_Setup(*this));
    sdl_vms.addListener(new CVMS_Signal_Release(*this));
    sdl_vms.addListener(new CVMS_Signal_Connect(*this));
    sdl_vms.addListener(new CVMS_Signal_Info(*this));

    {//Создаём сигнал инициализации
      //SDL::CSDL_Signal_InitIP initIP(myIP, myNET, myVoIPNAT);
      //initIP.AddToRoute();
      //и отправляем его
      send(initIP);
    }

    {//Основной цикл выборки сообщений
      int timeOut = 2;
      while (1) {
        //Блокировка потока на ожидании сигнала,
        //также поток разблокируетя по истечению timeOut секунд
        SDL::CSDL_Signal* sig = sdl_vms.receive(timeOut);
        if (NULL == sig) {
          //сработал timeout
          //LOG4("VMS: time is %d sec\n", ++seconds);
          err_log_level = INFO;
          timeOut = 60;
          continue;
        }
        delete sig;
      }
    }
    //...


  } catch (P_EXC e) {
    //в случае искл ситуации - залогировать и выйти
    E_CATCH(e);
  }

  Exit(1);
  return NULL;
}

}
