/** \brief Реализация методов CService_Call_in.
 * CService_Call_in.cpp
 *
 *  Created on: 13.04.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */
#include "define.h"
#include "CVoiceMailService.h"
#include "vms_signals.h"
#include "CService_Call.h"

#include <time.h>
#include "utils/utils.h"

namespace AMT {

// Установить порт входящего потока
void CVoiceMailService::setRTP_in(int _id, int _rtp_in){
  //проверка на допустимость индекса
  if(_id >= poolCount)
    return;//throw E(E_INVALIDARG, "VMS:setRTPPorts");

  pool_ofCall_in[_id]->SetRTP_in(_rtp_in);
}

// Установить порт исходящего потока
void CVoiceMailService::setRTP_dst(int _id, int _d_rtp, int _d_ip){
  //проверка на допустимость индекса
  if(_id >= poolCount)
    return;//throw E(E_INVALIDARG, "VMS:setRTPPorts");
//  LOG3(TSTL1, "VMS: setRTP_dst: id%d. d_rtp=%d, d_ip=%d\n", _id, _d_rtp, _d_ip);
  pool_ofCall_in[_id]->setRTP_dst(_d_rtp, _d_ip);
}
// Установить SIP Info сигнал
void CVoiceMailService::putSIP_Info(int _id, int _data1, int _data2, int _data3){
  //проверка на допустимость индекса
  if(_id >= poolCount)
    throw E(E_INVALIDARG, "VMS:putSIP_Info");

  pool_ofCall_in[_id]->putSIP_Info(_data1, _data2, _data3);
}

//поток обслуживающий соединение
void *CService_Call::main_thread() {
  CString digiD, digiN; IP s_ip;
    pthread_barrier_init(&start_barier, NULL, 2);
    pthread_create(&rtp_listener, NULL, thread_RTP_listener, this);
    pthread_barrier_wait(&start_barier);
    //pthread_barrier_destroy(&start_barier);

    //    pthread_mutex_lock(&socket_mutex);
    //    pthread_barrier_wait(&start_barier);

    do
    try {
      LOCK(&lock);
      status = _idle;
      int res = WAITSIGNAL(&cond, &lock);

      status = _busy;

      if (res == EOK) {
        switch (mailBox.cmd) {
          case (_invite): {
            //входящие вызовы
            //внешняя команда
            //id = mailBox.id;
            digiD = mailBox.data2;//_digiD;
            digiN = mailBox.data1;//_digiN;
            s_ip = mailBox.data_32;
            //разлокировть и обработать
            REPLYandUNLOCK(&cond, &lock);
            processCallIn(digiD, digiN, s_ip);
          } break;

          case (_setup):{
            //исходящий вызовы
            digiD = mailBox.data2;//_digiD;
            digiN = mailBox.data1;//_digiN;
            s_ip = mailBox.data_32;
            //обработать и разлокировать
            REPLYandUNLOCK(&cond, &lock);
            processCallOut(digiD, digiN, s_ip);
            REPLY(&setup_cond);
          }
          break;
          case (_rtp_cancel): {
            //rtp=поток сообщил, что завершил работу по ошибке
            LOG1(ALRM, "VMS:Invite(%d): recv _rtp_cancel\n", id);
            //throw E(WSAGetLastError(), "VMS:Call_in:main_thread");
          }
          break;
          default:
            //на всё остальное!
            REPLYandUNLOCK(&cond, &lock);
        }
      } else {
        LOG2(ALRM, "VMS:Invite(%d):pthread_cond_timedwait, code=%d\n"
            , id, res);
        REPLYandUNLOCK(&cond, &lock);
        Sleep(1000);
      }

    } catch (P_EXC e) {
      E_CATCH(e);
      REPLYandUNLOCK(&cond, &lock);
    }
    while (1);

  return NULL;
}

//Обработка после окончания вызова
void CService_Call::AfterCall(const CString& _digiN){
  //слушающий поток
  vms.remBusyNumber(_digiN);
  status = _idle;
  rtp_out = 0;
  rtp_gw  = (uint32_t)0;
  //auto it_number = find(listOfBusyNumbers.begin(), listOfBusyNumbers.end(), );
  LOG1(INFO, "VMS:AfterCall, id=%d\n", getId());
  LOGTIME(INFO);
  if(socket_rtp_out != -1){
    closesocket(socket_rtp_out);socket_rtp_out = -1;}
  if(socket_rtp_in != -1){
    closesocket(socket_rtp_in);socket_rtp_in = -1;}
}

//Проверить цифры входящего соединения
void CService_Call::BeforeCall(const CString& _digiN){
    {//запустить timeout
      //Sleep(1000);
      LOG2(INFO, "VMS:BeforeCall, id=%d, DigiN=%s\n", getId(), _digiN.c_str());
      LOGTIME(INFO);
      timespec abstime;
      abstime.tv_sec = time(NULL) + 2;//2 sec of timeout
      abstime.tv_nsec = 0;

      LOCK(&(lock));
      if(0 == rtp_out){
        //ожидаем wr_sender_table
        WAITSIGNALTIMED(&(rtpDest_cond), &(lock), &abstime);
//        rtp_out = vms.invitePorts[id];
//        rtp_gw  = vms.inviteGWs[id];
      }//else - уже был сигнал
      UNLOCK(&(lock));

      if(0 == rtp_out){
        //wr_sender_table так и не пришёл, отбой
        int err = errno;
        LOG3(ERRR,"VMS:BeforeCall(%d):%s: Timeout on wr_senderTable, reason %d\n"
                        , id, _digiN.c_str(), err);
        throw E(err, "see prev logs");
      }
    }
    //запускаем сокеты на прослушку
    rtp_out_count = 0;
    rtp_out_timeStamp = 0;


    //создать сокет исходящего трафика
    if (-1 == (socket_rtp_out = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)))
      throw E(WSAGetLastError(), "VMS:BeforeCall:socket");
    memset((char *) &addr_rtp_out, 0, sizeof(sockaddr_in));

    addr_rtp_out.sin_family = AF_INET;
    addr_rtp_out.sin_port = htons(0);
    addr_rtp_out.sin_addr.s_addr = htonl(vms.myIP.getIP());

    if (bind(socket_rtp_out, (sockaddr *) &addr_rtp_out, sizeof(addr_rtp_out)) == -1)
      throw E(WSAGetLastError(), "VMS::socket_rtp_out::bind");

    addr_rtp_out.sin_port = htons(rtp_out);
    addr_rtp_out.sin_addr.S_un.S_addr = htonl(rtp_gw); //= htonl(vms.gwIP);

    //создать сокет входящего трафика
    if (-1 == (socket_rtp_in = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)))
      throw E(WSAGetLastError(), "VMS::socket_rtp_in::socket");

    memset((char *) &addr_rtp_in, 0, sizeof(addr_rtp_in));
    addr_rtp_in.sin_family = AF_INET;
    addr_rtp_in.sin_port = htons(rtp_in);
    addr_rtp_in.sin_addr.s_addr = htonl(vms.myIP.getIP());

    if (bind(socket_rtp_in, (sockaddr *) &addr_rtp_in, sizeof(addr_rtp_in)) == -1)
      throw E(WSAGetLastError(), "VMS::BeforeCall::bind");

    //Дать разрешении на прослушку RTP-слушателю
    //чёйто я тут навертел!
    LOCK(&socket_mutex);
    SIGNALandWAITREPLY(&socket_cond, &socket_mutex);
    UNLOCK(&socket_mutex);

    vms.addBusyNumber(_digiN);
    return ;
}

void CService_Call::putSIP_Info(int _data1, int _data2, int _data3){
  LOCK(&lock);
    //if(_play == status)
    {
      std::string str = "";
      str += char(_data1); str+= char(_data2); str += char(_data3);
      mailBox.cmd = _SIP_INFO;
      mailBox.data_32 = _data2;
      SIGNALandWAITREPLY(&cond, &lock);
    }
  UNLOCK(&lock);
}

//Invite - запрос на соединение
void CService_Call::Invite(const std::string& _digiD
                            , const std::string& _digiN, const uint32_t _s_ip){
  if(status !=  _idle){
    //уже работаем, отказ
    doEndOfCall();
    LOG3(INFO, "VMS:Invite(%d): %s:%s was rejected\n", id, _digiD.c_str(), _digiN.c_str());
    return ;
  }
  //id = _id;
  LOCK(&lock);
    mailBox.cmd = _invite;
    mailBox.data2 = _digiD;
    mailBox.data1 = _digiN;
    mailBox.data_32 = _s_ip;
    SIGNALandWAITREPLY(&cond, &lock);
  UNLOCK(&lock);

  return ;
}

//Release - отбой звонка
void CService_Call::Connect(){
  LOCK(&lock);
    mailBox.cmd = _connect;
    SIGNALandWAITREPLY(&cond, &lock);
  UNLOCK(&lock);
}

//Release - отбой звонка
void CService_Call::Release(){
  LOCK(&lock);
    mailBox.cmd = _release;
    SIGNALandWAITREPLY(&cond, &lock);
  UNLOCK(&lock);
}

void CService_Call::Setup(const std::string & _digiD, const std::string & _digiN, const uint32_t _s_ip){
  if(status !=  _idle){
    //уже работаем, отказ
    LOG3(INFO, "VMS:Setup(%d): %s:%s was rejected\n", id, _digiD.c_str(), _digiN.c_str());
    return ;
  }
  //id = _id;
  LOCK(&lock);
    mailBox.cmd = _setup;
    mailBox.data2 = _digiD;
    mailBox.data1 = _digiN;
    mailBox.data_32 = _s_ip;
    SIGNALandWAITREPLY(&cond, &lock);
    WAITREPLY(&setup_cond, &lock);
  UNLOCK(&lock);

  //WAITREPLY(&setup_cond, &setup_lock);
  return ;
}

//завершить вызов
void CService_Call::doEndOfCall() {
  //сигнал на отбой
  CVMS_Signal_Release_out release(id);
  vms.send(&release);
  return;
}

//CService_Call_in
CService_Call::CService_Call(CVoiceMailService& _vms, int _id)
  :vms(_vms), id(_id), rtp_in(0), rtp_out(0), rtp_gw()
 , status(_idle), socket_rtp_out(0), socket_rtp_in(0)
{
  pthread_mutex_init(&lock, NULL);
  //if(errno != EOK) LOG1(ALRM, "VMS:Call_in(%d): Error lock\n", errno);
  pthread_cond_init(&cond, NULL);

  pthread_mutex_init(&socket_mutex, NULL);
  pthread_cond_init(&socket_cond, NULL);

  pthread_cond_init(&rtpDest_cond, NULL);
  pthread_cond_init(&setup_cond, NULL);

  pthread_create(&callIn_script, NULL, thread_main, this);
}

//~CService_Call_in
CService_Call::~CService_Call() {

  if(socket_rtp_out>0) closesocket(socket_rtp_out);

  pthread_cancel(rtp_listener);
  pthread_cancel(callIn_script);

  pthread_cond_destroy(&rtpDest_cond);
  pthread_cond_destroy(&setup_cond);

  pthread_mutex_destroy(&socket_mutex);
  pthread_cond_destroy(&socket_cond);

  pthread_mutex_destroy(&lock);
  pthread_cond_destroy(&cond);
  pthread_barrier_destroy(&start_barier);
}

}

