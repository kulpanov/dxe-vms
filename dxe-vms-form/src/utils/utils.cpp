#include <QtCore/QDir>
#include <QtCore/QDateTime>
#include <QtGui/QMessageBox>

#include "utils.h"

//14 bold
QFont* title_font;
//10 bold
QFont* header_font;

QString logDirectory = "./logs/";
int countAbonents = 0;

void CreateFonts(){
  //создаем необходимые шрифты
  title_font = new QFont();
  title_font->setBold(true);
  title_font->setPointSize(14);

  header_font = new QFont();
  header_font->setPointSize(10);
  header_font->setBold(true);
  header_font->setKerning(false);

};


int removeFolder(QDir & dir)
{
  int res = 0;
  //Получаем список каталогов
  QStringList lstDirs = dir.entryList(QDir::Dirs |
                  QDir::AllDirs |
                  QDir::NoDotAndDotDot);
  //Получаем список файлов
  QStringList lstFiles = dir.entryList(QDir::Files);

  //Удаляем файлы
  foreach (QString entry, lstFiles)
  {
   QString entryAbsPath = dir.absolutePath() + "/" + entry;
   QFile::setPermissions(entryAbsPath, QFile::ReadOwner | QFile::WriteOwner);
   //QFile::setPermissions( entryAbsPath, QFile::WriteOwner|QFile::WriteUser|QFile::WriteGroup|QFile::WriteOther );
   QFile::remove(entryAbsPath);
  }

  //Для папок делаем рекурсивный вызов
  foreach (QString entry, lstDirs)
  {
   QString entryAbsPath = dir.absolutePath() + "/" + entry;
   QDir dr(entryAbsPath);
   removeFolder(dr);
  }

  if (!QDir().rmdir(dir.absolutePath()))
  {
    res = 1;
  }
  return res;
}


QString  CreateLogDir(){
  QDir log_dir;
  QDateTime dt = QDateTime::currentDateTime();
  QString res = dt.toString("yyyy_MM_dd_hh_mm_ss");
//  logDirectory = "./logs/" + res + "/";
//  log_dir.mkdir(logDirectory);
  LOG("Form has started at " + res + "\n");
  return res;
};

void LOG(const QString & _logStr){
  QFile logFile(logDirectory + "vms.log");
  if (!logFile.open(QIODevice::ReadWrite))
      return;
  logFile.readAll();
  logFile.write(QString("vms.form: "+
        QDateTime::currentDateTime().toString("Tdd/MM hh:mm:ss")
        +": "+_logStr).toStdString().c_str());
  logFile.close();
};

int CreateMessageBox(const QString &_message, const QString &_action) {
  QMessageBox msgBox;
  msgBox.setWindowTitle(QString::fromUtf8("Сообщение"));
  msgBox.setText(_message);
  msgBox.setInformativeText(_action);
  msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
  msgBox.setDefaultButton(QMessageBox::Cancel);
  return msgBox.exec();
}
bool CheckAbonents(){
    QDir dir("./abonents");
    int newCount = dir.entryList(QDir::Dirs | QDir::AllDirs | QDir::NoDotAndDotDot).size();
    if(newCount != countAbonents){
        countAbonents = newCount;
        return true;
    };
    return false;
};
