/* MD5.H - header file for MD5C.C
 */

/* Copyright (C) 1991-2, RSA Data Security, Inc. Created 1991. All
rights reserved.

License to copy and use this software is granted provided that it
is identified as the "RSA Data Security, Inc. MD5 Message-Digest
Algorithm" in all material mentioning or referencing this software
or this function.

License is also granted to make and use derivative works provided
that such works are identified as "derived from the RSA Data
Security, Inc. MD5 Message-Digest Algorithm" in all material
mentioning or referencing the derived work.

RSA Data Security, Inc. makes no representations concerning either
the merchantability of this software or the suitability of this
software for any particular purpose. It is provided "as is"
without express or implied warranty of any kind.

These notices must be retained in any copies of any part of this
documentation and/or software.
 */
#pragma once

/* MD5 context. */
typedef struct {
  UINT4 state[4];                                   /* state (ABCD) */
  UINT4 count[2];        /* number of bits, modulo 2^64 (lsb first) */
  unsigned char buffer[64];                         /* input buffer */
} MD5_CTX;

void MD5Init (MD5_CTX *);
void MD5Update (MD5_CTX *, unsigned char *, unsigned int);
void MD5Final (unsigned char [16], MD5_CTX *);

void MDString (unsigned char *string, unsigned char digest[16]);
//void MDString (unsigned char *string, unsigned char digest[16]);
//void MDString (char **string, unsigned char *digest[16]);
//
//void MDString(void* string, void* digest);
//void MDTimeTrial (void);
//void MDTestSuite (void);
//void MDFile (char *);
//void MDFilter (void);
//
//void MD5Transform (UINT4 [4], unsigned char [64]);
//void Encode (unsigned char *, UINT4 *, unsigned int);
//void Decode (UINT4 *, unsigned char *, unsigned int);
//void MD5_memcpy (POINTER, POINTER, unsigned int);
//void MD5_memset (POINTER, int, unsigned int);
