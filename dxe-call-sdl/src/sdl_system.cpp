/*
 * env.cpp
 *
 *  Created on: 21.12.2010
 *      Author: Kulpanov
 */

#include <time.h>
#include "define.h"
#include "../include/sdl_sys.h"
#ifdef __cplusplus
extern "C"{
#include "scttypes.h"
//#include "testcomponent.ifc"

//#include "gwbf.h"
//extern void xCloseEnv(void);
//extern void xMainInit(void);
extern void sdl_main (void);

void deliverSignal(void* SignalOut){
  SDL::sdlSystem->deliver(SignalOut);
}
}
#endif

namespace SDL{

////////////////////////////////////////////////////////////////////////////////
CSDL_system* sdlSystem = NULL;

CSDL_system::CSDL_system(){
  pthread_mutex_init(&mutex, NULL);
  pthread_cond_init(&cond, NULL);
}

CSDL_system::~CSDL_system(){
  pthread_cancel(sdl_thread);
  //pthread_join(sdl_thread, NULL);
  sleep(1);
  pthread_detach(sdl_thread);
  pthread_mutex_destroy(&mutex);
  pthread_cond_destroy(&cond);
}

//==============================================================================
void CSDL_system::start(std::string _opts)
{
  //заполнение обработчиков выходных из SDL сигналов
//  signalListeners[MG_Start] = new CSDLSignal_sys_start();//0x6a39998c
//  signalListeners[MADR_nt] = new CSDLSignal_Announce();
//  signalListeners[InitIP_I] = new CSDLSignal_InitIP_RQ();

  //старт потока
  pthread_mutex_lock( &mutex );
  pthread_create(&sdl_thread, NULL, thread_main, this);
  pthread_mutex_unlock( &mutex );
  //Sleep(1000);//на всякий случай

}

//==============================================================================
void* CSDL_system::thread_main(void* _sdlSystem){
  sdlSystem = (CSDL_system*)_sdlSystem;
  //основной поток SDL
  sdl_main();

  //XSYSD Trace_Default = 4;
  return NULL;
}

//void CSDL_system::addListener1(const CSDL_Signal *_sig){
//  //поиск по sig->NameNode
//  signalListeners[_sig->index()] = _sig;
//}

//client part ==================================================================
void CSDL_system::send(const CSDL_Signal *_sig)
{
  xSignalStruct* sig = (xSignalStruct*)(_sig->GetSignal(NULL));
  SDL_Output(sig , (xIdNode *)0);
}

//client part===================================================================
CSDL_Signal* CSDL_system::receive(int _timeOut)
{
  struct timespec abstime;
  abstime.tv_sec = time(NULL) + _timeOut;
  abstime.tv_nsec = 0;

  int res = EOK;
  SDL::ISDL_Signal* outSignal = NULL;

  pthread_mutex_lock( &mutex );

    if (queue_ofSignal.size() == 0) {
      //очередь пуста - ждём заполнения и сигнала
      res = pthread_cond_timedwait(&cond, &mutex, &abstime);
    }
    if(res == ETIMEDOUT){
    //тайм аут ожидания
      outSignal = NULL;
    }else if(res == EOK){
      outSignal = queue_ofSignal.front();
      queue_ofSignal.pop();
      //ASSERT(outSignal); //not null
    }else{
      perror("SDL:receive:pthread_cond_timedwait");
      sleep(1);
    }

  pthread_mutex_unlock( &mutex );

  if(outSignal) outSignal->OnReceive(NULL);
  return outSignal;
}

//server part ==================================================================
void CSDL_system::deliver(const void* _sig)
{
  ASSERT(_sig);
  //xSignalNode *SignalOut = (xSignalNode*)_sig;
  xSignalStruct* sig = *((xSignalNode*)_sig);
  pthread_mutex_lock(&mutex);
  {
    uint32_t indx = (uint32_t )(sig->NameNode);
    tListeners::iterator it = signalListeners.find(indx);
    if(it != signalListeners.end()){
      //есть обработчик, заполнить его данные
      queue_ofSignal.push(signalListeners[indx]->Clone(sig));
      //и выдать сигнал слушателю
      pthread_cond_signal(&cond);
    }
    //неизвестный сигнал - проглотить
    //...
  }
  pthread_mutex_unlock(&mutex);

}

}
