﻿
/** \brief Объявления класса CSDL_system - основного класса интеграции SDL.
 *  sdl_sys.h
 *
 *  Created on: 21.12.2010
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */

#pragma once

#ifdef BUILDING_DLL
#define DLL_DECL __declspec(dllexport)
#else
#define DLL_DECL __declspec(dllimport)
#endif

#include <string>
#include <queue>
#include <map>

#include <pthread.h>

#include "sdl_signals.h"

/// Пространство имён SDL
namespace SDL {

using namespace std;
////////////////////////////////////////////////////////////////////////////////
///CSDL_system - Предоставляет клиентам доступ к основным методам работы с SDL-системой.
/** Представление SDL системы с методами отправления/принятия/очередизации сигналов.
FIXME: Только однопользовательский режим.
Для работы во многопользовательском необходимо доработать:
  1. Разделить клиентскую и серверную часть CSDL_system
  2. Организовать очереди слушателей в deliver
  3. Вынести создание классов-сообщений в receive
 */
class DLL_DECL CSDL_system {
protected:

  //локирования доступа к полям класса
  pthread_mutex_t mutex;
  pthread_cond_t  cond;

protected:
  ///Очередь сигналов из SDL
  queue<ISDL_Signal*> queue_ofSignal;
  ///Связь SDL сигналов и ISDLSignal
  typedef map<const uint32_t, const ISDL_Signal*> tListeners;
  ///Слушатели-обработчики исходящих сигналов
  tListeners signalListeners;

protected:
  ///Поток SDL-системы
  pthread_t sdl_thread;
  ///Функция потока SDL-системы
  static void* thread_main(void* _sdlSystem);

public:
  /**  \brief Установить сигнал в очередь исходящих сигналов
   * Внутренне-используемый метод в серверной части объекта.
   * Вызывается из xOutEnv()
   */
  virtual void deliver(const void* _sig);
public:
  /**  \brief Старт SDL системы.
   * @param _opts строка опций запуска -для будущего использования.
   */
  virtual void start(std::string _opts);

  /** \brief Добавить слушатель сигнала в систему.
   * @param _sig обработчик
   */
  virtual void addListener(const CSDL_Signal *_sig)
  {//поиск по sig->NameNode
    signalListeners[_sig->index()] = _sig;
  };

  /** \brief Подать сигнал в систему.
   * @param _sig посылаемый сигнал
   */
  virtual void send(const CSDL_Signal *_sig);

  /**  \brief Заблокироваться и принять сигнал из системы.
   * Выделяет память для сигнала, удаление лежит на клиентском коде.
   * Запускается как правило из отдельного потока.
   * @param _timeOut  - время timeout на блокировку
   * @return Указатель на сигнал.
   */
  virtual CSDL_Signal* receive(int _timeOut = 0);

public:
  CSDL_system();
  virtual ~CSDL_system();
};

extern CSDL_system* sdlSystem;

};
