
/* Program generated by Cadvanced 4.5.0  */
#define XSCT_CADVANCED

#define SCT_VERSION_4_5
#define XENABLE_VERSION_CHECK

#include "scttypes.h"
#include "MAC_declaration.h"
#include "Synonyms.h"
#include "ctypes.h"
#include "MDB_MCFG_MLD_var.h"
#include "I_MCFG_MDB.h"
#ifdef XINCLUDE_HS_FILE
#include "vms_server.hs"
#endif
#ifdef XCTRACE
static char  xFileName[] = "C:\\prj\\amt\\cool-ts-0.60\\sdl\\VMS_SDL_system\\generation\\Untitled._DPE1298231677\\Win32_Threaded\\.\\I_MCFG_MDB.c";
#endif


/*************************************************************************
**                  #CODE directives, #BODY sections                    **
*************************************************************************/

/*************************************************************************
**                  SECTION Variables and Functions                     **
*************************************************************************/

/*****
* PACKAGE I_MCFG_MDB
* #SDTREF(SDL,C:\prj\amt\cool-ts-0.60\sdl\Common\MLD_MCFG_MDB\I_MCFG_MDB.sun,4,1,9)
******/
XCONST struct xPackageIdStruct yPacR_z_I_MCFG_MDB__I_MCFG_MDB = {xPackageEC
  xSymbTLink((xIdNode)0, (xIdNode)0), (xIdNode)&xSymbolTableIdRec
  xIdNames("I_MCFG_MDB") XCOMMON_EXTRAS xIdNames(0) XPAC_EXTRAS};

/*****
* SIGNAL InitCFG_R
* <<PACKAGE I_MCFG_MDB>>
* #SDTREF(SDL,C:\prj\amt\cool-ts-0.60\sdl\Common\MLD_MCFG_MDB\I_MCFG_MDB.sun(1),119(20,40),5,8)
******/
#ifndef XNOSIGNALIDNODE
XCONST struct xSignalIdStruct ySigR_z_I_MCFG_MDB_0_InitCFG_R = {xSignalEC
  xSymbTLink((xIdNode)0, (xIdNode)0), (xIdNode)&yPacR_z_I_MCFG_MDB__I_MCFG_MDB
  xIdNames("InitCFG_R") XCOMMON_EXTRAS, (xptrint)sizeof(XSIGNALHEADERTYPE), 0,
  0 xFreS(0) SIGCODE(InitCFG_R)
  xBreakB("#SDTREF(SDL,C:\\prj\\amt\\cool-ts-0.60\\sdl\\Common\\MLD_MCFG_MDB\\I_MCFG_MDB.sun(1),119(20,40),5,8)")
  xSigPrioPar(xDefaultPrioSignal) XSIG_EXTRAS};
#endif

/*****
* SIGNAL InitCFG_I
* <<PACKAGE I_MCFG_MDB>>
* #SDTREF(SDL,C:\prj\amt\cool-ts-0.60\sdl\Common\MLD_MCFG_MDB\I_MCFG_MDB.sun(1),119(20,40),6,8)
******/
#ifndef XNOSIGNALIDNODE
static xSignalNode ySigA_z_I_MCFG_MDB_1_InitCFG_I = (xSignalNode)0;
XCONST struct xSignalIdStruct ySigR_z_I_MCFG_MDB_1_InitCFG_I = {xSignalEC
  xSymbTLink((xIdNode)0, (xIdNode)0), (xIdNode)&yPacR_z_I_MCFG_MDB__I_MCFG_MDB
  xIdNames("InitCFG_I") XCOMMON_EXTRAS,
  (xptrint)sizeof(yPDef_z_I_MCFG_MDB_1_InitCFG_I),
  &ySigA_z_I_MCFG_MDB_1_InitCFG_I, 0 xFreS(0) SIGCODE(InitCFG_I)
  xBreakB("#SDTREF(SDL,C:\\prj\\amt\\cool-ts-0.60\\sdl\\Common\\MLD_MCFG_MDB\\I_MCFG_MDB.sun(1),119(20,40),6,8)")
  xSigPrioPar(xDefaultPrioSignal) XSIG_EXTRAS};
#endif
#ifndef XOPTSIGPARA
XCONST struct xVarIdStruct ySPaR1_z_I_MCFG_MDB_1_InitCFG_I = {xSignalParEC
  xSymbTLink((xIdNode)0, (xIdNode)0), (xIdNode)&ySigR_z_I_MCFG_MDB_1_InitCFG_I
  xIdNames(" ") XCOMMON_EXTRAS, &ySrtR_z_MAC_declaration_0_MAC,
  (tSDLTypeInfo *)&ySDL_z_MAC_declaration_0_MAC,
  xOffsetOf(yPDef_z_I_MCFG_MDB_1_InitCFG_I, Param1), (xptrint)0, (xbool)0
  XSPA_EXTRAS};
XCONST struct xVarIdStruct ySPaR2_z_I_MCFG_MDB_1_InitCFG_I = {xSignalParEC
  xSymbTLink((xIdNode)0, (xIdNode)0), (xIdNode)&ySigR_z_I_MCFG_MDB_1_InitCFG_I
  xIdNames(" ") XCOMMON_EXTRAS, &xSrtR_SDL_Octet,
  (tSDLTypeInfo *)&ySDL_SDL_Octet,
  xOffsetOf(yPDef_z_I_MCFG_MDB_1_InitCFG_I, Param2), (xptrint)0, (xbool)0
  XSPA_EXTRAS};
XCONST struct xVarIdStruct ySPaR3_z_I_MCFG_MDB_1_InitCFG_I = {xSignalParEC
  xSymbTLink((xIdNode)0, (xIdNode)0), (xIdNode)&ySigR_z_I_MCFG_MDB_1_InitCFG_I
  xIdNames(" ") XCOMMON_EXTRAS, &xSrtR_SDL_Octet,
  (tSDLTypeInfo *)&ySDL_SDL_Octet,
  xOffsetOf(yPDef_z_I_MCFG_MDB_1_InitCFG_I, Param3), (xptrint)0, (xbool)0
  XSPA_EXTRAS};
#endif

/*****
* SIGNAL Ready
* <<PACKAGE I_MCFG_MDB>>
* #SDTREF(SDL,C:\prj\amt\cool-ts-0.60\sdl\Common\MLD_MCFG_MDB\I_MCFG_MDB.sun(1),122(20,70),2,8)
******/
#ifndef XNOSIGNALIDNODE
XCONST struct xSignalIdStruct ySigR_z_I_MCFG_MDB_2_Ready = {xSignalEC
  xSymbTLink((xIdNode)0, (xIdNode)0), (xIdNode)&yPacR_z_I_MCFG_MDB__I_MCFG_MDB
  xIdNames("Ready") XCOMMON_EXTRAS, (xptrint)sizeof(XSIGNALHEADERTYPE), 0, 0
  xFreS(0) SIGCODE(Ready)
  xBreakB("#SDTREF(SDL,C:\\prj\\amt\\cool-ts-0.60\\sdl\\Common\\MLD_MCFG_MDB\\I_MCFG_MDB.sun(1),122(20,70),2,8)")
  xSigPrioPar(xDefaultPrioSignal) XSIG_EXTRAS};
#endif

/*****
* SIGNAL SetFuncs
* <<PACKAGE I_MCFG_MDB>>
* #SDTREF(SDL,C:\prj\amt\cool-ts-0.60\sdl\Common\MLD_MCFG_MDB\I_MCFG_MDB.sun(1),122(20,70),9,8)
******/
#ifndef XNOSIGNALIDNODE
static xSignalNode ySigA_z_I_MCFG_MDB_3_SetFuncs = (xSignalNode)0;
XCONST struct xSignalIdStruct ySigR_z_I_MCFG_MDB_3_SetFuncs = {xSignalEC
  xSymbTLink((xIdNode)0, (xIdNode)0), (xIdNode)&yPacR_z_I_MCFG_MDB__I_MCFG_MDB
  xIdNames("SetFuncs") XCOMMON_EXTRAS,
  (xptrint)sizeof(yPDef_z_I_MCFG_MDB_3_SetFuncs),
  &ySigA_z_I_MCFG_MDB_3_SetFuncs, 0 xFreS(0) SIGCODE(SetFuncs)
  xBreakB("#SDTREF(SDL,C:\\prj\\amt\\cool-ts-0.60\\sdl\\Common\\MLD_MCFG_MDB\\I_MCFG_MDB.sun(1),122(20,70),9,8)")
  xSigPrioPar(xDefaultPrioSignal) XSIG_EXTRAS};
#endif
#ifndef XOPTSIGPARA
XCONST struct xVarIdStruct ySPaR1_z_I_MCFG_MDB_3_SetFuncs = {xSignalParEC
  xSymbTLink((xIdNode)0, (xIdNode)0), (xIdNode)&ySigR_z_I_MCFG_MDB_3_SetFuncs
  xIdNames(" ") XCOMMON_EXTRAS, &ySrtR_z_MDB_MCFG_MLD_var_1A_ArrFuncType,
  (tSDLTypeInfo *)&ySDL_z_MDB_MCFG_MLD_var_1A_ArrFuncType,
  xOffsetOf(yPDef_z_I_MCFG_MDB_3_SetFuncs, Param1), (xptrint)0, (xbool)0
  XSPA_EXTRAS};
#endif

/*****
* SIGNAL SetGroups
* <<PACKAGE I_MCFG_MDB>>
* #SDTREF(SDL,C:\prj\amt\cool-ts-0.60\sdl\Common\MLD_MCFG_MDB\I_MCFG_MDB.sun(1),122(20,70),11,8)
******/
#ifndef XNOSIGNALIDNODE
static xSignalNode ySigA_z_I_MCFG_MDB_4_SetGroups = (xSignalNode)0;
XCONST struct xSignalIdStruct ySigR_z_I_MCFG_MDB_4_SetGroups = {xSignalEC
  xSymbTLink((xIdNode)0, (xIdNode)0), (xIdNode)&yPacR_z_I_MCFG_MDB__I_MCFG_MDB
  xIdNames("SetGroups") XCOMMON_EXTRAS,
  (xptrint)sizeof(yPDef_z_I_MCFG_MDB_4_SetGroups),
  &ySigA_z_I_MCFG_MDB_4_SetGroups, 0 xFreS(0) SIGCODE(SetGroups)
  xBreakB("#SDTREF(SDL,C:\\prj\\amt\\cool-ts-0.60\\sdl\\Common\\MLD_MCFG_MDB\\I_MCFG_MDB.sun(1),122(20,70),11,8)")
  xSigPrioPar(xDefaultPrioSignal) XSIG_EXTRAS};
#endif
#ifndef XOPTSIGPARA
XCONST struct xVarIdStruct ySPaR1_z_I_MCFG_MDB_4_SetGroups = {xSignalParEC
  xSymbTLink((xIdNode)0, (xIdNode)0), (xIdNode)&ySigR_z_I_MCFG_MDB_4_SetGroups
  xIdNames(" ") XCOMMON_EXTRAS, &ySrtR_z_MDB_MCFG_MLD_var_14_GrType,
  (tSDLTypeInfo *)&ySDL_z_MDB_MCFG_MLD_var_14_GrType,
  xOffsetOf(yPDef_z_I_MCFG_MDB_4_SetGroups, Param1), (xptrint)0, (xbool)0
  XSPA_EXTRAS};
#endif

/*************************************************************************
**                       SECTION Initialization                         **
*************************************************************************/
extern void yInit_I_MCFG_MDB (void)
{
  static int IsCalled = 0;
  int  Temp;
  YINIT_TEMP_VARS
  if (IsCalled) return;
  IsCalled = 1;
  xInsertIdNode((xIdNode)&yPacR_z_I_MCFG_MDB__I_MCFG_MDB);
  yInit_MAC_declaration();
  yInit_MDB_MCFG_MLD_var();
#ifndef XNOSIGNALIDNODE
  xInsertIdNode((xIdNode)&ySigR_z_I_MCFG_MDB_0_InitCFG_R);
#endif
#ifndef XNOSIGNALIDNODE
  xInsertIdNode((xIdNode)&ySigR_z_I_MCFG_MDB_1_InitCFG_I);
#endif
#ifndef XOPTSIGPARA
  xInsertIdNode((xIdNode)&ySPaR1_z_I_MCFG_MDB_1_InitCFG_I);
  xInsertIdNode((xIdNode)&ySPaR2_z_I_MCFG_MDB_1_InitCFG_I);
  xInsertIdNode((xIdNode)&ySPaR3_z_I_MCFG_MDB_1_InitCFG_I);
#endif
#ifndef XNOSIGNALIDNODE
  xInsertIdNode((xIdNode)&ySigR_z_I_MCFG_MDB_2_Ready);
#endif
#ifndef XNOSIGNALIDNODE
  xInsertIdNode((xIdNode)&ySigR_z_I_MCFG_MDB_3_SetFuncs);
#endif
#ifndef XOPTSIGPARA
  xInsertIdNode((xIdNode)&ySPaR1_z_I_MCFG_MDB_3_SetFuncs);
#endif
#ifndef XNOSIGNALIDNODE
  xInsertIdNode((xIdNode)&ySigR_z_I_MCFG_MDB_4_SetGroups);
#endif
#ifndef XOPTSIGPARA
  xInsertIdNode((xIdNode)&ySPaR1_z_I_MCFG_MDB_4_SetGroups);
#endif
}
