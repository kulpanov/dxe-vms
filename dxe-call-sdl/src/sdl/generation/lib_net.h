
/* Program generated by Cadvanced 4.5.0  */
#define XSCT_CADVANCED

#ifndef XX_lib_net_H
#define XX_lib_net_H

/*************************************************************************
**                SECTION Types and Forward references                  **
*************************************************************************/

/*****
* PACKAGE lib_net
* #SDTREF(SDL,C:\prj\amt\cool-ts-0.60\sdl\Common\MLLC\lib_net.sun,4,1,9)
******/
extern XCONST struct xPackageIdStruct yPacR_z_lib_net__lib_net;

extern void yInit_lib_net (void);

/*****
* PROCESS TYPE MAIN_LLCP
* <<PACKAGE lib_net>>
* #SDTREF(SDL,C:\prj\amt\cool-ts-0.60\sdl\Common\MLLC\lib_net.sun(1),212(50,85),1,1)
******/
extern XCONST XSIGTYPE yPrsS_z_lib_net_0_MAIN_LLCP[];
extern XCONST xStateIdNode yPrsT_z_lib_net_0_MAIN_LLCP[];
extern XCONST struct xPrsIdStruct yPrsR_z_lib_net_0_MAIN_LLCP;
#define yPrsN_z_lib_net_0_MAIN_LLCP  (&yPrsR_z_lib_net_0_MAIN_LLCP)
#ifdef XCOVERAGE
extern long int yPrsC_z_lib_net_0_MAIN_LLCP[];
#endif
#ifndef XNOSTARTUPIDNODE
extern XCONST struct xSignalIdStruct ySigR_z_lib_net_0_MAIN_LLCP;
#define ySigN_z_lib_net_0_MAIN_LLCP  (&ySigR_z_lib_net_0_MAIN_LLCP)
#endif
typedef struct {
    SIGNAL_VARS
    STARTUP_VARS
} yPDef_z_lib_net_0_MAIN_LLCP;
typedef yPDef_z_lib_net_0_MAIN_LLCP  *yPDP_z_lib_net_0_MAIN_LLCP;
XPROCESSDEF_H(MAIN_LLCP,"MAIN_LLCP",z_lib_net_0_MAIN_LLCP,0, \
  yVDef_z_lib_net_0_MAIN_LLCP)
#ifdef XBREAKBEFORE
#define ySym_z_lib_net_0_MAIN_LLCP  0
extern char * yRef_z_lib_net_0_MAIN_LLCP (int, xSymbolType *);
#endif
typedef struct {
    PROCESS_VARS
} yVDef_z_lib_net_0_MAIN_LLCP;
typedef yVDef_z_lib_net_0_MAIN_LLCP  *yVDP_z_lib_net_0_MAIN_LLCP;
extern void yFree_z_lib_net_0_MAIN_LLCP (void *);

/*************************************************************************
**                #CODE directives, #HEADING sections                   **
*************************************************************************/
#endif
