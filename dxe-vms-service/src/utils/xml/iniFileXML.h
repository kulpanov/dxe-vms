// IniFile.cpp:  Implementation of the CIniFileXML class.
// Written by:   Adam Clauss
// Email: cabadam@tamu.edu
// You may use this class/code as you wish in your programs.  Feel free to distribute it, and
// email suggested changes to me.
//
// Rewritten by: Shane Hill
// Date:         21/08/2001
// Email:        Shane.Hill@dsto.defence.gov.au
// Reason:       Remove dependancy on MFC. Code should compile on any
//               platform. Tested on Windows/Linux/Irix
//////////////////////////////////////////////////////////////////////

#ifndef CIniFileXML_H
#define CIniFileXML_H

using namespace std;

// C++ Includes
#include <string>
#include <vector>

#include "tinystr.h"
#include "tinyxml.h"

// C Includes
#include <stdlib.h>

#define MAX_KEYNAME    128
#define MAX_VALUENAME  128
#define MAX_VALUEDATA 2048

class CIniFileXML  
{
private:
//  bool   caseInsensitive;
  string path;
  TiXmlDocument doc;
//  struct key {
//    vector<string> names;
//    vector<string> values; 
//    vector<string> comments;
//  };
//  vector<key>    keys; 
//  vector<string> names; 
//  vector<string> comments;
  string CheckCase( string s) const;

public:
  enum errors{ noID = -1};
  CIniFileXML( );
  CIniFileXML( string const iniPath);
  
  virtual ~CIniFileXML()                            {}

//  // Sets whether or not keynames and valuenames should be case sensitive.
//  // The default is case insensitive.
//  void CaseSensitive()                           {caseInsensitive = false;}
//  void CaseInsensitive()                         {caseInsensitive = true;}

  TiXmlDocument GetDocument()                    {return doc;}
   
  void Path(string const newPath)                {path = newPath;}
  
  /*Задать новый путь к XML-файлу
   * @param newPath новый путь
   */
  void SetPath(string const newPath)             {Path( newPath);}
  
  /*Получить текущий путь до XML-файла
   */
  string Path() const                            {return path;}
  
/*Разобрать XML-документ
 * @param minsize минимальный размер(пока не используется)
 * @return результат разбора
 */
  bool ReadFile(int minsize=0);

/*Прочитать из строки XML-документ
 * @param XML строка
 * @return результат разбора
 */  
  bool ReadString(const char* ini_str);
  
/*Записать XML-документ по текущему заданному пути
 * @param minsize минимальный размер(пока не используется)
 * @return результат сохранения
 */  
  bool WriteFile(int minsize=0);
  
/*Записать XML-документ по текущему заданному пути из данной XML-строки
 * @param ini_str XML-строка
 * @return результат разбора и сохранения
 */   
  bool WriteFile(string &ini_str);
  
/*Очистить текущий XML-документ
 */
  void Erase();
  void Clear()                                   {Erase();}
  void Reset()                                   {Erase();}

/*Получить элемент со всеми атрибутами по пути
 * @param _elementPath путь вида /CNC/Params/Plasma/BurnTime
 * @return XML-элемент
 */
  TiXmlElement* Get(const char* _elementPath);

/*Получить атрибут элемента по пути к нему
 * @param _elementPath путь вида /CNC/Params/Plasma/BurnTime
 * @param _attributeName название атрибута
 * @param _defValue значение по умолчанию
 * @return полученное значение
 */
  string GetAttribute(const char* _elementPath, const char* _attributeName, const char* _defValue = "");


/*Получить значение атрибута descr
 * @param _elementPath путь вида /CNC/Params/Plasma/BurnTime
 * @param _defValue значение по умолчанию
 * @return полученное значение
 */  
  string GetDescr(const char* _elementPath, const char* _defValue = "");

/*Получить значение атрибута value
 * @param _elementPath путь вида /CNC/Params/Plasma/BurnTime
 * @param _defValue значение по умолчанию
 * @return полученное значение
 */  
  string GetValue(const char* _elementPath, const char* _defValue = "");

/*Получить значение атрибута value в int
 * @param _elementPath путь вида /CNC/Params/Plasma/BurnTime
 * @param _defValue значение по умолчанию
 * @return полученное значение
 */    
  int GetValueI(const char* _elementPath, int _defValue = 0);
  
 /*Получить значение атрибута value в double
 * @param _elementPath путь вида /CNC/Params/Plasma/BurnTime
 * @param _defValue значение по умолчанию
 * @return полученное значение
 */    
  double GetValueF(const char* _elementPath, double _defValue = 0.0);

/*Получить значение атрибута max
 * @param _elementPath путь вида /CNC/Params/Plasma/BurnTime
 * @param _defValue значение по умолчанию
 * @return полученное значение
 */  
  string GetMaxValue(const char* _elementPath, const char* _defValue = "");
  
/*Получить значение атрибута max в int
 * @param _elementPath путь вида /CNC/Params/Plasma/BurnTime
 * @param _defValue значение по умолчанию
 * @return полученное значение
 */    
  int GetMaxValueI(const char* _elementPath, int _defValue = 0);  

/*Получить значение атрибута max в double
 * @param _elementPath путь вида /CNC/Params/Plasma/BurnTime
 * @param _defValue значение по умолчанию
 * @return полученное значение
 */    
  double GetMaxValueF(const char* _elementPath, double _defValue = 0.0);  


/*Получить значение атрибута min
 * @param _elementPath путь вида /CNC/Params/Plasma/BurnTime
 * @param _defValue значение по умолчанию
 * @return полученное значение
 */  
  string GetMinValue(const char* _elementPath, const char* _defValue = "");
  
/*Получить значение атрибута min в int
 * @param _elementPath путь вида /CNC/Params/Plasma/BurnTime
 * @param _defValue значение по умолчанию
 * @return полученное значение
 */    
  int GetMinValueI(const char* _elementPath, int _defValue = 0);  

/*Получить значение атрибута min в double
 * @param _elementPath путь вида /CNC/Params/Plasma/BurnTime
 * @param _defValue значение по умолчанию
 * @return полученное значение
 */    
  double GetMinValueF(const char* _elementPath, double _defValue = 0.0);  


/*Задать атрибут элемента по пути к нему
 * @param _elementPath путь вида /CNC/Params/Plasma/BurnTime
 * @param _attributeName название атрибута
 * @param _attributeValue значение атрибута
 * @return смогли записать или нет
 */
  bool SetAttribute(const char* _elementPath, const char* _attributeName, const char* _attributValue);

/*Задать атрибут value элемента по пути к нему
 * @param _elementPath путь вида /CNC/Params/Plasma/BurnTime
 * @param _attributeValue значение атрибута
 * @return смогли записать или нет
 */
  bool SetValue(const char* _elementPath, const char* _attributValue);

/*Задать атрибут value элемента по пути к нему
 * @param _elementPath путь вида /CNC/Params/Plasma/BurnTime
 * @param _attributeValue int-значение атрибута
 * @return смогли записать или нет
 */
  bool SetValueI(const char* _elementPath, int _attributValue);
  
 /*Задать атрибут value элемента по пути к нему
 * @param _elementPath путь вида /CNC/Params/Plasma/BurnTime
 * @param _attributeValue double-значение атрибута
 * @return смогли записать или нет
 */
  bool SetValueF(const char* _elementPath, double& _attributValue);

//??? ???? ?????
  int CheckMailOnOff(const string &_number);

    TiXmlElement* FindAbonent(const string &_number);

    int DeleteFirstMessage(const string &_number);

    int DeleteFirstMessage(TiXmlElement* _abonent);

    //удалить все прочитанные
    int DeleteAllHasReadMessages(const string &_number, vector<string> &_files);

    //удалить все
    int DeleteAllMessages(const string &_number, vector<string> &_files);

    int GetFirstMessage(const string &_number, string& _source, string& _path);

    int GetFirstNotReadMessage(const string &_number, string& _source, string& _path);

    int MarkFirstMessage(const string &_number);

    int ResetMarks(const string &_number);

    int AddElement(const string &_number, const string &_source, const string &_path, int _buf_size);
  
  ///Рапечатать содержимое XML-документа
  void Print() {doc.Print();};
};

#endif
