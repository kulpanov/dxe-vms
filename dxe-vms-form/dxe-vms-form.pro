TEMPLATE = app
TARGET = vmsform

QT += core \
    gui \
    xml \
    network
HEADERS += src/mainwindow.h \
    src/utils/CXMLReader.h \
    src/utils/utils.h \
    src/pagecontrolprop.h \
    src/utils/CXAdapterWidget.h \
    src/aboutForm.h
SOURCES += src/mainwindow.cpp \
    src/main.cpp \
    src/utils/CXMLReader.cpp \
    src/pagecontrolprop.cpp \
    src/aboutForm.cpp \
    src/utils/CXAdapterWidget.cpp \
    src/utils/utils.cpp
FORMS += forms/mainwindow.ui \
    forms/pagecontrolprop.ui \
    forms/CXAdapterWidget.ui
    
RESOURCES += forms/resource.qrc
DESTDIR = ../../_vms/install
QMAKE_CXXFLAGS += -std=gnu++11 

win32:
{
  RC_FILE = forms/myapp.rc
}
