/** \brief Различные утилиты, объявления
 * utils.h
 *
 *  Created on: 05.09.2011
 *  \author Alexander Kulpanov, post@kulpanov.ru
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <string>
#include <stdlib.h>
#include <cstdint>

using namespace std;
////////////////////////////////////////////////////////////////////////////////
//строка
///Класс-обёртка для std::string
class CString: public string //basic_string<char, char_traits<char>, allocator<char> >
{
  ///ук-ль на данные, для отладочных целей
  const char* s;
public:
  const char* toChar() const {
    return this->c_str();
  }

  int32_t toInt(bool _hex = false) const {
    //TODO: реализовать atoh
    //    if(_hex)
    //      return atoh(this->c_str());
    //    else
    return atoi(this->c_str());
  }

  double toDouble() const {
    return atof(this->c_str());
  }

  operator const char*() const {
    return toChar();
  }

  operator int() const {
    return toInt();
  }

  operator double() const {
    return toDouble();
  }

  operator string() const {
    return toString();
  }

  const string toString() const {return (string)(*this);};

  CString& fromDouble(const double& _d) {
    char buf[20];
    sprintf(buf, "%f", _d);
    assign(buf);
    return *this;
  }

  CString& fromInt(const int _i, bool _hex = false) {
    char buf[20];
    if (!_hex)
      sprintf(buf, "%i", _i);
    else
      sprintf(buf, "0x%x", _i);
    assign(buf);
    return *this;
  }

  CString& format(const char* _frmt, ...) {
    //TODO:сделать format
    //    va_list arglist;
    //    va_start( arglist, _frmt );
    //    vprintfs(s, _frmt, arglist );
    //    va_end( arglist );

    return *this;
  }

  //  CString& operator +=(int _add){
  //    char buf[0x10];
  //    itoa(_add, buf, 10);
  //    *this += buf;
  //    return *this;
  //  }

  CString()
  :string(), s(string::c_str())
  {}

  CString(const char* _str)
  :string(_str), s(string::c_str())
  {}
  //  CString(const CString& _str)
  //    :string((string)_str)
  // {  };

  CString(const string& _str)
  :string(_str), s(string::c_str())
  {}

  CString(const int32_t _int, const bool _hex = false)
  :string("0"), s(string::c_str())
  {
    fromInt(_int, _hex);
  }

  CString(const uint32_t _int, const bool _hex = false)
  :string("0"), s(string::c_str())
  {
    fromInt(_int, _hex);
  }

  CString(const double& _double)
  :string("0"), s(string::c_str())
  {
    fromDouble(_double);
  }

};

extern const CString nullStr;
//#define CCHAR (const char*)

/** \brief Подсчитать кол-во файлов в каталоге
 * Считает всё, файлы и подкаталоги, пропускает файлы, что начинаются с '.' - точки
 * @param _dirname имя каталога
 * @return Число файлов/каталогов
 */
int getCountOfFilesInDir(const char* _dirname = ".");

#endif /* UTILS_H_ */
