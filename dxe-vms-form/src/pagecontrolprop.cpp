#include "pagecontrolprop.h"
#include "mainwindow.h"
#include "utils/CXMLReader.h"
#include "ui_pagecontrolprop.h"
#include <QtGui/QFileDialog>
#include <QtGui/QMessageBox>

#include "utils/CXAdapterWidget.h"

void pageControlProp::onItemClick(QTreeWidgetItem* item, int column) {
  if (item->checkState(0) == Qt::Unchecked) {
    QTreeWidgetItem* curItem = NULL;
    for (int i = 0; i < ui->mAdaptersList->topLevelItemCount(); i++) {
      curItem = ui->mAdaptersList->topLevelItem(i);

      if (curItem != item && curItem->checkState(0) == Qt::Checked)
        curItem->setCheckState(0, Qt::Unchecked);
    }

    item->setCheckState(0, Qt::Checked);
    mIndex = ui->mAdaptersList->indexOfTopLevelItem(item);
  } else {
    mIndex = -1;
    item->setCheckState(0, Qt::Unchecked);
  }
}

void pageControlProp::addAdapters() {
  CXMLReader* xmlReader = new CXMLReader("./settings.xml");

  QString curIP = xmlReader->GetAttribute("Params/General/myIP", "value",
      "127.0.0.1");
  QString curMask = xmlReader->GetAttribute("Params/General/myNet", "value",
      "127.0.0.1");

  auto interfaces = QNetworkInterface::allInterfaces();
  auto it_interf = interfaces.begin();
  for (; it_interf != interfaces.end(); ++it_interf) {

    if (!(((*it_interf).flags() & QNetworkInterface::IsUp)
        && ((*it_interf).flags() & QNetworkInterface::IsRunning)
        && !((*it_interf).flags() & QNetworkInterface::IsLoopBack)))
      continue;

    //      && aInterface.addressEntries().last().ip() != QHostAddress::LocalHost)
    //  if (!aInterface.addressEntries().isEmpty() && aInterface.addressEntries().last().ip() != QHostAddress::LocalHost)
    auto addresses = (*it_interf).addressEntries();
    if (addresses.isEmpty())
      continue;
    auto it_addr = addresses.begin(); //begin()==ipv6 address
    for (; it_addr != addresses.end(); it_addr++) {
      if ((*it_addr).ip().protocol() != QAbstractSocket::IPv4Protocol)
        continue;

      addr.push_back(*it_addr);
      CXAdapterWidget* widget = new CXAdapterWidget(
          (*it_interf).humanReadableName(), *it_addr);

      QTreeWidgetItem* newItem = new QTreeWidgetItem(ui->mAdaptersList);
      newItem->setFlags(
          Qt::ItemIsUserCheckable | Qt::ItemIsSelectable | Qt::ItemIsEnabled);

      if (widget->mIP == curIP && widget->mMask == curMask) {
        newItem->setCheckState(0, Qt::Checked);
        mIndex = addr.count() - 1;
      } else
        newItem->setCheckState(0, Qt::Unchecked);

      ui->mAdaptersList->addTopLevelItem(newItem);
      ui->mAdaptersList->setItemWidget(newItem, 0, widget);

    }
  }
  delete xmlReader;
  return;
}

void pageControlProp::ReadParameters(){
	CXMLReader* xmlReader = new CXMLReader("./settings.xml");
//    ui->ipAdressEdit->setText(xmlReader->GetAttribute("Params/General/myIP","value","0.0.0.0"));
//    ui->maskEdit->setText(xmlReader->GetAttribute("Params/General/myNet","value","0.0.0.0"));
    ui->maxCountMessageEdit->setText(xmlReader->GetAttribute("Params/Paths/maxCountMsg","value","30"));
    ui->maxCountBoxEdit->setText(xmlReader->GetAttribute("Params/Paths/maxCountMailBox","value","30"));
    ui->maxLenghtEdit->setText(xmlReader->GetAttribute("Params/Paths/maxRecordMsg","value","30"));
    ui->writeTheme->setText(xmlReader->GetAttribute("Params/Paths/typeRec","value","30"));
    ui->listenTheme->setText(xmlReader->GetAttribute("Params/Paths/typePlay","value","30"));
//    ui->deleteTheme->setText(xmlReader->GetAttribute("Params/Paths/typeDelete","value","30"));
//    ui->clearTheme->setText(xmlReader->GetAttribute("Params/Paths/typeClear","value","30"));
    //приводим в начальное состояние
    if(xmlReader->GetAttribute("Params/Events","check",0).toInt()){
        ui->eventBox->setEnabled(true);
        ui->messageCheck->setChecked(true);
    }else
      ui->eventBox->setEnabled(false);
    //выставляем время
    ui->timeBedin->setTime(QTime::fromString(xmlReader->GetAttribute("Params/Events/timeFrom","value","07:00"),"hh:mm"));
    ui->timeEnd->setTime(QTime::fromString(xmlReader->GetAttribute("Params/Events/timeTo","value","15:00"),"hh:mm"));
    ui->intervalSpin->setValue(xmlReader->GetAttribute("Params/Events/Interval","value","15").toInt());
    ui->intervalSpin->setMinimum(1);
    ui->intervalSpin->setMaximum(360);
    ui->lenghtSpin->setValue(xmlReader->GetAttribute("Params/Events/Length","value","20").toInt());
    ui->lenghtSpin->setMinimum(5);
    ui->lenghtSpin->setMaximum(60);

    auto cbValue = xmlReader->GetAttribute("Params/Options/autoStart","value","1");
    ui->cbAutoStart->setChecked(cbValue!="0");

    delete xmlReader;
};

void pageControlProp::SaveParameters(){
  switch(ui->tabWidget->currentIndex()){
    case 0 :
      SaveGeneralParameters();
    break;
    case 1:
      SaveRouteParameters();
    break;
  };
};

void pageControlProp::SaveGeneralParameters(){
  bool needRestart = false;
  if (((MainWindow*)parent)->serverVMS.state() != QProcess::NotRunning){
    //останавливаем сервер
    //((MainWindow*)parent)->StopCore();
    //needRestart = true;
  }
  if (mIndex == -1) {
    QMessageBox::question(this, tr("DXE-VMS"),
        QString().fromUtf8("Необходимо выбрать интерфейс.\n"), QMessageBox::Ok);
    return;
  }
  CXMLReader* xmlReader = new CXMLReader("./settings.xml");
  QString s = addr.at(mIndex).ip().toString();
  if (xmlReader->GetAttribute("Params/General/myIP", "value", "127.0.0.1")
      != s) {
    xmlReader->SetAttribute("Params/General/myIP", "value", s);
    xmlReader->SetAttribute("Params/General/myNet", "value",
        addr.at(mIndex).netmask().toString());
    needRestart = true;
  }

  xmlReader->SetAttribute("Params/Paths/maxRecordMsg","value",ui->maxLenghtEdit->text());
  xmlReader->SetAttribute("Params/Paths/maxCountMsg","value",ui->maxCountMessageEdit->text());
  xmlReader->SetAttribute("Params/Paths/maxCountMailBox","value",ui->maxCountBoxEdit->text());
  xmlReader->SetAttribute("Params/Paths/typeRec","value",ui->writeTheme->text());
  xmlReader->SetAttribute("Params/Paths/typePlay","value",ui->listenTheme->text());

  xmlReader->SetAttribute("Params/Events","check",(ui->messageCheck->checkState() == Qt::Checked)?"1":"0");
  xmlReader->SetAttribute("Params/Events/Interval","value",ui->intervalSpin->text());
  xmlReader->SetAttribute("Params/Events/Length","value",ui->lenghtSpin->text());
  xmlReader->SetAttribute("Params/Events/timeFrom","value",ui->timeBedin->text());
  xmlReader->SetAttribute("Params/Events/timeTo","value",ui->timeEnd->text());
  xmlReader->SetAttribute("Params/Options/autoStart","value",ui->cbAutoStart->checkState()==0?"0":"1");
  xmlReader->WriteFile();
  delete xmlReader;
  if(needRestart){
    QMessageBox::warning(this, tr("DXE-VMS"),
        QString().fromUtf8("Сетевые настройки VMS были изменены.\n"
            "Необходимо перезапустить сервер"), QMessageBox::Ok,
        QMessageBox::Ok);
    //стартуем сервер
    //((MainWindow*)parent)->StartCore();
  };

};

void pageControlProp::SaveRouteParameters(){
  //Заполняем таблицу
//  int countElem = xmlReader->GetCountElement("Route_Table");
//
//  for(int i = 0; i < countElem; i++){
//    xmlReader->SetAttribute(QString("Params/Route_Table/param" + QString("%1").arg(i)).toStdString().c_str(), "destIP", routeTable->item(i, 1)->text().toStdString().c_str());
//    xmlReader->SetAttribute(QString("Params/Route_Table/param" + QString("%1").arg(i)).toStdString().c_str(), "gateLocal", routeTable->item(i, 2)->text().toStdString().c_str());
//    xmlReader->SetAttribute(QString("Params/Route_Table/param" + QString("%1").arg(i)).toStdString().c_str(), "gateNAT", routeTable->item(i, 3)->text().toStdString().c_str());
//  };

}

void pageControlProp::CreateWidgets(){
  addAdapters();

  //создаем виджет с кнопками управления, у нас только применить
  applButton = new QPushButton(QString::fromUtf8("Применить"));
  okButton = new QPushButton(QString::fromUtf8("Закрыть"));
  ui->buttonBox->addButton(applButton, QDialogButtonBox::ActionRole);
  ui->buttonBox->addButton(okButton, QDialogButtonBox::ActionRole);
  //обработчики
  QObject::connect(applButton, SIGNAL(released()), (QObject*)this, SLOT(OnApplyButton()));
  QObject::connect(okButton, SIGNAL(released()), (QObject*)this, SLOT(OnOkButton()));
  QObject::connect(ui->bApplyLicense, SIGNAL(released()), this, SLOT(OnApplyLic()));
  QObject::connect(ui->mAdaptersList, SIGNAL(itemClicked(QTreeWidgetItem*, int)),
      SLOT(onItemClick(QTreeWidgetItem*, int)));

  //подписываем закладки
  ui->tabWidget->setTabText(0,QString::fromUtf8("Общие параметры"));
  //ui->tabWidget->setTabText(1,QString::fromUtf8("Параметры соединений"));
}

pageControlProp::pageControlProp(QWidget *parent) :
    QDialog(parent),parent(parent)
    ,ui(new Ui::pageControlProp)
{
    ui->setupUi(this);

    ReadParameters();
    CreateWidgets();
}

pageControlProp::~pageControlProp()
{
    delete ui;
}

void pageControlProp::on_messageCheck_stateChanged(int _state)
{
    if(_state == Qt::Checked){
      ui->eventBox->setEnabled(true);
      ui->messageCheck->setChecked(true);
    }else
      ui->eventBox->setEnabled(false);
}

void pageControlProp::OnApplyButton(){
    SaveParameters();
};

void pageControlProp::OnOkButton(){
    close();
};

QWidget *CPropDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &/* option */,
																		 const QModelIndex &/* index */) const
{
    QLineEdit *editor = new QLineEdit(parent);
    editor->setInputMask("999.999.999.999");
    editor->setEnabled(true);
    return editor;
}

void
pageControlProp::OnApplyLic ()
{
  QString fileName = QFileDialog::getOpenFileName(this, QString::fromUtf8("Открыть файл лицензии"),
                                                   "",
																									 QString::fromUtf8("LicenseFiles (*.key)"));
  if(fileName.size() == 0) return;
  QFile::remove("./license.key");
  QFile::copy(fileName, "./license.key");
//
//  CXMLReader lic(fileName);
//  QString licString = lic.GetAttribute("General/licenseString", "value", "nostring");
//  CXMLReader cfg("./settings.xml");
//  cfg.SetAttribute("Params/General/licenseString", "value", licString);
//  cfg.WriteFile();
}
