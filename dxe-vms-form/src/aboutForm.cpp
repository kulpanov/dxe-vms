/*
 * aboutForm.cpp
 *
 *  Created on: 23.06.2011
 *      Author: tsvet
 */

#include <QtGui>


#include "aboutForm.h"
#include "utils/utils.h"

//#include "../../vms.server/src/define.h"
///Номер и дата версии
#define VERSION_SHORT "DXE-VMS-K01.17"

#define VERSION VERSION_SHORT" " __DATE__" "__TIME__

CAboutForm::CAboutForm(QWidget *_parent, const QRect &_pos) : QDialog()
{

  if (this->objectName().isEmpty())
    this->setObjectName(QString::fromUtf8("about"));
  this->setWindowTitle(QString::fromUtf8("О программе"));
  this->setGeometry(_pos);
  this->setFixedSize(200, 120);//this->width(),this->height());
  QVBoxLayout* v_layout = new QVBoxLayout();
  QLabel* l_name =  new QLabel(QString::fromUtf8("Сервис голосовой почты\n(DXE-VMS)"));
  l_name->setFixedHeight(40);
  l_name->setFont(*header_font);
  l_name->setAlignment(Qt::AlignCenter);
  v_layout->addWidget(l_name,0,Qt::AlignTop);
  //l_name->width()
    this->setFixedSize(300, 120);

  QString version = QString::fromUtf8(" версия " VERSION_SHORT " от\n");
  version += __DATE__ ;
  version += " ";
  version += __TIME__ ;

  QLabel* l_name2 =  new QLabel(version);
  l_name2->setFixedHeight(40);
  //l_name2->setFont(*title_font);
  l_name2->setAlignment(Qt::AlignCenter);
  v_layout->addWidget(l_name2,0,Qt::AlignTop);


//  QLabel* l_version =  new QLabel(QString::fromUtf8("Версия\n"VERSION));
//  l_version->setAlignment(Qt::AlignCenter);
//  l_version->setFixedHeight(30);
//  v_layout->addWidget(l_version,0,Qt::AlignTop);

  QLabel* l_adress =  new QLabel(QString::fromUtf8("www.amtelecom.ru"));
  l_adress->setAlignment(Qt::AlignCenter);
  l_adress->setFixedHeight(30);
  v_layout->addWidget(l_adress,0,Qt::AlignTop);

  v_layout->insertStretch(5);

  this->setLayout(v_layout);
  //about->append("<h1>Голосовая почта</h1>Версия VMS 0.1<br/> www.amtelecom.ru");

}
