/** \brief Реализация методов CService_Call_in обработки входящих звонков и сценариев.
 * callIn_scripts.cpp
 *
 *  Created on: 14.04.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */
#include <time.h>
#include <vector>
#include <string>

#include "define.h"
#include "vms_signals.h"
#include "CService_Call.h"
#include <windows.h>

namespace AMT {
using namespace std;

///Обработать входящий вызов
void CService_Call::processCallOut(const CString& _digiD,
    const CString& _digiN, const IP& _s_ip) {

//  int digiD = atoi(_digiD.c_str());
//
//без проверки исх сценария
//  if (digiD == vms.typeNotify) {
//    //...
//  } else {
//    LOG1(WARN, "VMS:processCallOut(%d):the unknown scenario\n", id);
//    return; //неизвестный сценарий, игнорировать
//  }

  //проверить по XML доступность абонента _digiD на операцию _digiN
  CString dpath = "abonents/" + _digiN;
  CString fpath = dpath + "/abonent.xml";
  int next_Step = 0;

  try {
	  rtp_out = 0;
    //исходящее соединение - дать запрос на исходящее
    CVMS_Signal_Setup_out setup(id, _digiN, _digiD, _s_ip);
    vms.send(&setup);

    //ищем абонента, соединяем, звоним: запустить timeout
    timespec abstime;
    abstime.tv_sec = time(NULL) + vms.notifying_callTime;// sec of timeout
    abstime.tv_nsec = 0;
    //ожидаем wr_sender_table
    LOCK(&lock);
    int res = pthread_cond_timedwait(&cond, &lock, &abstime);
    if (res == ETIMEDOUT) {
      LOG1(INFO, "VMS:notify_script(%d): timeout of setup\n", id);
      //timeout, неудачно, выйти
      next_Step = -1;

    } else if (res == EOK) {

      //внешняя команда
      if (_release == mailBox.cmd) {
        //внешний отбой, прекратить вещать
        next_Step = -1;

      }
      if (_connect == mailBox.cmd) {
        //соединили - след шаг
        next_Step = 1;
      }
      mailBox.cmd = _ok;
    } else {
      LOG2(ERRR, "VMS:processCallOut(%d):pthread_cond_timedwait, code=%d\n", id, res);
      next_Step = -2; //проблемы? след шаг
    }
  } catch (P_EXC e) {
    E_CATCH(e);
    //искл ситуация, отбой
    next_Step = -2;
  }
  REPLYandUNLOCK(&cond, &lock);
  if (0 >= next_Step) {
    //неудачные пред шагм, закончить звонок!
    doEndOfCall();
    return;
  }

  try {
    //продолжаем
    BeforeCall(_digiN);

    {
      string str;
      LOG4(3, "VMS:CallOut(%d):gw=%s, rtp_out=%d, rtp_in=%d\n", id,
          rtp_gw.toString(str).c_str(), rtp_out, rtp_in);

      LOG3(3, "VMS:CallOut(%d):digiD=%s, digiN=%s\n", id, _digiD.c_str(), _digiN.c_str());
    }

    //сообщить, что соед установили
    playVoiceMsg(vms.voice_msg_OnConnect);

   // if (vms.typeNotify == digiD) {
      //уведомить
      notify_script(_digiN, _s_ip);
//    } else {
//      LOG1(WARN, "VMS:Invite(%d):ignore\n", id);
//    }

  } catch (P_EXC e) {
    E_CATCH(e);
    //искл ситуация, отбой
  }
  //завершить звонок
  doEndOfCall();
  AfterCall(_digiN);
}


//Исходящее соединение!
void CService_Call::notify_script(const CString& _digiN, const IP& _s_ip){
  LOG1(TSTL1, "VMS:notify_script(%d): started\n", id);

  Sleep(500);
  int next_Step = playVoiceMsg(vms.voice_msg_Notify);
  if(0 >= next_Step){
    doEndOfCall();
    return;
  }
  //проиграть только не прочтённые сообщенийя
  play_script(_digiN, true);

//  int next_Step = 0;
//  TiXmlElement* child; TiXmlElement* abonent;
//
//  //  try{
//  CIniFileXML abo("abonents/" + digiN + "/abonent.xml");
//  if (false == abo.ReadFile(10)) {
//    LOG2(ERRR, "VMS:notify_script(%d)::Unable to perform abo.ReadFile(): %s\n", id, abo.Path().c_str());
//    goto L_empty;
//  }
//
//  abonent = abo.FindAbonent(digiN);
//  if(NULL == abonent){
//    LOG2(TSTL1, "VMS:notify_script(%d):no abonent: %s\n", id, digiN.c_str());
//    goto L_empty;
//  }
//  child = ((TiXmlElement*)abonent->FirstChild());
//  if(NULL == child){
//    LOG2(TSTL1, "VMS:notify_script(%d):no child: %s\n", id, digiN.c_str());
//    goto L_empty;
//  }
//
//  do{
//    //шаг1: открыть файл в очередь и проиграть сообщение
//    LOG1(TSTL1, "VMS:notify_script(%d):Step1: playing\n", id);
//    const char* path = child->Attribute("path");
//
//    if(NULL == path){
//      LOG1(WARN, "VMS:notify_script(%d): no path\n", id);
//      //goto L_error;
//      continue;
//    }
//    LOG1(TSTL0, "VMS:notify_script(%d):loading\n", id);
//    {
//      tVoiceMsg q_rtp_out(path);
//      //tVoiceMsg::Load_qmsg(q_rtp_out, path);
//      LOG2(TSTL0, "VMS:notify_script(%d):end of loading %d\n", id, q_rtp_out.size());
//      next_Step = playVoiceMsg(q_rtp_out);
//    }
//    LOG1(TSTL0, "VMS:notify_script(%d):end of playing\n", id);
//    if (0 > next_Step) {
//      //положили трубку
//      goto L_endOfCall;
//    }
//    //прослушали полностью - пометить как прочитано
//    child->SetAttribute("has_read", "1");
//
//    LOCK(&lock);
//    abo.WriteFile(100);
//    UNLOCK(&lock);
//
//    child = child->NextSiblingElement();
//    if(NULL == child)
//      continue;//нет следующего
//
//    //шаг 2: проиграть "след сообщение"
//    LOG1(TSTL1, "VMS:notify_script(%d):Step2: next message\n", id);
//    next_Step = playVoiceMsg(vms.voice_msg_NextMessage);
//    if (0 > next_Step) {
//      //положили трубку
//      goto L_endOfCall;
//    }
//
//
//  }while(NULL != child);
//
//  {
//    LOG1(TSTL1, "VMS:play_script(%d):Step3: request for final action, msg after all\n", id);
//    tVoiceMsg& voice_AfterPlay = vms.voice_msg_AfterPlay;
//
//    //Спрашиваем: отметить/удалить сообщения как прослушенные?
//    next_Step = playVoiceMsg(voice_AfterPlay, _SIP_INFO );
//    if(0 <= next_Step)
//      next_Step = playVoiceMsg(vms.voice_msg_Silence,  _SIP_INFO );// просто ждём и слушаем тишину
//
//    if(-10 == next_Step){
//      //Абонент ответил утвердительно - удалить все прочтённые
//      delete_script(digiN);
//    }
//  }
//
//  //  }catch(int res){
//  //
//  //  }
//  L_endOfCall:
//  LOG1(TSTL1, "VMS:play_script(%d): finished\n", id);
//  return ;
//
//  L_empty:
//  playVoiceMsg(vms.voice_msg_Empty);// пустой ящик/нету абонента
//  return ;
}

}

