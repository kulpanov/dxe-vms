//============================================================================
// Name        : main.cpp
// Author      : Kulpanov
// Version     :
// Copyright   : Your copyright notice
// Description : main file of project
//============================================================================

#include <iostream>
#include <stdio.h>
#include <typeinfo>
#include <unistd.h>

#include "define.h"
#include "debug.h"

#include "CVoiceMailService.h"
#include <pthread.h>
#include "utils/netutils.h"
#include "testes/tests.h"

#ifdef WITH_CMD_THREAD
pthread_t cmd_thread;
extern void* thread_cmd2(void* _param);
#endif

////////////////////////////////////////////////////////////////////////////////
/**main -функция приложения
 *
 * @return
 */
int main(int argc, char** argv) {
  //иницилизировать подсистему отладки и логирования
  AMT::init_debug(2, "vms", NULL
#ifdef DEBUG
														,NULL);
#else
														,"logs/dxe");
#endif
//if need to debug
//  testVoiceQueue();
//  return 0;

  try{
    //основной объект VMService, создать
    AMT::CVoiceMailService& vmservice = AMT::CVoiceMailService::Create();
    //и запустить
    vmservice.start("");
  }catch(P_EXC e){
    E_CATCH2(e);
  }

  Sleep(5000);

//для нужд отладки
#ifdef WITH_CMD_THREAD
  thread_cmd2(NULL);
//  thread_cmd2(NULL);
//  thread_cmd2(NULL);
//  thread_cmd2(NULL);
#endif
  //закрыть этот поток
  pthread_exit(NULL);
  return 0;
}

