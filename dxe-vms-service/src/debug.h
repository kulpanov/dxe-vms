/**debug.h
 * Отладочные макросы и журналирование работы
 *  Created on: 17.01.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */
#ifndef DEBUG_H_
#define DEBUG_H_

#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <exception>
#include <string>
#include "define.h"

//extern int log_level;
//extern FILE* log;
//extern void init_debug(int _debug_level);

#define DEVL0   1
#define DEVL1   2
#define TSTL0   DEVL1
#define TSTL1   3
#define INFO    4
#define WARN    5
#define ERRR    6
#define ALRM    7

char* LogTime(char* _str);

#define IFLOG(level)  if(AMT::log_level<=level)

#define LOG(level, p1)\
  IFLOG(level)fprintf(AMT::log_file, (level>WARN)? #level"%s":"%s", p1);


#define LOG0 LOG
#define LOG1(level, format, p1) CLOG1(level, format, this, p1)
#define CLOG1(level, format, p0, p1)\
		IFLOG(level) AMT::flogf((FILE*)AMT::log_file,  #level":%s:%s:" format, AMT::getTimeOfDay(p0).c_str(), AMT::programName, p1)

#define LOG2(level, format, p1, p2)  CLOG2(level, format, this, p1, p2)
#define CLOG2(level, format, p0, p1, p2)\
		IFLOG(level) AMT::flogf((FILE*)AMT::log_file,  #level":%s:%s:" format, AMT::getTimeOfDay(p0).c_str(), AMT::programName, p1, p2)

#define LOG3(level, format, p1, p2, p3) CLOG3(level, format, this, p1, p2, p3)
#define CLOG3(level, format, p0, p1, p2, p3)\
		IFLOG(level) AMT::flogf((FILE*)AMT::log_file,  #level":%s:%s:" format, AMT::getTimeOfDay(p0).c_str(), AMT::programName, p1, p2, p3)

#define LOG4(level, format, p1, p2, p3, p4) CLOG4(level, format, this, p1, p2, p3, p4)
#define CLOG4(level, format, p0, p1, p2, p3, p4)\
		IFLOG(level) AMT::flogf((FILE*)AMT::log_file,  #level":%s:%s:" format, AMT::getTimeOfDay(p0).c_str(), AMT::programName, p1, p2, p3, p4)

#define LOG5(level, format, p1, p2, p3, p4, p5) CLOG5(level, format, this, p1, p2, p3, p4, p5)
#define CLOG5(level, format, p0, p1, p2, p3, p4, p5)\
		IFLOG(level) AMT::flogf((FILE*)AMT::log_file,  #level":%s:%s:" format, AMT::getTimeOfDay(p0).c_str(), AMT::programName, p1, p2, p3, p4, p5)

#define LOG10(level, format, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10) CLOG10(level, format, this, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10)
#define CLOG10(level, format, p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10)\
		IFLOG(level) AMT::flogf((FILE*)AMT::log_file,  #level":%s:%s:" format, AMT::getTimeOfDay(p0).c_str(), AMT::programName, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10)


//#define LOGTIME(level) LOG1(level, "T%s\n", AMT::LogTime(NULL))

#define LOGTIME(level)\
    IFLOG(level) {AMT::flogf(AMT::log_file, "T%s\n", LogTime(NULL)); fflush(AMT::log_file); }

//
namespace AMT{

	extern int log_level;
	extern FILE* log_file;
	extern const char* programName;
	extern void init_debug(int _log_level, const char* _programName, const char* version
	    , const char* _logFile);

	extern int flogf (FILE *__stream, const char *__format, ...);
char* LogTime(char* _str);

int getMS();
std::string getTimeOfDay(const void* ptr);

////////////////////////////////////////////////////////////////////////////////
//

class CException_my//: public std::exception
{
  char msg[0x100];
  public:
    virtual const char* what() const throw(){
      return msg;
    };
  public:
    CException_my(const CException_my& _e)
    { memcpy(msg, _e.msg, 0x100); }

    CException_my(int _code, int _line, const char* _file, const char* _msg)
    { sprintf(msg, "%s:%d\n%d:%s", _file, _line, _code, _msg); }

    virtual ~CException_my(){ }
};
#define P_EXC AMT::CException_my&

#define E(_code, _msg) AMT::CException_my(_code, __LINE__, __FILE__, _msg)

#define E_CATCH(exc) LOG1(ALRM, "%s\n", exc.what()); //delete e
#define E_CATCH2(exc) CLOG1(ALRM, "%s\n", NULL, exc.what()); //delete e

}
#endif
