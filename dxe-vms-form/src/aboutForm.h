/*
 * aboutForm.h
 *
 *  Created on: 23.06.2011
 *      Author: tsvet
 */

#ifndef ABOUTFORM_H_
#define ABOUTFORM_H_

#include <QtGui/QDialog>
#include <QtGui/QLabel>

class CAboutForm: public QDialog{
  Q_OBJECT

public:
  CAboutForm(QWidget*_parent, const QRect &_pos);
  ~CAboutForm(){};

};


#endif /* ABOUTFORM_H_ */
