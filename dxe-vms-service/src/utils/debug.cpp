/*
 * debug.cpp
 *
 *  Created on: 19.04.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */

#include <stdio.h>
#include <time.h>
#include "../debug.h"

#include <stdarg.h>
#define BOOST_THREAD_USE_LIB
#include <boost/date_time.hpp>
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>
//#include <boost/chrono/system_clocks.hpp>

namespace AMT{
int log_level = INFO;
FILE* log_file = stdout;
const char* programName = "";
void backup_log(const char* fname);

void init_debug(int _log_level, const char* _programName, const char* version
    , const char* _logFile){
  log_level = _log_level;

//time_t log_time = time(NULL);
  char deflogname[100];
  if(NULL == _programName){
    programName = "";
  }else
    programName = _programName;

  if(NULL == _logFile){
    log_file = stdout;
  }else{
    sprintf(deflogname, "%s.%s.log", _logFile, programName);
    _logFile = deflogname;
    backup_log(_logFile);
    log_file = fopen(_logFile, "a+");
  };

  time_t rawtime;
  time ( &rawtime );
  fprintf(log_file, "\n\n\n\n\n%s %s: started at %s\n", programName, version, ctime (&rawtime) );
//_IOFBF
  setvbuf(   log_file, NULL, _IONBF , 0);
  setvbuf(   stdout, NULL, _IOLBF , 0);
  setvbuf(   stderr, NULL, _IOLBF , 0);
  setvbuf(   stdin, NULL, _IOLBF , 0);
}

int flogf (FILE *__stream, const char *__format, ...)
{
  static boost::mutex lock;
  boost::mutex::scoped_lock locker(lock);
  register int ret;
  va_list __local_argv;
  va_start( __local_argv, __format );
  ret = __mingw_vfprintf( __stream, __format, __local_argv );
  va_end( __local_argv );
  return ret;
}

int getMS(){
  return boost::posix_time::microsec_clock::local_time().time_of_day().total_milliseconds();
}

std::string getTimeOfDay(const void* ptr){
  std::stringstream ss; ss.width(2);
  auto timeOfDay = boost::posix_time::microsec_clock::local_time().time_of_day();
  ss  << timeOfDay.hours()    <<":"
      << timeOfDay.minutes()  <<":"
      << timeOfDay.seconds()  <<"."
      << timeOfDay.fractional_seconds()
      << " :" << boost::this_thread::get_id()
      << " :" << ptr;
  return ss.str();
}

char log_time_string[50];
char* LogTime(char* _str){

//  std::stringstream s;
//  s << boost::posix_time::microsec_clock::local_time().time_of_day();
//  const char* cs = s.rdbuf()->gptr();

  time_t rawtime;
  time ( &rawtime );
  tm* local_time = localtime(&rawtime);

  if(NULL == _str) _str = log_time_string;
  sprintf(_str, "%d/%d %d:%d:%d:%d\n"
      , local_time->tm_mday, local_time->tm_mon+1
      , local_time->tm_hour, local_time->tm_min, local_time->tm_sec, 0);
//  sprintf(_str, "%d/%d %s\n", local_time->tm_mday, local_time->tm_mon+1, cs);
  return _str;
}

void backup_log(const char* fname)
{using namespace boost::filesystem;//copy log file to logs dir
try{
  path p (fname);
  if(file_size(p)> (5*1024*1024)){
    rename(p, unique_path( path("logs/dxe.vms.%%%%-%%%%.log")));
    remove(p);
  }
  }catch(...){

  }
}

}
