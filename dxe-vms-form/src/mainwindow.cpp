#include <QtCore/QDir>
#include <QtGUI/QMessageBox>
#include <QtGUI/QKeyEvent>

#include "mainwindow.h"

#include <windows.h>

#include "ui_mainwindow.h"
#include "pagecontrolprop.h"
#include "aboutForm.h"
#include "utils/CXMLReader.h"
#include "utils/utils.h"

static QString prevDescr = "";
static int prevRow = -1;
static QString prevNumber = "";
bool isChangeProcessActivated = false;

void MainWindow::showHide(QSystemTrayIcon::ActivationReason _r) {
  if (_r == QSystemTrayIcon::DoubleClick) { //если двойной клик
    if (!this->isVisible()){ //если окно было не видимо - отображаем его
      this->show();
      QTimer::singleShot(200, this, SLOT(showNormal()));
      //this->showNormal();
    }else
      this->hide(); //иначе скрываем
  };
}

void MainWindow::CreateSysTray() {
  //sys tray
  restoreAction = new QAction(QString::fromUtf8("&Показать"), this);
  connect(restoreAction, SIGNAL(triggered()), this, SLOT(showNormal()));
  exitAct = new QAction(QString::fromUtf8("&Завершить"), this);
  connect(exitAct, SIGNAL(activated()), this, SLOT(close()));
  startAct = new QAction(QString::fromUtf8("&Старт"), this);
  startAct->setStatusTip(QString::fromUtf8("Старт сервера"));
  connect(startAct, SIGNAL(activated()), this, SLOT(StartCore()));

  stopAct = new QAction(QString::fromUtf8("&Стоп"), this);
  stopAct->setStatusTip(QString::fromUtf8("Стоп сервера"));
  connect(stopAct, SIGNAL(activated()), this, SLOT(StopCore()));

  propAct = new QAction(QString::fromUtf8("&Настройка"), this);
  propAct->setStatusTip(QString::fromUtf8("Открыть настройки"));
  connect(propAct, SIGNAL(activated()), this, SLOT(OpenProperties()));

  trayIconMenu = new QMenu(this);
  trayIconMenu->addAction(startAct);
  trayIconMenu->addAction(stopAct);
  trayIconMenu->addSeparator();
  trayIconMenu->addAction(propAct);
  trayIconMenu->addSeparator();
  trayIconMenu->addAction(restoreAction);
  trayIconMenu->addAction(exitAct);

  trayIcon = new QSystemTrayIcon(this);
  trayIcon->setContextMenu(trayIconMenu);
  trayIcon->setIcon(QIcon(":/new/prefix1/images/main.png"));

  startAct->setEnabled(true);
  stopAct->setEnabled(false);

  connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this,
      SLOT(showHide(QSystemTrayIcon::ActivationReason)));
}


void MainWindow::CreateTableInfo() {
  QDir dir("./abonents");
  QStringList lstDirs = dir.entryList(
      QDir::Dirs | QDir::AllDirs | QDir::NoDotAndDotDot);

  //Для папок делаем рекурсивный вызов
  foreach (QString dirName, lstDirs)
  {
    if (dirName != "libs") {
      //если не директория библиотек
      //имя название папки
      QString name = dirName;
      QString entryAbsPath = dir.absolutePath() + "/" + dirName + "/"
      + "abonent.xml";
      CXMLReader reader(entryAbsPath);
      //описание берем из abonent.xml
      QString descr = reader.GetAttribute("Base/Abonent", "name", "-1");
      SetNewNumber(name, descr);
    };
  }
  countAbonents = ui->tableWidget->rowCount();
}

void MainWindow::SetNewNumber(QString _number, QString _name) {
  ui->tableWidget->setSortingEnabled(false);
  int abs_num = ui->tableWidget->rowCount();
  if (ui->tableWidget->rowCount() < ++abs_num)
    ui->tableWidget->setRowCount(abs_num);

  //создаем строку
  QTableWidgetItem *new_row = new QTableWidgetItem();
  ui->tableWidget->setVerticalHeaderItem(abs_num - 1, new_row);
  new_row->setText(QString::number(abs_num));

  //check
  QTableWidgetItem *new_check = new QTableWidgetItem();
  new_check->setCheckState(Qt::Unchecked);
  ui->tableWidget->setItem(abs_num - 1, 0, new_check);

  //номер
  QTableWidgetItem *new_number = new QTableWidgetItem();
  new_number->setText(_number);
  ui->tableWidget->setItem(abs_num - 1, 1, new_number);

  //информация
  QTableWidgetItem *new_name = new QTableWidgetItem();
  new_name->setText(_name);
  ui->tableWidget->setItem(abs_num - 1, 2, new_name);

  //вниз скроллим
  ui->tableWidget->scrollToBottom();
}

void MainWindow::showNormal(){
  LOG(QString::fromUtf8("showing normal\n"));

  QMainWindow::showNormal();
}

bool MainWindow::close(){
  LOG( QString::fromUtf8("closing\n"));

  return QMainWindow::close();
}

void MainWindow::StartCore() {
	if(isDemo) return;
  form_is_closing = false;
  QString program = "dxe-vms-service.exe";
  serverVMS.start(program);

  SetPriorityClass(serverVMS.pid()->hProcess, REALTIME_PRIORITY_CLASS);
  //#define REALTIME_PRIORITY_CLASS 0x100); winbase.h

  //блокируем удалить абонента
  ui->deleteAllButton->setEnabled(false);
  ui->deleteCheckedButton->setEnabled(false);
  //ui->->setEnabled(false);
  //ui->tableWidget->setEnabled(false);
  startAct->setEnabled(false);
  stopAct->setEnabled(true);
  ui->serverButton->setText(QString::fromUtf8("Выключить сервис"));
  ui->serverInfo->setText(
      QString::fromUtf8("Сервис \"Голосовая почта\" включен"));
  ui->serverButton->setIcon(QIcon(":/new/prefix1/images/redStone.png"));

  LOG(QString::fromUtf8("start vms\n"));
}

#include <windows.h>

void MainWindow::StopCore() {
  form_is_closing = true;
  serverVMS.close();
  serverVMS.waitForFinished(1000);
  //разблокируем удалить абонента
  ui->deleteAllButton->setEnabled(true);
  ui->deleteCheckedButton->setEnabled(true);
  ui->tableWidget->setEnabled(true);
  startAct->setEnabled(true);
  stopAct->setEnabled(false);
  ui->serverInfo->setText(
      QString::fromUtf8("Сервис \"Голосовая почта\" выключен"));
  ui->serverButton->setText(QString::fromUtf8("Включить сервис"));
  ui->serverButton->setIcon(QIcon(":/new/prefix1/images/greenStone.png"));

  LOG(QString::fromUtf8("stop vms\n"));

}


void MainWindow::DeleteRow(const QString& _number) {
  for (int i = 0; i < ui->tableWidget->rowCount(); i++) {
    if (_number == ui->tableWidget->item(i, 1)->text()) {
      ui->tableWidget->removeRow(i);
      prevRow--;
      return;
    };
  };
}
static bool must_close = false;
void killVMS() {
  QProcess kill;
  must_close = true;
  kill.start("./deleteVMS.bat");
  kill.waitForFinished(100);
  must_close = false;
}

QString MainWindow::GetAbonentNumber(int _row) {
  QTableWidgetItem* item = ui->tableWidget->item(_row, 1);
  if (item == NULL)
    return "no_item";
  return item->text();
}

void MainWindow::NewAbonent() {
  SetNewNumber(QString::fromUtf8("Default"), QString::fromUtf8("Default_name"));
  ui->tableWidget->setCurrentCell(ui->tableWidget->rowCount() - 1, 0);
}

void MainWindow::DeleteAbonent() {
  //выбираем текущего из таблицы и вычищаем
  prevRow = ui->tableWidget->currentRow();
  QString number = GetAbonentNumber(prevRow);

  DeleteAbonent(number);
  prevRow = -1;
  RefreshAbonents();

}

int MainWindow::DeleteAbonent(const QString &_number) {
  if ((_number == "") || (prevRow == -1)) {
    //пытаемся удалить но это на создании нового
    return 0;
  };

  LOG(QString::fromUtf8("DeleteAbonent: ") + _number + "\n");

  //удаляем папку и из xml
  QString path = QString::fromUtf8("./abonents/") + _number;
  QDir abonentDir(path);

  int res = removeFolder(abonentDir);

  QString resStr = "DeleteAbonent end with result = " + QString("%1").arg(res)
      + "\n";
  //QIODevice::errorString() + "\n"; //QString::fromUtf8(strerror(errno)) + "\n";
  LOG(resStr);
  return res;
}

void MainWindow::ReplaceAbonent(const QString &_number) {
  QDir dir;
  LOG(
      QString::fromUtf8("Replace abonent: ") + _number + " to abonent: "
          + GetAbonentNumber(prevRow) + "\n");

  if (prevRow == -1)
    return;

  if (!dir.exists(
      QString::fromUtf8("./abonents/")
          + ui->tableWidget->item(prevRow, 1)->text())) {
    //будет замена существующего каталога удалится в save
    DeleteAbonent(_number);
  };
  SaveAbonent();
  LOG(QString::fromUtf8("Replace abonent end\n"));
}

void MainWindow::RefreshAbonents() {
  ui->tableWidget->setRowCount(0);
  //Заполнение из инифайла таблицы
  LOG( QString::fromUtf8("refresh abons\n"));

  CreateTableInfo();
  prevRow = -1;
  prevNumber = -1;
}

void MainWindow::UndoLastChanges() {
  LOG("UndoLastChanges\n");
  if (prevRow == -1)
    return;
  //восстанавливаем предыдущие значения
  ui->tableWidget->item(prevRow, 1)->setText(prevNumber);
  ui->tableWidget->item(prevRow, 2)->setText(prevDescr);
}

void MainWindow::ReNumColumn() {
  for (int i = 0; i < ui->tableWidget->rowCount(); i++) {
    ui->tableWidget->verticalHeaderItem(i)->setText(QString::number(i + 1));
  };
}

void MainWindow::SaveAbonent() {
  //берем текущую строку
  if (prevRow < 0)
    prevRow = ui->tableWidget->currentRow();
  int ret = 0;
  //обновить состояние таблицы
  QString number = GetAbonentNumber(prevRow);
  LOG("SaveAbonent: " + number + "\n");
  if ((number == prevNumber) && (number != "Default")) {
    //номер не поменялся, обновляем только описание
    QString path = "./abonents/" + prevNumber + "/abonent.xml";
    string _path = path.toStdString();
    CXMLReader xmlReader(path);
    xmlReader.SetAttribute("Base/Abonent", "name",
        ui->tableWidget->item(prevRow, 2)->text());
    xmlReader.WriteFile();
    prevRow = -1;
    return;
  };
  //проверяем новый или нет
  QDir abonentDir;
  QString path = "./abonents/" + number;
  if (!abonentDir.exists(path)) {
    //создаем
    QString resStr = "CreateAbonent: " + number + " is begining\n";
    LOG(resStr);
    int res = abonentDir.mkdir(path);
    resStr = "CreateAbonent end with result = " + QString("%1").arg(res) + "\n";
    LOG(resStr);
  } else {
    ret = CreateMessageBox(QString::fromUtf8("Такой абонент уже существует."),
        QString::fromUtf8("Переписать данные?"));
    switch (ret) {
    case QMessageBox::Ok: {
      //удалить предыдущую запись
      DeleteAbonent(prevNumber);
      //удалить запись абонента
      DeleteAbonent(number);
      DeleteRow(number);
      //создаем
      abonentDir.mkdir(path);
    }
      ;
      break;
    case QMessageBox::Cancel:

      return UndoLastChanges();
    };
  };
  if (ret == QMessageBox::Cancel)
    return;
  if (prevRow < 0)
    return;
  //сохраняем изменения в xml
  path += "/abonent.xml";
  CXMLReader xmlReader(path);
  ReNumColumn();
  prevNumber = number;
  prevDescr = ui->tableWidget->item(prevRow, 2)->text();
  xmlReader.CreateNewAbonent(path, prevNumber, prevDescr);
}

void MainWindow::CreateCallbacks() {
  QObject::connect(ui->tableWidget, SIGNAL(currentCellChanged(int,int,int,int)),
      this, SLOT(CurrentCellChanged(int, int, int, int)));
  connect(&serverVMS, SIGNAL(finished(int, QProcess::ExitStatus)), this,
      SLOT(ProcessFinished(int, QProcess::ExitStatus)));
  connect(&serverVMS, SIGNAL(error(QProcess::ProcessError)), this,
      SLOT(ProcessError(QProcess::ProcessError)));
  //    QObject::connect(ui->serverButton, SIGNAL(Clicked()),
  //        this, SLOT(on_ServerButtonClicked()));
  //QMetaObject::connectSlotsByName(this);
}

static QFile tmp_file("./tmp/form.tmp");
void CreateTmpFile() {
  if (!tmp_file.open(QIODevice::WriteOnly)) {
    return;
  };
}
;

bool CheckTmpFile() {
  if (!tmp_file.open(QIODevice::ReadOnly))
    return false; //файлика нет
  tmp_file.close();
  QFile::remove("./tmp/form.tmp");
  if (!tmp_file.open(QIODevice::ReadOnly))
    return false; //удалили
  tmp_file.close();
  //не дала удалить предыдущая форма
  return true;
}

extern bool autostart;

MainWindow::MainWindow(QWidget *parent) :
    base(parent), form_is_closing(false), ui(new Ui::MainWindow) {

  if (CheckTmpFile())
    exit(0);
  CreateTmpFile();
  isDemo = false;
  killVMS();
  ui->setupUi(this);
  setWindowTitle(QString::fromUtf8("DXE-VMS: Сервис голосовой почты"));
  {
  	CXMLReader keyFile("./license.key");
  	if(keyFile.GetAttribute("root/General/licenseString", "value", "nostring") == "nostring"){
  		setWindowTitle(QString::fromUtf8("DXE-VMS: Сервис голосовой почты, демо режим. Необходимо получить файл ключа."));
			isDemo = true;
  	}
  }
  if(!autostart){
    CXMLReader settings("./settings.xml");
    auto svalue = settings.GetAttribute("Params/Options/autoStart", "value", "1");
    autostart = svalue != "0";
  }
  setFixedSize(800, 600);

  //создание systray
  CreateSysTray();
  //Заполнение из инифайла таблицы
  CreateTableInfo();
  //Создаем обработчик событий
  CreateCallbacks();

  CreateFonts();

  CreateLogDir();

  ui->tableWidget->installEventFilter(this);

  //запускаем таймер
  refreshTimer = new QTimer(this);
  connect(refreshTimer, SIGNAL(timeout()), this, SLOT(checkChangesAbonents()));
  //раз в минуту
  refreshTimer->start(30000);
  isChangeProcessActivated = false;

  //qDebug()<<"autostart " << autostart;
  if(autostart){
    //perform autostart action
    QTimer::singleShot(0,this,SLOT(StartCore()));
    QTimer::singleShot(1000,this,SLOT(hide()));
  }
}

MainWindow::~MainWindow() {
  if (serverVMS.state() != QProcess::NotRunning)
    StopCore();
  LOG("close. MainWindow destructor\n");
  delete ui;
  delete refreshTimer;
  tmp_file.close();
}

void MainWindow::CurrentCellChanged(int _row, int _column, int _prev_row,
    int _prev_column) {
  isChangeProcessActivated = false;
  if (_prev_row == 0)
    return;
  if (_row == _prev_row) {
    //ходим по строке
    string str = prevNumber.toStdString();
    if (prevNumber != ui->tableWidget->item(_row, 1)->text()) {
      ReplaceAbonent(prevNumber);
    } else if (prevDescr != ui->tableWidget->item(_row, 2)->text()) {
      //были изменения - нада сохранить
      SaveAbonent();
    };

  } else {
    if (prevRow >= 0)
      if ((prevNumber != ui->tableWidget->item(prevRow, 1)->text())
          || (prevDescr != ui->tableWidget->item(prevRow, 2)->text()))
        SaveAbonent();
    if (_row != -1) {
      //записываем новые значения
      prevRow = _row;
      //сохраняем предыдущие значения
      prevNumber = ui->tableWidget->item(prevRow, 1)->text();
      prevDescr = ui->tableWidget->item(prevRow, 2)->text();
    };
  };
}

bool MainWindow::eventFilter(QObject *obj, QEvent *evt) {
  if (evt->type() == QEvent::FocusOut) {
    //    if(obj == mainTable){
    //      int row = mainTable->currentRow();
    //      row++;
    //    }
  } else if (evt->type() == QEvent::KeyRelease) {
    if (ui->tableWidget->hasFocus()) {
      //вот тут и ловится нажатие клавиши "Enter" на QTableWidget'e
      QKeyEvent *key_event = (QKeyEvent*) static_cast<QEvent *>(evt);
      if (key_event->key() == Qt::Key_Escape) {
        //нажатие Esc в ячейке
      } else if (key_event->key() == Qt::Key_Return) {
        isChangeProcessActivated = false;
        //заменяем
        if (prevRow >= 0) {
          if (prevNumber != ui->tableWidget->item(prevRow, 1)->text()) {
            ReplaceAbonent(prevNumber);
          } else
            SaveAbonent();
        };
      } else if (key_event->key() == Qt::Key_Space) {
        if (ui->tableWidget->currentItem()->column() == 2) {
          //          SetCheckState(mainTable->currentItem());
        };
      };
    };
  };
  return base::eventFilter(obj, evt);
}

void MainWindow::closeEvent(QCloseEvent *event) {
  //  if (trayIcon->isVisible()) {
  //    //trayIcon->hide();
  //    //hide();
  //    //event->ignore();
  //  }
  //quit();
  must_close = true;
  base::closeEvent(event);
  QApplication::exit();
}

void MainWindow::hideEvent(QHideEvent *event) {
  if (must_close)
    return;

  if (!trayIcon->isVisible()) {
    //hide();
    trayIcon->show();
  }

  if (isMinimized()) {
    QTimer::singleShot(0, this, SLOT(hide()));
  }
  base::hideEvent(event);
}

void MainWindow::showEvent(QShowEvent* event) {
  base::showEvent(event);
  trayIcon->hide();
}

void MainWindow::ProcessFinished(int _exitCode,
    QProcess::ExitStatus _exitStatus) {
  if (form_is_closing)
    return;
  if (must_close) {
    return;
  }
//  StartCore();
  StopCore();
}

void MainWindow::ProcessError(QProcess::ProcessError _error) {
  if (form_is_closing)
    return;
  if (must_close) {
    return;
  }
//  StartCore();
  LOG(QString("ProcessError=%1\n").arg( (int)_error));
  StopCore();
}

void MainWindow::OpenProperties() {
  LOG("show properties\n");
  pageControlProp* properties = new pageControlProp(this);
  properties->exec();
}

void MainWindow::on_openPropButton_released() {
  OpenProperties();
}

void MainWindow::on_aboutFormButton_released() {
  CAboutForm* aboutForm = new CAboutForm(this, QRect(200, 200, 280, 170));
  aboutForm->exec();
}
void MainWindow::checkChangesAbonents() {
  if (isChangeProcessActivated)
    return;
  if (CheckAbonents())
    RefreshAbonents();
}
;

void MainWindow::on_tableWidget_cellDoubleClicked(int row, int column) {
  isChangeProcessActivated = true;
}

void MainWindow::on_serverButton_released() {
  //по нажатию на кнопку переключаем состояния
  if (serverVMS.state() != QProcess::NotRunning) {
    StopCore();
  } else {
    StartCore();
  }
}

void MainWindow::on_deleteCheckedButton_released() {
  //идем и удаляем вес выделенные
  for (int i = 0; i < ui->tableWidget->rowCount(); i++) {
    if (ui->tableWidget->item(i, 0)->checkState() == Qt::Checked) {
      //удаляем
      //удаляем папку и из xml
      QString path = QString::fromUtf8("./abonents/")
          + ui->tableWidget->item(i, 1)->text();
      QDir abonentDir(path);
      //удаляем
      removeFolder(abonentDir);
    };
  };
  RefreshAbonents();
  prevRow = -1;
  prevDescr = "";
  prevNumber = "";
}

void MainWindow::on_deleteAllButton_released() {
  for (int i = 0; i < ui->tableWidget->rowCount(); i++) {
    //удаляем папку и из xml
    QString path = QString::fromUtf8("./abonents/")
        + ui->tableWidget->item(i, 1)->text();
    QDir abonentDir(path);
    //удаляем
    removeFolder(abonentDir);
  };
  RefreshAbonents();
  prevRow = -1;
  prevDescr = "";
  prevNumber = "";
}
