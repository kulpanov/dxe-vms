/*
03.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <stdio.h>
#include <winsock2.h>
#include <string.h>

#include "../define.h"
#include "../CVoiceMailService.h"
//
//#include "mysdlSignals.h"

#include "../../etc/ex2.c"
#include "../../etc/ex3.c"
#include "../../etc/ex4.cpp"
#include "../utils/netutils.h"

using namespace std;
using AMT::tVoiceMsg;
using AMT::CPacket_RTP;



void diep(const char* _msg){
  LOG1(7, "VMS:cmd: %s\n", _msg);
  exit(1);
}

#define BUFLEN 2048
struct sockaddr_in si_other, si_me;
int s_out, s_in, slen = sizeof(si_other);

void Send_INVITE(char id, int number, int scenario
    , const char* fromIP, const char* toIP
    , const char* fromIP2, const char* toIP2
    , int rtp_port) {

  uint8_t buf[BUFLEN];
  int len;

  char* invite = new char[0x300];
  sprintf(invite, EX4::pkt_INVITE0_template, scenario, toIP //invite
      , fromIP, id, toIP2, fromIP //via
      , scenario, toIP //to
      , number, fromIP, fromIP2 //from
      , id, toIP2, fromIP //callid
      , number, fromIP //Contact
      , fromIP //o=
      , fromIP //c=
      , rtp_port //RTP/AVP
  );

  //1:INVITE
  memset(buf, '\0', BUFLEN);

  len = strlen(invite);
  memcpy(buf, invite, len);//ex1.pkt4

  if (-1 == (sendto(s_out, (const char*) buf, len, 0, (sockaddr*) &(si_other),
      sizeof(sockaddr_in))))
    diep("VMS:UDPDR_long:sendto");

  delete invite;
}

void Send_ACK(char id, int number, int scenario
    , const char* fromIP, const char* toIP
    , const char* fromIP2, const char* toIP2
  ) {
  uint8_t buf[BUFLEN];
  int len;
  char* ack = new char[0x300];
  sprintf(ack, EX4::pkt_ACK0_template, scenario, toIP //invite
      , fromIP, id, toIP2, fromIP //via
      , scenario, toIP //to
      , number, fromIP, fromIP2 //from
      , id, toIP2, fromIP //callid
  );
  //strcpy(req, EX4::pkt_INVITE0);
  {
    LOG(6, "VMS:cmd: ACK pkt\n");
    memset(buf, '\0', BUFLEN);
    len = strlen(ack);
    //memcpy(buf, EX4::pkt_ACK1, len);//ex1.pkt10
    memcpy(buf, ack, len);//ex1.pkt10

    //  len = 560 - 0x2a;
    //  memcpy(buf, &EX2::pkt7[0x2a], len);//ex1.pkt10
    if (-1 == (sendto(s_out, (const char*) buf, len, 0,
        (sockaddr*) &(si_other), sizeof(sockaddr_in))))
      diep("VMS:UDPDR_long:sendto");
  }

  delete ack;
}

void Send_BYE(char id, int number, int scenario
    , const char* fromIP, const char* toIP
    , const char* fromIP2, const char* toIP2
  ) {
  uint8_t buf[BUFLEN];
  int len;
  char* msg = new char[0x300];
  sprintf(msg, EX4::pkt_BYE0_template, scenario, toIP //invite
      , fromIP, id, toIP2, fromIP //via
      , scenario, toIP //to
      , number, fromIP, fromIP2 //from
      , id, toIP2, fromIP //callid
  );
  //strcpy(msg, EX4::pkt_BYE0);

  {
    memset(buf, '\0', BUFLEN);
    len = strlen(msg);
    memcpy(buf, msg, len);//ex1.pkt10

    if (-1 == (sendto(s_out, (const char*) buf, len, 0,
        (sockaddr*) &(si_other), sizeof(sockaddr_in))))
      diep("VMS:UDPDR_long:sendto");
  }

  delete msg;
}


#define FIRST_OUT_RTP_PORT 5064

pthread_t test_thread;
void* thread_test(void* _param);
struct TEST_PARAMETERS{
  int id;
  int number;
  int scen;
  int port;
};

//
void* thread_cmd2(void* _param) {
  string str;
  //AMT::CVoiceMailService* vms = (AMT::CVoiceMailService*)_param;

  {
    if ((s_out = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
      diep("socket");

    memset((char *) &si_other, 0, sizeof(si_other));
    si_other.sin_family = AF_INET;
    si_other.sin_port = htons(5060);
    si_other.sin_addr.S_un.S_addr = htonl(INADDR_LOOPBACK);
    //si_other.sin_addr.S_un.S_addr = htonl(0xc0a8e90b);
  }

  {
    if ((s_in = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
      diep("socket");

    memset((char *) &si_me, 0, sizeof(si_me));
    si_me.sin_family = AF_INET;
    si_me.sin_port = htons(5061);
    si_me.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(s_in, (sockaddr *) &si_me, sizeof(si_me)) == -1)
      diep("bind");
  };

  int id = 15;
  for(id=0; id<19; id++)
  {
    TEST_PARAMETERS testParam;
    testParam.id = id;testParam.number = 300+id;testParam.scen = 1;testParam.port = 5063+(id*2);
    pthread_create(&test_thread, NULL, thread_test, &testParam);
    Sleep(100);
  }

  Sleep(30000);

  closesocket(s_out);
  closesocket(s_in);
  return NULL;
};

void bufcpy(uint8_t* buf1, uint8_t* buf) {
  int count = 0;
  while ('\n' != (buf1[count] = buf[count]))
    count++;

  buf1[count] = '\0';
}


void* thread_test(void* _param) {
  uint8_t buf[BUFLEN]; int len;
  TEST_PARAMETERS testParam;
  memcpy(&testParam, _param, sizeof(TEST_PARAMETERS));

  //1:INVITE
  LOG1(6, "VMS:cmd(%d): INVITE\n", testParam.id);
  Send_INVITE(testParam.id, testParam.number, testParam.scen
      , "192.168.233.2", "192.168.233.1"
      , "1921682332", "1921682331"
      , testParam.port);

  //2:TRYING
  memset(buf, '\0', BUFLEN);
  if (-1
      == (len = recvfrom(s_in, (char*) buf, BUFLEN, 0, (sockaddr *) &si_other, &slen)))
    diep("recvfrom()");

//  LOG1(6, "VMS:recv(%d):\n", testParam.id);
//  bufcpy(buf1, buf);
//  LOG1(6, "\t%s\n", buf1);

  //3:OK
  memset(buf, '\0', BUFLEN);
  if (-1
      == (len = recvfrom(s_in, (char*) buf, BUFLEN, 0, (sockaddr *) &si_other, &slen)))
    diep("recvfrom()");

//  LOG1(6, "VMS:recv(%d):\n", testParam.id);
//  bufcpy(buf1, buf);
//  LOG1(6, "\t%s\n", buf1);
  //4:ACK
  LOG1(6, "VMS:cmd(%d): ACK pkt\n", testParam.id);
  Send_ACK(testParam.id, testParam.number, testParam.scen, "192.168.233.2", "192.168.233.1", "1921682332", "1921682331");
  {
      Sleep(7000);
      struct sockaddr_in si_other_rtp;
      int s_out_rtp;
      if ((s_out_rtp = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    diep("socket");
      memset((char*)(&si_other_rtp), 0, sizeof (si_other_rtp));
      si_other_rtp.sin_family = AF_INET;
      si_other_rtp.sin_port = htons(FIRST_OUT_RTP_PORT + (testParam.id*2));
      si_other_rtp.sin_addr.S_un.S_addr = htonl(INADDR_LOOPBACK);
      LOG1(6, "VMS:cmd(%d): send RTP\n", testParam.id);
      tVoiceMsg q_rtp_out;
      tVoiceMsg::Load_qmsg(q_rtp_out, "wavs/test.wav");
      tVoiceMsg::iterator it = q_rtp_out.begin();
      do{
          //for(int rtp_count=0; rtp_count<100; rtp_count++){
          CPacket_RTP *rtp_packet = *it;
          //      memset(buf, '\0', BUFLEN);
          //      len = 536 - 0x2a;
          //      memcpy(buf, &EX4::pkt_RTP1[0], len);//ex1.pkt4
        if(-1 == (sendto(s_out_rtp, (const char*)(rtp_packet->GetData()), rtp_packet->GetLength(), 0, (sockaddr*)(&(si_other_rtp)), sizeof(sockaddr_in))))
          diep("VMS:UDPDR_long:sendto");

      Sleep(60);

    }while(++it != q_rtp_out.end());
  }
  //1:BYE
  LOG1(6, "VMS:cmd(%d): BYE pkt\n", testParam.id);
  Send_BYE(testParam.id, testParam.number, testParam.scen
      , "192.168.233.2", "192.168.233.1"
      , "1921682332", "1921682331");

  //2:OK
  memset(buf, '\0', BUFLEN);
  if (-1
      == (len = recvfrom(s_in, (char*) buf, BUFLEN, 0, (sockaddr *) &si_other, &slen)))
    diep("recvfrom()");

//  LOG1(6, "VMS:recv(%d):\n", testParam.id);
//  bufcpy(buf1, buf);
//  LOG1(6, "\t%s\n", buf1);
  return NULL;
}
