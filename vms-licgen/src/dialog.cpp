#include "dialog.h"

#include <QtCore/QTextStream>
#include "QtGui/QFileDialog"

#include "crypter.h"
#include "CXMLReader.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    QObject::connect(ui->bGenerate, SIGNAL(released()), this, SLOT(onOk()));
    QObject::connect(ui->bGen2, SIGNAL(released()), this, SLOT(onOk2()));
}

Dialog::~Dialog()
{
    delete ui;
}

void
Dialog::onOk ()
{
	CCrypter crypt;
	std::string s = "AMTelecom DXE SIP Gateway 118" + ui->spinBox->text().toStdString();
	s = crypt.crypt(s);

  QString fileName = QFileDialog::getSaveFileName(this, QString::fromUtf8("Открыть файл лицензии"),
                                                   "",
																									 QString::fromUtf8("LicenseFiles (*.key)"));
  if(fileName.size() ==0 ) return ;

  {
		QFile file(fileName);
		if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
			return ;
		QTextStream out(&file);
		out << "<?xml version=\"1.0\"?>\n";
		out << "<root>\n<General>\n<licenseString value='nostring' />\n</General>\n</root>\n";
  }

  CXMLReader lic(fileName);
  lic.SetAttribute("root/General/licenseString", "value", QString().fromStdString(s));
  lic.WriteFile();
  close();
}

void
Dialog::onOk2 ()
{
	CCrypter crypt;
	std::string s = "AMTelecom DXE SIP Gateway 118" + ui->spinBox->text().toStdString();
	s = crypt.crypt(s);

	QString fileName = "./license.key";
  {
		QFile file(fileName);
		if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
			return ;
		QTextStream out(&file);
		out << "<?xml version=\"1.0\"?>\n";
		out << "<root>\n<General>\n<licenseString value='nostring' />\n</General>\n</root>\n";
  }

  CXMLReader lic(fileName);
  lic.SetAttribute("root/General/licenseString", "value", QString().fromStdString(s));
  lic.WriteFile();
  close();
}
