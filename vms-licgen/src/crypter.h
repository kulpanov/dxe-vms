//============================================================================
// Name        : crypter.h
// Author      : Kulpanov
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#pragma once
#include <string>
#include <iostream>
using namespace std;

class CCrypter {
public:
	bool match(const string& _inputstr){
		string crypt_str = crypt(_inputstr);
		return (crypt_str == factoryNumberCrypted);
	}

	string crypt(const string& _s){
		string crypt_str;
		unsigned int pos = 0;
		auto first = _s.begin();
		while (first!=_s.end()) {
			if(pos<key.length())crypt_str.push_back(
					(*first^key[pos++])%('~' - '*') + '*'
			);
	    ++first;
		  }
		return crypt_str;
	}
	void setFactoryNumber(string _fnumber_crypted){
		factoryNumberCrypted = _fnumber_crypted;
	}
private:
	const string key = "qwertyuiop[]asdfghjkl;'zxcvbnm,./";
	string factoryNumberCrypted = " ";
};
