#include <QtGui/QApplication>
#include "mainwindow.h"

bool autostart = false;
int main(int argc, char *argv[])
{
    Q_INIT_RESOURCE(resource);
    QApplication a(argc, argv);
    QApplication::setQuitOnLastWindowClosed(false);

//    a.addLibraryPath("./libs");
    if(argc > 1)
      if(strcmp(argv[1], "--autostart") == 0)
        autostart = true;
    MainWindow w;
    w.show();

    return a.exec();
}
