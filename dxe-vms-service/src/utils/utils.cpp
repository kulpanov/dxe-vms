/** \brief Различные утилиты, реализация
 * utils.cpp
 *
 *  Created on: 05.09.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */

#include <windows.h>
#include "../define.h"

#include "utils.h"
const CString nullStr = "";

//Подсчитать кол-во файлов в каталоге
int getCountOfFilesInDir(const char* _dirname){
  int countOfFiles = 0;
  WIN32_FIND_DATA FindFileData;
  HANDLE hFind;

  hFind = FindFirstFile(_dirname /*"abonents\\*"*/, &FindFileData);
  if (hFind == INVALID_HANDLE_VALUE){
    throw E(GetLastError(), "FindFirstFile failed");
  }
  do{
    if(FindFileData.cFileName[0] != '.')
      countOfFiles ++; //кроме скрытых

  }while(FindNextFile(hFind, &FindFileData) ) ;

  FindClose(hFind);
  return countOfFiles;
}

