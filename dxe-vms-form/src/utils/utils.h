#ifndef UITLS_H
#define UITLS_H
#include <QString>
#include <QFont>

extern int countAbonents;
//14 bold
extern QFont* title_font;
//10 bold
extern QFont* header_font;

//созлание шрифтов
void CreateFonts();

//удалить директорию
int removeFolder(QDir & dir);

QString  CreateLogDir();
//логировать
void LOG(const QString & _logStr);
//создать сообщение
int CreateMessageBox(const QString &_message, const QString &_action);
//проверить папку абонентов
bool CheckAbonents();
#endif // UITLS_H
