/*
 * sdl_signals.cpp
 *
 *  Created on: 21.12.2010
 *      Author: Kulpanov
 */
#include <string>
#include <stdlib.h>
#include <algorithm>

#include "define.h"
#include "../include/sdl_sys.h"

extern "C"{
#include "sct/scttypes.h"
///Включаемы файл с описанием sdl-сигналов, используемых в системе
#include "vms_server.ifc"
//#include "VMS_SDL_system.ifc"
}

#include <winsock2.h>
//TODO: запоминание PID приёмника
namespace SDL{

///Задать Pid
void CSDL_Signal::SetReceiver(void* _pid){
//  if(NULL == sdl_sig) return;
//  if(NULL == _pid) {
    xSignalNode(sdl_sig)->Receiver = xNotDefPId;
//    return;
//  }
//  xSignalNode(sdl_sig)->Receiver = *((SDL_PId*)_pid);
}

///Получить pid
void* CSDL_Signal::GetSender(){
  if(NULL == sdl_sig) return NULL;
  return (void*) &(xSignalNode(sdl_sig)->Sender);
}

////////////////////////////////////////////////////////////////////////////////

//CSDLSignal_InitIP
//такая схема определения макро сделана спец. для удобства
//работы с "folding" редактора кода
#define sdl_signal ((yPDef_Route_array*)sdl_sig)
#ifdef sdl_signal
const char* CSDL_Signal_InitIP::sign() {return Route_array->Name; }

const uint32_t CSDL_Signal_InitIP::index() const{ return (uint32_t)Route_array; }

const size_t CSDL_Signal_InitIP::sizeOfData()const
{return (sizeof(yPDef_Route_array) - sizeof(xSignalStruct)); }

void* CSDL_Signal_InitIP::GetSignal(void* _notused)const{
  yPDef_Route_array* signal
      = (yPDef_Route_array*)xGetSignal(Route_array, sdl_signal->Receiver, xEnv);
  //копируем только пользовательские данные сигнала
  memcpy(& (signal->Param1),& (sdl_signal->Param1), sizeOfData());
  return signal;
}

const std::string& CSDL_Signal_InitIP::toString(std::string& str)const {
  char charbuf[256];
  uint8_t* ip1 = (uint8_t*)&(sdl_signal->Param1[0].IPDA.A[0]);
  uint8_t* net1 = (uint8_t*)&(sdl_signal->Param1[0].IPSA.A[0]);
  uint8_t* NAT1 = (uint8_t*)&(sdl_signal->Param1[0].NATVoIP.A[0]);

  str = (const char*)type();
  {
    sprintf(charbuf, ":IP=%d.%d.%d.%d:NET=%d.%d.%d.%d:VoIPNAT=%d.%d.%d.%d"
                      , ip1[0], ip1[1], ip1[2], ip1[3]
                      , net1[0], net1[1], net1[2], net1[3]
                      , NAT1[0], NAT1[1], NAT1[2], NAT1[3]);
    str += charbuf;
  }
  return str;
};

void CSDL_Signal_InitIP::AddToRoute
   (uint32_t _destIP, uint32_t _sIP, uint32_t _natVoIP){

   uint32_t dIP = htonl(_destIP);
   uint32_t sIP = htonl(_destIP);
   uint32_t natVoIP = htonl(_natVoIP);

  memcpy(& (sdl_signal->Param1[routeCount].IPDA.A[0]), &dIP, 4);
  memcpy(& (sdl_signal->Param1[routeCount].IPSA.A[0]), &sIP, 4);
  memcpy(& (sdl_signal->Param1[routeCount].NATVoIP.A[0]), &natVoIP, 4);
  routeCount++;
}

//static CSDL_Signal_InitIP* CSDL_Signal_InitIP::Create
//        (uint8_t _myIP[4], uint8_t _myNET[4], uint8_t _myVoIPNAT[4]){
//  return new CSDL_Signal_InitIP(_myIP, _myNET, _myVoIPNAT);
//}

CSDL_Signal_InitIP::CSDL_Signal_InitIP(
    uint32_t _myIP, uint32_t _myNET, uint32_t _myVoIPNAT)
:base(NULL), routeCount(1)
{
  yPDef_Route_array* signal = (yPDef_Route_array*)malloc(sizeof(yPDef_Route_array));

  uint32_t myIP = htonl(_myIP);
  uint32_t myNET = htonl(_myNET);
  uint32_t myVoIPNAT = htonl(_myVoIPNAT);
  memcpy(& (signal->Param1[0].IPDA.A[0]), &myIP, 4);
  memcpy(& (signal->Param1[0].IPSA.A[0]), &myNET, 4);
  memcpy(& (signal->Param1[0].NATVoIP.A[0]), &myVoIPNAT, 4);

  sdl_sig = signal;
  SetReceiver(NULL);
}

#undef sdl_signal
#endif

//////////////////////////////////////////////////////////////////////////////////
//CSDL_Signal_InitRTPPort
#define sdl_signal ((yPDef_VoIP_ports*)sdl_sig)
#ifdef sdl_signal
const char* CSDL_Signal_InitRTPPort::sign() {return VoIP_ports->Name; }

const uint32_t CSDL_Signal_InitRTPPort::index() const{ return (uint32_t)VoIP_ports; }

const size_t CSDL_Signal_InitRTPPort::sizeOfData()const
{return (sizeof(yPDef_VoIP_ports) - sizeof(xSignalStruct)); }

void* CSDL_Signal_InitRTPPort::GetSignal(void* _notused)const{
  yPDef_VoIP_ports* signal
      = (yPDef_VoIP_ports*)xGetSignal(VoIP_ports, sdl_signal->Receiver, xEnv);
  //копируем только пользовательские данные сигнала
  memcpy(& (signal->Param1),& (sdl_signal->Param1), sizeOfData());
  return signal;
}

const std::string& CSDL_Signal_InitRTPPort::toString(std::string& str)const {
  char charbuf[256];
  str = (const char*)type();
  {
    sprintf(charbuf, ":port %d", sdl_signal->Param1);
    str += charbuf;
  }
  return str;
};

CSDL_Signal_InitRTPPort::CSDL_Signal_InitRTPPort(
    uint32_t _initPort)
:base(NULL)
{
  yPDef_VoIP_ports* signal = (yPDef_VoIP_ports*)malloc(sizeof(yPDef_VoIP_ports));
  signal->Param1 = _initPort;
  sdl_sig = signal;
  SetReceiver(NULL);
}

#undef sdl_signal
#endif

//////////////////////////////////////////////////////////////////////////////////
//CSDL_Signal_InitIPCOM_RQ
#define sdl_signal ((yPDef_InitIPCOM_RQ*)sdl_sig)
#ifdef sdl_signal

IMPL_SDL_SIGNAL_HEAD(InitIPCOM_RQ)

//void* CSDL_Signal_InitIPCOM_RQ::GetSignal(void* _notused)const{
//
//  yPDef_InitIPCOM_RQ* signal = (yPDef_InitIPCOM_RQ*)xGetSignal(InitIPCOM_RQ, xNotDefPId, xEnv);
//
//  memcpy(& (signal->Param1),& (sdl_signal->Param1), sizeOfData());
//  return signal;
//}

void* CSDL_Signal_InitIPCOM_RQ::GetSignal(void* _sdl_sig)const{
  yPDef_InitIPCOM_RQ* signal =
      (yPDef_InitIPCOM_RQ*)malloc(sizeof(yPDef_InitIPCOM_RQ));

  yPDef_InitIPCOM_RQ* ipcom = (yPDef_InitIPCOM_RQ*)_sdl_sig;
  memcpy(& (signal->Param1)
      ,& (ipcom->Param1), sizeOfData());

  return signal;
}

const std::string& CSDL_Signal_InitIPCOM_RQ::toString(std::string& str)const {
  char charbuf[256];
  const uint16_t port = (getLowerBoundaryOfPortsOfRTP());
  const uint32_t ip = getMyIP();  uint8_t* ip1 = (uint8_t*)&ip;
  const uint32_t net = getMyNET();  uint8_t* net1 = (uint8_t*)&net;
  str = (const char*)sign();
  sprintf(charbuf, ":port=%d, IP=%d.%d.%d.%d, NET=%d.%d.%d.%d"
                    , port
                    , ip1[3], ip1[2], ip1[1], ip1[0]
                    , net1[3], net1[2], net1[1], net1[0]);
  str += charbuf;
  return str;
};

//
const uint16_t CSDL_Signal_InitIPCOM_RQ::getLowerBoundaryOfPortsOfRTP()const{
  return ntohs(*(uint16_t*)&(sdl_signal->Param1.A));
}

//получить IP
const uint32_t CSDL_Signal_InitIPCOM_RQ::getMyIP() const{
  return ntohl(*(uint32_t*)&(sdl_signal->Param2.A));
}

//
const uint32_t CSDL_Signal_InitIPCOM_RQ::getMyNET() const{
  return ntohl(*(uint32_t*)&(sdl_signal->Param3.A));
}

#undef sdl_signal
#endif

////////////////////////////////////////////////////////////////////////////////
//CSDL_Signal_Err_IPCOM
#define sdl_signal ((yPDef_Err_IPCOM*)sdl_sig)
#ifdef sdl_signal

  IMPL_SDL_SIGNAL_HEAD(Err_IPCOM);

  IMPL_SDL_SIGNALIN_HEAD(Err_IPCOM, Param1)

  const std::string& CSDL_Signal_Err_IPCOM::toString(std::string& str)const {
    str = (const char*)sign();
    return str;
  };

#undef sdl_signal
#endif

////////////////////////////////////////////////////////////////////////////////
//CSDL_Signal_InitIPCOM_I
#define sdl_signal ((yPDef_InitIPCOM_I*)sdl_sig)
#ifdef sdl_signal
#define yPDef_InitIPCOM_I xSignalStruct

IMPL_SDL_SIGNAL_HEAD(InitIPCOM_I);

CSDL_Signal_InitIPCOM_I::CSDL_Signal_InitIPCOM_I(){
  yPDef_InitIPCOM_I* signal = (yPDef_InitIPCOM_I*)malloc(sizeof(yPDef_InitIPCOM_I));
  sdl_sig = signal;
  SetReceiver(NULL);
}

void* CSDL_Signal_InitIPCOM_I::GetSignal(void* _unused)const{
  yPDef_InitIPCOM_I* signal = (yPDef_InitIPCOM_I*)xGetSignal(InitIPCOM_I, sdl_signal->Receiver, xEnv);
  return signal;
}

const std::string& CSDL_Signal_InitIPCOM_I::toString(std::string& str)const {
  str = (const char*)sign();
  return str;
};

#undef sdl_signal
#endif

////////////////////////////////////////////////////////////////////////////////
//CSDL_Signal_UDPDI_long
#define sdl_signal ((yPDef_UDPDI_long*)sdl_sig)
#ifdef sdl_signal

IMPL_SDL_SIGNAL_HEAD(UDPDI_long);

void* CSDL_Signal_UDPDI_long::GetSignal(void* _unused)const{
  yPDef_UDPDI_long* signal
      = (yPDef_UDPDI_long*)xGetSignal(UDPDI_long, sdl_signal->Receiver, xEnv);
  //копируем только пользовательские данные сигнала
  memcpy(& (signal->Param1),& (sdl_signal->Param1), sizeOfData());
  return signal;
}

static bool IsReturn (char i) { return (i == '\r'); }

const std::string& CSDL_Signal_UDPDI_long::toString(std::string& str)const {
  str = (const char*)sign();
  char charbuf[0x100];
  uint8_t* ip1 = (uint8_t*)&(sdl_signal->Param1.A[0]);
  str = (const char*)type();
  {
    sprintf(charbuf, ":sIP=%d.%d.%d.%d, sPort=%d, dPort=%d, len=%d\n"
                      , ip1[0], ip1[1], ip1[2], ip1[3]
                      , sdl_signal->Param2
                      , sdl_signal->Param3
                      , sdl_signal->Param4);
    str += charbuf;

//    char s[1372];
//    memcpy(s, sdl_signal->Param5.A, sdl_signal->Param4);
//    charbuf[sdl_signal->Param4] = '\0';

    char s[101]; memcpy(s, sdl_signal->Param5.A, 100);
    //char* s1 = strchr(s, '\r'); s1[0] = '0';

    str += s;

    std::replace_if (str.begin(), str.end(), IsReturn, ' ');
  }
  return str;
};

//получить source IP
const uint32_t CSDL_Signal_UDPDI_long::getIP() const{
 return ntohl(* ((const uint32_t*) &(sdl_signal->Param1.A[0])) );
}


CSDL_Signal_UDPDI_long::CSDL_Signal_UDPDI_long(uint32_t _sIP
                                         , uint16_t _sPort
                                         , uint16_t _dPort
                                         , uint32_t _len, uint8_t* _upd_data){
  yPDef_UDPDI_long* signal = (yPDef_UDPDI_long*)malloc(sizeof(yPDef_UDPDI_long));

  uint32_t sIP = htonl(_sIP);

  memcpy(&(signal->Param1.A[0]), &sIP, 4);

  signal->Param2 = _sPort;
  signal->Param3 = _dPort;
  if(_len>=1372)_len = 1372;//magic number взят из SDL объявления Param5
  signal->Param4 = _len;
  memcpy(& (signal->Param5.A[0]), _upd_data, _len);
  UDPDI_long_data = (SUDPDI_long_data*)&(signal->Param1);

  sdl_sig = signal;
  SetReceiver(NULL);
}

#undef sdl_signal
#endif

////////////////////////////////////////////////////////////////////////////////
//CSDL_Signal_UDPDR_long
#define sdl_signal ((yPDef_UDPDR_long*)sdl_sig)
#ifdef sdl_signal

IMPL_SDL_SIGNAL_HEAD(UDPDR_long);

void* CSDL_Signal_UDPDR_long::GetSignal(void* _sdl_sig)const{
  yPDef_UDPDR_long* signal =
      (yPDef_UDPDR_long*)malloc(sizeof(yPDef_UDPDR_long));

  memcpy(& (signal->Param1)
      ,& ( ((yPDef_UDPDR_long*)_sdl_sig)->Param1), sizeOfData());

  return signal;
}

const std::string& CSDL_Signal_UDPDR_long::toString(std::string& str)const {
  str = (const char*)sign();
  char charbuf[1756];
  str = (const char*)type();
  const uint32_t ip = getIP(); uint8_t* ip1 = (uint8_t*)&ip;
  {
    sprintf(charbuf, ":sIP=%d.%d.%d.%d, sPort=%d, dPort=%d, len=%d\n%s"
                      , ip1[0], ip1[1], ip1[2], ip1[3]
                      , getSPort()
                      , getDPort()
                      , getLengthOfData()
                      , getData());
    str += charbuf;
  }
  return str;
};

//получить source IP
const uint32_t CSDL_Signal_UDPDR_long::getIP() const{
 return ntohl(* ((const uint32_t*) &(sdl_signal->Param1.A[0])) );
}

//Получить source port
const uint16_t CSDL_Signal_UDPDR_long::getSPort() const{
  return sdl_signal->Param2;
}

//Получить dest port
const uint16_t CSDL_Signal_UDPDR_long::getDPort() const{
  return sdl_signal->Param3;
}

//Получить длину данных
const uint32_t CSDL_Signal_UDPDR_long::getLengthOfData() const{
  return sdl_signal->Param4;
}

//Получить данные
const uint8_t* CSDL_Signal_UDPDR_long::getData() const{
  return (uint8_t*) &(sdl_signal->Param5.A[0]);
}

#undef sdl_signal
#endif

////////////////////////////////////////////////////////////////////////////////
//CSDL_Signal_Err
#define sdl_signal ((yPDef_Err*)sdl_sig)
#ifdef sdl_signal

IMPL_SDL_SIGNAL_HEAD(Err);

IMPL_SDL_SIGNALOUT_HEAD(Err, Param1)

const std::string& CSDL_Signal_Err::toString(std::string& str)const {
  str = (const char*)sign();
  char charbuf[256];
  const uint8_t* err = getErr();

  str = (const char*)type();
  {
    sprintf(charbuf, ":err %d,%d,%d\n", err[0],err[1],err[2]);
    str += charbuf;
    for(int i=3;i<40;i++){
      //itoa(err[i], charbuf, 16);
      sprintf(charbuf, " %x", err[i]);
      str += charbuf;
    }
  }
  return str;
};

//получить source Err
const uint8_t*  CSDL_Signal_Err::getErr() const{
 return &(sdl_signal->Param1.A[0]);
}

//получить source Err
const uint32_t  CSDL_Signal_Err::getLenOfData() const{
 return 40;//magic number from SDL
}

#undef sdl_signal
#endif

////////////////////////////////////////////////////////////////////////////////
//CSDL_Signal_Err
#define sdl_signal ((yPDef_vms_err*)sdl_sig)
#ifdef sdl_signal

IMPL_SDL_SIGNAL_HEAD(vms_err);

IMPL_SDL_SIGNALOUT_HEAD(vms_err, Param1)

const std::string& CSDL_Signal_vms_err::toString(std::string& str)const {
  str = (const char*)sign();
  char charbuf[256];
  const uint8_t* err = getErr();

  str = (const char*)type();
  {
    sprintf(charbuf, "vms_err ncon=%d, signal=%d:%s, page=%d,%d\n", err[0],
        err[1], err[1]==0?"release_VMS":err[1]==1?"connect_VMS":err[1]==2?"setup_VMS":" "
       ,err[2],err[3]);
    str += charbuf;
  }
  return str;
};

//получить source Err
const uint8_t*  CSDL_Signal_vms_err::getErr() const{
 return &(sdl_signal->Param1);
}

//получить source Err
const uint32_t  CSDL_Signal_vms_err::getLenOfData() const{
 return 4;
}

#undef sdl_signal
#endif


////////////////////////////////////////////////////////////////////////////////
//CSDL_Signal_wr_Receiver_Table
#define sdl_signal ((yPDef_wr_Receiver_Table*)sdl_sig)
#ifdef sdl_signal

IMPL_SDL_SIGNAL_HEAD(wr_Receiver_Table);

IMPL_SDL_SIGNALOUT_HEAD(wr_Receiver_Table, Param1)

//
const std::string& CSDL_Signal_wr_Receiver_Table::toString(std::string& str)const {
  str = (const char*)sign();
  char charbuf[256];
  const uint8_t* mac = getDestMAC();

  str = (const char*)type();
  {
    sprintf(charbuf, ":row=%d:MAC=0x%X,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x"
        , getRowOfTable(), mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    str += charbuf;
  }
  return str;
};
  //Получить данные
  const uint32_t CSDL_Signal_wr_Receiver_Table::getRowOfTable() const{
    return sdl_signal->Param1;
  }
  const uint8_t* CSDL_Signal_wr_Receiver_Table::getDestMAC() const{
    return sdl_signal->Param2.A;
  }
  const uint16_t CSDL_Signal_wr_Receiver_Table::getDestPort() const{
    return ntohs(*(uint16_t*)(&sdl_signal->Param5.A));
  }

#undef sdl_signal
#endif


////////////////////////////////////////////////////////////////////////////////
//CSDL_Signal_wr_Sender_Table
#define sdl_signal ((yPDef_wr_Sender_Table*)sdl_sig)
#ifdef sdl_signal

IMPL_SDL_SIGNAL_HEAD(wr_Sender_Table);

IMPL_SDL_SIGNALOUT_HEAD(wr_Sender_Table, Param1)

//
const std::string& CSDL_Signal_wr_Sender_Table::toString(std::string& str)const {
  str = (const char*)sign();
  char charbuf[256];
  const uint8_t* mac = getDestMAC();

  str = (const char*)type();
  {
    sprintf(charbuf, ":row=%d:MAC=0x%X,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x"
        , getRowOfTable(), mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    str += charbuf;
  }
  return str;
};
  //Получить данные
  const uint32_t CSDL_Signal_wr_Sender_Table::getRowOfTable() const{
    return sdl_signal->Param1;
  }
  const uint8_t* CSDL_Signal_wr_Sender_Table::getDestMAC() const{
    return sdl_signal->Param2.A;
  }
  const uint32_t CSDL_Signal_wr_Sender_Table::getDestIP() const{
    return ntohl(* ((const uint32_t*) &(sdl_signal->Param3.A[0])));
  }
  const uint16_t CSDL_Signal_wr_Sender_Table::getDestPort() const{
    return ntohs(* (const uint16_t*)(&sdl_signal->Param5.A));
  }
  const uint16_t CSDL_Signal_wr_Sender_Table::getSrcPort() const{
    return ntohs(*(uint16_t*)(&sdl_signal->Param4.A));
  }
  const uint8_t* CSDL_Signal_wr_Sender_Table::getSSRC() const{
    return sdl_signal->Param6.A;
  }
  const uint32_t CSDL_Signal_wr_Sender_Table::getLenOfRTPFrame() const{
    return sdl_signal->Param7;
  }
  const uint8_t CSDL_Signal_wr_Sender_Table::getCodec() const{
    return sdl_signal->Param8;
  }
#undef sdl_signal
#endif

////////////////////////////////////////////////////////////////////////////////
//CSDL_Signal_vms_setup_env
#define sdl_signal ((yPDef_vms_setup_env*)sdl_sig)
#ifdef sdl_signal

  IMPL_SDL_SIGNAL_HEAD(vms_setup_env);
//  const char* CSDL_Signal_vms_setup_env_out::sign()
//  {return vms_setup_env->Name; }
//
//  const uint32_t CSDL_Signal_vms_setup_env_out::index() const
//  { return (uint32_t)vms_setup_env; }
//
//  const uint32_t CSDL_Signal_vms_setup_env_out::sizeOfData()const
//  {return (sizeof(yPDef_vms_setup_env) - sizeof(xSignalStruct)); }

  //IMPL_SDL_SIGNALOUT_HEAD(vms_setup_env, Param1)
  void* CSDL_Signal_vms_setup_env::GetSignal(void* _sdl_sig)const{
    yPDef_vms_setup_env* signal = (yPDef_vms_setup_env*)malloc(sizeof(yPDef_vms_setup_env));
    memcpy(& (signal->Param1), &( ((yPDef_vms_setup_env*)_sdl_sig)->Param1), sizeOfData());

    signal->Param2.Bits = new uint8_t[signal->Param2.Length];
    memcpy(signal->Param2.Bits,  ((yPDef_vms_setup_env*)_sdl_sig)->Param2.Bits, signal->Param2.Length);

    signal->Param3.Bits = new uint8_t[signal->Param3.Length];
    memcpy(signal->Param3.Bits,  ((yPDef_vms_setup_env*)_sdl_sig)->Param3.Bits, signal->Param3.Length);

    return signal;
  }

  //
  const std::string& CSDL_Signal_vms_setup_env::toString(std::string& str)const {
    str = (const char*)sign();
    char charbuf[256];

    str = (const char*)type();
    {
      sprintf(charbuf, ":id=%d, N=%s, D=%s"
          , getId(), getDigitsN().c_str(), getDigitsD().c_str());
      str += charbuf;
    }
    return str;
  };

  //
  const uint8_t CSDL_Signal_vms_setup_env::getId() const{
    return sdl_signal->Param1;
  }

  //
  std::string CSDL_Signal_vms_setup_env::getDigitsN() const{
    std::string _str;
    for(int i=0;i<sdl_signal->Param2.Length;i++){
      _str += (char)(sdl_signal->Param2.Bits[i]);
    };
    return  _str;
  }

  //
  std::string CSDL_Signal_vms_setup_env::getDigitsD() const{
    std::string _str;
    for(int i=0;i<sdl_signal->Param3.Length;i++){
      _str += (sdl_signal->Param3.Bits[i]);
    };
    return  _str;
  }

  const uint32_t CSDL_Signal_vms_setup_env::getSrcIP() const{
    return ntohl(* ((const uint32_t*) &(sdl_signal->Param4.A[0])) );
  }

  CSDL_Signal_vms_setup_env::~CSDL_Signal_vms_setup_env(){
    delete[] (sdl_signal->Param2.Bits);
    delete[] (sdl_signal->Param3.Bits);
  }

  //////////////////////////
  void* CSDL_Signal_vms_setup_env_in::GetSignal(void* _notused) const {
  yPDef_vms_setup_env* signal = (yPDef_vms_setup_env*) xGetSignal(
      vms_setup_env, ((yPDef_vms_setup_env*) sdl_sig)->Receiver, xEnv);

  memcpy(&(signal->Param1), &((sdl_signal)->Param1),
      sizeOfData());

  signal->Param2.Bits = new uint8_t[signal->Param2.Length];
  memcpy(signal->Param2.Bits, (sdl_signal)->Param2.Bits,
      signal->Param2.Length);

  signal->Param3.Bits = new uint8_t[signal->Param3.Length];
  memcpy(signal->Param3.Bits, (sdl_signal)->Param3.Bits,
      signal->Param3.Length);

  return signal;
}

  CSDL_Signal_vms_setup_env_in::CSDL_Signal_vms_setup_env_in(uint8_t _id,
      const string& _digi_n, const string& _digi_d, uint32_t _dsst_ip)
  {
    yPDef_vms_setup_env* signal
    = (yPDef_vms_setup_env*)malloc(sizeof(yPDef_vms_setup_env));


    signal->Param1 = _id;

    {
      int len  = _digi_n.length();
      signal->Param3.Bits = new unsigned char[len];
      for(int i=0; i<len; i++){
        (signal->Param3.Bits[i]) = _digi_n[i];
      };
      signal->Param3.Length = len;
      signal->Param3.IsAssigned = 1;
    }

    {
      int len  = _digi_d.length();
      signal->Param2.Bits = new unsigned char[len];
      for(int i=0; i<len; i++){
        (signal->Param2.Bits[i]) = _digi_d[i];
      };
      signal->Param2.Length = len;
      signal->Param2.IsAssigned = 1;
    }

    ///
    _dsst_ip = htonl(_dsst_ip);

    memcpy(&(signal->Param4.A[0]), &_dsst_ip, 4);

    sdl_sig = signal;
    SetReceiver(NULL);
  }

#undef sdl_signal
#endif

////////////////////////////////////////////////////////////////////////////////
//CSDL_Signal_vms_setup_res
#define sdl_signal ((yPDef_vms_setup_res*)sdl_sig)
#ifdef sdl_signal

  IMPL_SDL_SIGNAL_HEAD(vms_setup_res);

  //IMPL_SDL_SIGNALOUT_HEAD(vms_setup_env, Param1)
  void* CSDL_Signal_vms_setup_res::GetSignal(void* _sdl_sig)const{
    yPDef_vms_setup_res* signal = (yPDef_vms_setup_res*)malloc(sizeof(yPDef_vms_setup_res));
    memcpy(& (signal->Param1), &( ((yPDef_vms_setup_res*)_sdl_sig)->Param1), sizeOfData());

    signal->Param2.Bits = new uint8_t[signal->Param2.Length];
    memcpy(signal->Param2.Bits,  ((yPDef_vms_setup_res*)_sdl_sig)->Param2.Bits, signal->Param2.Length);

    signal->Param3.Bits = new uint8_t[signal->Param3.Length];
    memcpy(signal->Param3.Bits,  ((yPDef_vms_setup_res*)_sdl_sig)->Param3.Bits, signal->Param3.Length);
    //ip
    memcpy(&(signal->Param4),  &(((yPDef_vms_setup_res*)_sdl_sig)->Param4), 4);

    return signal;
  }

  //
  const std::string& CSDL_Signal_vms_setup_res::toString(std::string& str)const {
    str = (const char*)sign();
    char charbuf[256];
    const uint8_t id = getId();
    string N_digits = getDigitsN();
    string D_digits = getDigitsD();
    //string src_ip;    getSrcIP(src_ip);

    str = (const char*)type();
    {
      sprintf(charbuf, ":id=%d, N=%s, D=%s"
          , id, N_digits.c_str(), D_digits.c_str());
      str += charbuf;
    }
    return str;
  };

  //
  const uint8_t CSDL_Signal_vms_setup_res::getId() const{
    return sdl_signal->Param1;
  }

  //
  std::string CSDL_Signal_vms_setup_res::getDigitsN() const{
    std::string _str;
    for(int i=0;i<sdl_signal->Param3.Length;i++){
      _str += (char)(sdl_signal->Param3.Bits[i]);
    };
    return  _str;
  }

  //
  std::string CSDL_Signal_vms_setup_res::getDigitsD() const{
    std::string _str;
    for(int i=0;i<sdl_signal->Param2.Length;i++){
      _str += (sdl_signal->Param2.Bits[i]);
    };
    return  _str;
  }

  const uint32_t CSDL_Signal_vms_setup_res::getSrcIP() const{
    return ntohl(* ((const uint32_t*) &(sdl_signal->Param4.A[0])) );
  }

  CSDL_Signal_vms_setup_res::~CSDL_Signal_vms_setup_res(){
    delete[] (sdl_signal->Param2.Bits);
    delete[] (sdl_signal->Param3.Bits);
  }

#undef sdl_signal
#endif

////////////////////////////////////////////////////////////////////////////////
//CSDL_Signal_Info
#define sdl_signal ((yPDef_vms_info_env*)sdl_sig)
#ifdef sdl_signal

IMPL_SDL_SIGNAL_HEAD(vms_info_env);

IMPL_SDL_SIGNALOUT_HEAD(vms_info_env, Param1)

const std::string& CSDL_Signal_vms_info_env::toString(std::string& str)const {
  str = (const char*)sign();
  char charbuf[256];
  str = (const char*)type();
  {

    sprintf(charbuf, ":id=%d, info=%d,%d,%d", getId(), getData1(), getData2(), getData3());
    str += charbuf;
  }
  return str;
};


const uint8_t CSDL_Signal_vms_info_env::getId() const{
  return (sdl_signal->Param1);
}

//Получить info доп набора кнопок
const uint8_t CSDL_Signal_vms_info_env::getData1() const{
  return (sdl_signal->Param2);
}

//Получить info доп набора кнопок
const uint8_t CSDL_Signal_vms_info_env::getData2() const{
  return (sdl_signal->Param3);
}

//Получить info доп набора кнопок
const uint8_t CSDL_Signal_vms_info_env::getData3() const{
  return (sdl_signal->Param4);
}

#undef sdl_signal
#endif

////////////////////////////////////////////////////////////////////////////////
//CSDL_Signal_vms_release_env
#define sdl_signal ((yPDef_vms_release_env*)sdl_sig)
#ifdef sdl_signal

  IMPL_SDL_SIGNAL_HEAD(vms_release_env);

  IMPL_SDL_SIGNALOUT_HEAD(vms_release_env, Param1)

  //
  const std::string& CSDL_Signal_vms_release_env::toString(std::string& str)const {
    str = (const char*)sign();
    char charbuf[256];
    const uint8_t id = getId();

    str = (const char*)type();
    {
      sprintf(charbuf, ":id=%d", id);
      str += charbuf;
    }
    return str;
  };

  //
  const uint8_t CSDL_Signal_vms_release_env::getId() const{
    return sdl_signal->Param1;
  }

  void* CSDL_Signal_vms_release_env_in::GetSignal(void* _notused) const {
    yPDef_vms_release_env* signal
      = (yPDef_vms_release_env*) xGetSignal(vms_release_env,
          ((yPDef_vms_release_env*) sdl_sig)->Receiver, xEnv);

    memcpy(&(signal->Param1), &(((yPDef_vms_release_env*) sdl_sig)->Param1),
        sizeOfData());

    return signal;
  }

  CSDL_Signal_vms_release_env_in::CSDL_Signal_vms_release_env_in(uint8_t _id)
  {
    yPDef_vms_release_env* signal
    = (yPDef_vms_release_env*)malloc(sizeof(yPDef_vms_release_env));

    signal->Param1 = _id;

    sdl_sig = signal;
    SetReceiver(NULL);
  }

#undef sdl_signal
#endif

////////////////////////////////////////////////////////////////////////////////
//CSDL_Signal_in_connect
#define sdl_signal ((yPDef_vms_connect_env*)sdl_sig)
#ifdef sdl_signal

  IMPL_SDL_SIGNAL_HEAD(vms_connect_env);

  IMPL_SDL_SIGNALOUT_HEAD(vms_connect_env, Param1)

  //
  const std::string& CSDL_Signal_vms_connect_env::toString(std::string& str)const {
    //str = (const char*)sign();
    char charbuf[256];
    const uint8_t id = getId();

    str = (const char*)type();
    {
      sprintf(charbuf, ":id=%d"
          , id);
      str += charbuf;
    }
    return str;
  };

  //
  const uint8_t CSDL_Signal_vms_connect_env::getId() const{
    return sdl_signal->Param1;
  }

  void* CSDL_Signal_vms_connect_env_in::GetSignal(void* _notused) const {
    yPDef_vms_connect_env* signal
      = (yPDef_vms_connect_env*) xGetSignal(vms_connect_env,
          ((yPDef_vms_connect_env*) sdl_sig)->Receiver, xEnv);

    memcpy(&(signal->Param1), &(((yPDef_vms_connect_env*) sdl_sig)->Param1),
        sizeOfData());

    return signal;
  }

  CSDL_Signal_vms_connect_env_in::CSDL_Signal_vms_connect_env_in(uint8_t _id)
  {
    yPDef_vms_connect_env* signal
    = (yPDef_vms_connect_env*)malloc(sizeof(yPDef_vms_connect_env));

    signal->Param1 = _id;
    sdl_sig = signal;

    SetReceiver(NULL);
  }
#undef sdl_signal
#endif

////////////////////////////////////////////////////////////////////////////////
//CSDL_Signal_vms_alert_env
#define sdl_signal ((yPDef_vms_alert_env*)sdl_sig)
#ifdef sdl_signal

  IMPL_SDL_SIGNAL_HEAD(vms_alert_env);

  IMPL_SDL_SIGNALIN_HEAD(vms_alert_env, Param1)

  CSDL_Signal_vms_alert_env::CSDL_Signal_vms_alert_env(uint8_t _id):base()
  {
    yPDef_vms_alert_env* signal = (yPDef_vms_alert_env*)malloc(sizeof(yPDef_vms_alert_env));
    signal->Param1 = _id;
    sdl_sig = signal;
    SetReceiver(NULL);
  }
  //
  const std::string& CSDL_Signal_vms_alert_env::toString(std::string& str)const {
    //str = (const char*)sign();
    char charbuf[256];
    const uint8_t id = getId();

    str = (const char*)type();
    {
      sprintf(charbuf, ":id=%d"
          , id);
      str += charbuf;
    }
    return str;
  };

  //
  const uint8_t CSDL_Signal_vms_alert_env::getId() const{
    return sdl_signal->Param1;
  }
#undef sdl_signal
#endif

}
