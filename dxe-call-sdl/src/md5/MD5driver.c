/* MDDRIVER.C - test driver for MD2, MD4 and MD5
 */

/* Copyright (C) 1990-2, RSA Data Security, Inc. Created 1990. All
rights reserved.

RSA Data Security, Inc. makes no representations concerning either
the merchantability of this software or the suitability of this
software for any particular purpose. It is provided "as is"
without express or implied warranty of any kind.

These notices must be retained in any copies of any part of this
documentation and/or software.
 */

/* The following makes MD default to MD5 if it has not already been
  defined with C compiler flags.
 */
#ifndef MD
#define MD 5
#endif

#include <stdio.h>
#include <time.h>
#include <string.h>

#include "global.h"
#include "MD5.h"


/* Length of test block, number of test blocks.
 */
#define TEST_BLOCK_LEN 1000
#define TEST_BLOCK_COUNT 1000

//void MDString (char *);
//void MDTimeTrial (void);
//void MDTestSuite (void);
//void MDFile (char *);
//void MDFilter (void);
    /* static void MDPrint PROTO_LIST ((unsigned char [16])); */

#define MD_CTX MD5_CTX
#define MDInit MD5Init
#define MDUpdate MD5Update
#define MDFinal MD5Final


       /* Main driver.
  
       Arguments (may be any combination):
         -sstring - digests string
         -t       - runs time trial
         -x       - runs test script
         filename - digests file
         (none)   - digests standard input
        */
      /* int main (argc, argv)
         int argc;

         char *argv[];
         {
           int i;

           if (argc > 1)
          for (i = 1; i < argc; i++)
            if (argv[i][0] == '-' && argv[i][1] == 's')
              MDString (argv[i] + 2);
            else if (strcmp (argv[i], "-t") == 0)
              MDTimeTrial ();
            else if (strcmp (argv[i], "-x") == 0)
              MDTestSuite ();
            else
              MDFile (argv[i]);
           else
          MDFilter ();

           return (0);
         } */

/* Digests a string and prints the result.
 */
void MDString (unsigned char *string, unsigned char digest[16])
{
  MD_CTX context;
  /*unsigned char digest[16];*/
  unsigned int len = strlen ((const char*)string);

  MDInit (&context);
  MDUpdate (&context, string, len);
  MDFinal (digest, &context);

      /* printf ("MD%d (\"%s\") = ", MD, string);
         MDPrint (digest);
         printf ("\n"); */
}

//void MDString (char **string, unsigned char *digest[16]){
//  MDString((unsigned char*)*string, *digest);
//}
//
//void MDString(void* string, void* digest){
//  MDString((char**)string, (unsigned char *)digest);
//}
