#ifndef PAGECONTROLPROP_H
#define PAGECONTROLPROP_H

#include <QtGUI/QDialog>
#include <QtGUI/QPushButton>
#include <QtGui/QTableWidget>
#include <QtGui/QItemDelegate>
#include <QtGui/QTreeWidget>
#include "utils/CXMLReader.h"
#include <QtNetwork/QNetworkInterface>

namespace Ui {
    class pageControlProp;
}

class pageControlProp : public QDialog
{
    Q_OBJECT
protected:
  QWidget* parent;
public:
    void ReadParameters();
    void SaveParameters();
    void SaveGeneralParameters();
    void SaveRouteParameters();
    void CreateWidgets();
    void addAdapters();
public:
    explicit pageControlProp(QWidget *parent = 0);
    ~pageControlProp();

private:
    Ui::pageControlProp *ui;
    QPushButton* applButton;
    QPushButton* okButton;

    QList<QNetworkAddressEntry> addr;
    int mIndex;
private slots:
    void on_messageCheck_stateChanged(int _state);
    void OnApplyButton();
    void OnOkButton();
    void onItemClick(QTreeWidgetItem* item, int column);
    void OnApplyLic();
};

//класс через который задаем маску в ячейки
class CPropDelegate : public QItemDelegate
{
public:
  QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                            const QModelIndex &index) const;
public:
  CPropDelegate(QObject *parent = 0) : QItemDelegate(parent){

  };
  virtual ~CPropDelegate(){};

};


#endif // PAGECONTROLPROP_H
