﻿#ifndef CXADAPTERWIDGET_H
#define CXADAPTERWIDGET_H

#include <QtGui/QWidget>
#include <QtNetwork/QNetworkInterface>

#include "ui_CXAdapterWidget.h"

/*!
*/
class CXAdapterWidget : public QWidget
{
	Q_OBJECT
protected:
	Ui::CXAdapterWidget ui;
public:
	CXAdapterWidget(const QString& _humanReadableName
	    , const QNetworkAddressEntry& addr, QWidget* parent = 0);
	~CXAdapterWidget();

public:
	QString mIP;
	QString mMask;
};

#endif // CXADAPTERWIDGET_H
