#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGUI/QMainWindow>
#include <QtCore/QProcess>
#include <QtGUI/QMenu>
#include <QtGUI/QTableWidgetItem>
#include <QtGUI/QSystemTrayIcon>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
typedef QMainWindow base;
    Q_OBJECT
public:
    QProcess serverVMS;
protected:
    bool form_is_closing;
    QAction *minimizeAction;
    QAction *maximizeAction;
    QAction *restoreAction;
    QAction* exitAct;
    //QAction *quitAction;
    QAction* startAct;
    QAction* stopAct;
    QAction* propAct;


    QSystemTrayIcon *trayIcon;
    QMenu *trayIconMenu;

    QTimer *refreshTimer;
    bool isDemo;
protected:
    void CreateSysTray();
    void CreateTableInfo();
    void CreateCallbacks();

protected:
    virtual bool eventFilter( QObject *obj, QEvent *evt );
    virtual void closeEvent(QCloseEvent *event);
    virtual void hideEvent ( QHideEvent * event );
    virtual void showEvent(QShowEvent* event);

public:
    void SetNewNumber(QString _number, QString _name);

public:
    void DeleteRow(const QString& _number);
    void ReNumColumn();
public:
    void NewAbonent();
    void DeleteAbonent();
    int DeleteAbonent(const QString& _number);
    void ReplaceAbonent(const QString& _number);
    void RefreshAbonents();
    QString GetAbonentNumber(int _row);
    void SaveAbonent();
    void UndoLastChanges();
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

public slots:
    //обработчики меню
    void showNormal();
    bool close();
    void StartCore();
    void StopCore();
    void OpenProperties();

private slots:
    void on_tableWidget_cellDoubleClicked(int row, int column);
    void on_aboutFormButton_released();
    void on_openPropButton_released();
    void ProcessFinished(int _exitCode, QProcess::ExitStatus _exitStatus);
    void ProcessError(QProcess::ProcessError _error);
    void CurrentCellChanged(int _row, int _column, int _prev_row, int _prev_column);
    void checkChangesAbonents();
    void on_deleteAllButton_released();
    void on_deleteCheckedButton_released();
    void on_serverButton_released();
    void showHide(QSystemTrayIcon::ActivationReason _r);
};

#endif // MAINWINDOW_H
