﻿/** CService_Call_in.h
 * Описание классов обработчиков SDL-сообщений.
 *
 *  Created on: 13.04.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */

#ifndef CSDL_SIGNALS_H_
#define CSDL_SIGNALS_H_

#include <string>
#include <unistd.h>
#include <algorithm>

#include "define.h"
#include "utils/netutils.h"
#include "CVoiceMailService.h"

namespace AMT {

/**\brief CVMS_Signal_Init - иниц SDL-системы
 * Обработчик InitIPCOM_RQ сигнала
 */
class CVMS_Signal_Init: public SDL::CSDL_Signal_InitIPCOM_RQ {

  typedef SDL::CSDL_Signal_InitIPCOM_RQ base;
protected:
  ///ответный сигнал - подтверждение иниц.
  SDL::CSDL_Signal_InitIPCOM_I init_ipcom;
  ///мастер объект
  CVoiceMailService& vms;
public:
  ///Клонировать
  virtual SDL::ISDL_Signal* Clone(void* _sdl_sig) const {
    return new CVMS_Signal_Init(vms, GetSignal(_sdl_sig));
  }

  ///Обработчик принятия, наследники реализуют свои обработчики
  virtual void OnReceive(void* data) {
    //Получить отправителя сигнала
    vms.defReceiver = GetSender();
    //Залогировать сигнал
		IFLOG(TSTL1) {
				const uint16_t lowPortOfRTP = getLowerBoundaryOfPortsOfRTP();
				string str;
				IP(getMyIP()).toString(str);
				LOG2(TSTL1, " InitIPCOM_RQ UDP_port:%d, MyIP:%s\n", lowPortOfRTP, str.c_str());

				//std::string str1, str2;
				LOG(TSTL1, " the SDL subsystem started successfully\n");
			}

    //нижняя граница портов RTP
    vms.lowBounderOfRTP = getLowerBoundaryOfPortsOfRTP();

    //ответ на инициализацию, отправляем его
    vms.send(&init_ipcom);
    return;
  }

public:
  explicit CVMS_Signal_Init(CVoiceMailService& _vms, void* _sdl_sig = NULL) :
      base(_sdl_sig), vms(_vms) {
  }
};

/** \brief CVMS_Signal_Err - сигнал Err
 * Системные сообщения SDL-библиотеки
 */
class CVMS_Signal_Err: public SDL::CSDL_Signal_Err {

  typedef SDL::CSDL_Signal_Err base;
protected:
  ///Мастер объект
  CVoiceMailService& vms;
public:
  ///клонировать
  virtual SDL::ISDL_Signal* Clone(void* _sdl_sig) const {
    return new CVMS_Signal_Err(vms, GetSignal(_sdl_sig));
  }

  ///Обработчик принятия, наследники реализуют свои обработчики
  virtual void OnReceive(void* data) {
    //std::string str;toString(str);
    //Залогировать сигнал
    if(!vms.service_started)   return;
    IFLOG(TSTL1/*vms.err_log_level*/){
      const uint8_t* err_data = getErr();
      LOG10(TSTL1, " Err= %2X %2X %2X %2X %2X %2X %2X %2X %2X %2X\n"
          , err_data[0], err_data[1], err_data[2], err_data[3], err_data[4]
          , err_data[5], err_data[6], err_data[7], err_data[8], err_data[9]);
    }
    return; //base::OnReceive(data);
  }

public:
  explicit CVMS_Signal_Err(CVoiceMailService& _vms, void* _sdl_sig = NULL) :
      base(_sdl_sig), vms(_vms) {
  }
};

class CVMS_Signal_vms_err: public SDL::CSDL_Signal_vms_err {

  typedef SDL::CSDL_Signal_vms_err base;
protected:
  ///Мастер объект
  CVoiceMailService& vms;
public:
  ///клонировать
  virtual SDL::ISDL_Signal* Clone(void* _sdl_sig) const {
    return new CVMS_Signal_vms_err(vms, GetSignal(_sdl_sig));
  }

  ///Обработчик принятия, наследники реализуют свои обработчики
  virtual void OnReceive(void* data) {
    //std::string str;toString(str);
    //Залогировать сигнал
    if(!vms.service_started)   return;
    IFLOG(TSTL1/*vms.err_log_level*/){
      string s;
      LOG1(TSTL1, "%s\n", toString(s).c_str());
    }
    return; //base::OnReceive(data);
  }

public:
  explicit CVMS_Signal_vms_err(CVoiceMailService& _vms, void* _sdl_sig = NULL) :
      base(_sdl_sig), vms(_vms) {
  }
};

/**\brief CVMS_Signal_wr_Sender_Table - сигнал wr_Sender_Table
 */
class CVMS_Signal_wr_Sender_Table: public SDL::CSDL_Signal_wr_Sender_Table {
  typedef SDL::CSDL_Signal_wr_Sender_Table base;
protected:
  ///мастер-объект
  CVoiceMailService& vms;
public:
  ///Клонировать
  virtual SDL::ISDL_Signal* Clone(void* _sdl_sig) const {
    return new CVMS_Signal_wr_Sender_Table(vms, GetSignal(_sdl_sig));
  }

  ///Обработчик принятия, наследники реализуют свои обработчики
  virtual void OnReceive(void* data) {
    if(getDestPort() == 0){
      //LOG1(TSTL1, "wr_Sender_Table(%d): getSrcPort() == 0\n", getRowOfTable());
      return ;
    }
//    ICallManager_SIP* call = vms.getCallManager(getRowOfTable());
//    if(NULL == call){
//      LOG1(WARN, "wr_Sender_Table(%d): no such callManager\n", getRowOfTable());
//    }else
//      call->putWrSenderTable(getDestPort(), getDestIP());
    vms.setRTP_dst(getRowOfTable(), getDestPort(), getDestIP());
    if(!vms.service_started) return;

    IFLOG(TSTL1){
       std::string str, str1;
       LOG3(TSTL1, "SDL wr_Sender_Table: id=%d, S_UDP_port=%d, D_UDP_port=%d\n"
           , getRowOfTable(), getSrcPort(), getDestPort());
       LOG5(TSTL1, ", SSRC=%d, SizePacket=%d, Codec=%d, MAC=%s, IPaddr=%s\n"
           , 0, getLenOfRTPFrame(), getCodec(), MAC(getDestMAC()).toString(str1).c_str(),
           (IP(getDestIP()).toString(str)).c_str());
     }

    return; //base::OnReceive(data);
  }

public:
  explicit CVMS_Signal_wr_Sender_Table(CVoiceMailService& _vms, void* _sdl_sig = NULL) :
      base(_sdl_sig), vms(_vms) {
  }
};

/**\brief CVMS_Signal_wr_Receiver_Table - сигнал wr_Receiver_Table
 *
 */
class CVMS_Signal_wr_Receiver_Table: public SDL::CSDL_Signal_wr_Receiver_Table {
  typedef SDL::CSDL_Signal_wr_Receiver_Table base;
protected:
  ///Мастер-объект
  CVoiceMailService& vms;
public:
  ///Клонировать
  virtual SDL::ISDL_Signal* Clone(void* _sdl_sig) const {
    return new CVMS_Signal_wr_Receiver_Table(vms, GetSignal(_sdl_sig));
  }

  ///Обработчик принятия, наследники реализуют свои обработчики
  virtual void OnReceive(void* data) {
    //Задать RTP-порты для исходящего трафика
    if(getDestPort() == 0)  return ;

    vms.setRTP_in(getRowOfTable(), getDestPort());

    if(!vms.service_started)   return;
    LOG2(/*vms.err_log_level*/TSTL1, " wr_Receiver_Table: Ind=%d, D_UDP_port=%d\n", getRowOfTable(), getDestPort());
    return;//base::OnReceive(data);
  }

public:
  explicit CVMS_Signal_wr_Receiver_Table(CVoiceMailService& _vms, void* _sdl_sig = NULL) :
      base(_sdl_sig), vms(_vms) {
  }
};

/**\brief CVMS_Signal_UDPDR_long - сигнал UDPDR_long
 * Исходящий UDP-пакет.
 */
class CVMS_Signal_UDPDR_long: public SDL::CSDL_Signal_UDPDR_long {
  typedef SDL::CSDL_Signal_UDPDR_long base;
protected:
  ///мастер-объект
  CVoiceMailService& vms;
public:
  ///клонировать
  virtual SDL::ISDL_Signal* Clone(void* _sdl_sig) const {
    return new CVMS_Signal_UDPDR_long(vms, GetSignal(_sdl_sig));
  }

  //обработчик для алгоритма замены
  static bool IsReturn(char i) {
    return (i == '\r');
  }

  const std::string& toString(std::string& str) const {
    str = "UDPDR_long";
    IFLOG(TSTL1) {
      str += ": IP="; str += IP(getIP()).toString();  str += ", data=\n";
      //залогировать сигнал
      const int maxLen = 512;
      int l =  getLengthOfData(); if(l>maxLen) l = maxLen;
      str.append( (char*)getData(), l);
      std::replace_if(str.begin(), str.end(), IsReturn, ' ');
    }
    return str;
  }

  ///Обработчик принятия, наследники реализуют свои обработчики
  virtual void OnReceive(void* data) {
    //залогировать сигнал
    string s;
    LOG1(TSTL1, "SDL: %s\n", toString(s).c_str());
//    vms.sendToSIP(getIP(), getData(), getLengthOfData());
    //Отправить данные пакета в сеть
    const uint8_t* buf = getData();
    const uint32_t len = getLengthOfData();
    vms.sip_other.sin_addr.S_un.S_addr = htonl(getIP());
    if (-1 == (sendto(vms.socket_sipudp_out, (const char*) buf, len, 0,
									 (sockaddr*) &(vms.sip_other), sizeof(sockaddr_in))))
    	throw E(WSAGetLastError(), "VMS:UDPDR_long:sendto");
    //
    return; //base::OnReceive(data);
  }

 public:
  explicit CVMS_Signal_UDPDR_long(CVoiceMailService& _vms, void* _sdl_sig = NULL) :
      base(_sdl_sig), vms(_vms) {
  }
};

/**\brief CVMS_Signal_UDPDI_long - сигнал UDPDI_long
 * Входной для SDL сигнал.
 */
class CVMS_Signal_UDPDI_long: public SDL::CSDL_Signal_UDPDI_long {
  typedef SDL::CSDL_Signal_UDPDI_long base;
public:
  static bool IsReturn(char i) {
    return (i == '\r');
  }

  const std::string& toString(std::string& str) const {
  	str = "UDPDI_long";
  	IFLOG(TSTL1) {
  		str += ": IP="; str += IP(getIP()).toString();  str += ", data=\n";
  		//залогировать сигнал
  		const int maxLen = 512;
  		int l =   UDPDI_long_data->Param4; if(l>maxLen) l = maxLen;
  		str.append((char*)UDPDI_long_data->Param5, l);
  		std::replace_if(str.begin(), str.end(), IsReturn, ' ');
  		//LOG2(DEVL1, " UDPDI_long: IP=%s, data=\n%s\n", IP(getIP()).toString(str).c_str(), s);
  	}
  	return str;
  }

//
//  const std::string& toString(std::string& str) const {
//    str = "UDPDI_long";
//    IFLOG(INFO) {
//      char s[101]; string str1;
//      str += ": IP="; str += IP(getIP()).toString(str1);  str += ", data= ";
//      if (INFO == log_level) {
//        memcpy(s, UDPDI_long_data->Param5, 100);
//        char* s1 = strchr(s, '\r');
//        s1[0] = '\0';
//        str1 = (char*) s;
//      } else {
//        char s[1372];
//        memcpy(s, UDPDI_long_data->Param5, UDPDI_long_data->Param4);
//        s[UDPDI_long_data->Param4] = '\0';
//        str1 = (char*) s;
//      }
//      std::replace_if(str1.begin(), str1.end(), IsReturn, ' ');
//      str += str1;
//    }
//
//    return str;
//  }

public:
  explicit CVMS_Signal_UDPDI_long(uint32_t _sIP, uint16_t _sPort, uint16_t _dPort,
      uint32_t _len, uint8_t* _upd_data) :
      base(_sIP, _sPort, _dPort, _len, _upd_data) {
  }
};

/**\brief CVMS_Signal_Setup - Сигнал входящий вызов.
 * Запрос на установку соединения. для SDL выходной. */
class CVMS_Signal_Setup: public SDL::CSDL_Signal_vms_setup_env {
  typedef SDL::CSDL_Signal_vms_setup_env base;
protected:
  ///мастер-объект
  CVoiceMailService& vms;
public:
  ///клонировать
  virtual SDL::ISDL_Signal* Clone(void* _sdl_sig) const {
    return new CVMS_Signal_Setup(vms, GetSignal(_sdl_sig));
  }

  ///Обработчик принятия
  virtual void OnReceive(void* data) {
    //Определить номер и доб номер абонента
	  string str;toString(str);
    LOG4(TSTL1, "SDL Setup, id=%d, number=%s, add=%s, ip=%x\n", getId(), getDigitsN().c_str(), getDigitsD().c_str(), getSrcIP());
    //Запрос на соединение
    vms.Invite(getId(), getDigitsD(), getDigitsN(), getSrcIP(), "nocode");
    return; //base::OnReceive(data);
  }

public:
  explicit CVMS_Signal_Setup(CVoiceMailService& _vms, void* _sdl_sig = NULL) :
      base(_sdl_sig), vms(_vms) {
  }
};

/**\brief CVMS_Signal_Setup_out - Сигнал исходящий вызов.
 * Запрос на установку соединения. для SDL входной. */
class CVMS_Signal_Setup_out: public SDL::CSDL_Signal_vms_setup_env_in {
  typedef SDL::CSDL_Signal_vms_setup_env_in base;
public:
  explicit CVMS_Signal_Setup_out(uint8_t _id,
      const std::string& _digi_n, const std::string& _digi_d, uint32_t _dsst_ip) :
      base(_id, _digi_n, _digi_d, _dsst_ip)
  {  }
};

/**\brief CVMS_Signal_Release - сигнал Release
 * Отбой соединения. для SDL выходной. */
class CVMS_Signal_Release: public SDL::CSDL_Signal_vms_release_env {
  typedef SDL::CSDL_Signal_vms_release_env base;
protected:
  ///мастер-объект
  CVoiceMailService& vms;
public:
  ///клонировать
  virtual SDL::ISDL_Signal* Clone(void* _sdl_sig) const {
    return new CVMS_Signal_Release(vms, GetSignal(_sdl_sig));
  }

  ///Обработчик принятия
  virtual void OnReceive(void* data) {
    //Отбой соединения, по его номеру
  	LOG1(TSTL1, "SDL Release, id=%d\n", getId());
    vms.Release(getId());
    return; //base::OnReceive(data);
  }

public:
  explicit CVMS_Signal_Release(CVoiceMailService& _vms, void* _sdl_sig = NULL) :
      base(_sdl_sig), vms(_vms) {
  }
};

/**\brief CVMS_Signal_Release_out - сигнал Release
 *  Отбой соединения. для SDL входной. */
class CVMS_Signal_Release_out: public SDL::CSDL_Signal_vms_release_env_in {
  typedef SDL::CSDL_Signal_vms_release_env_in base;
public:
  explicit CVMS_Signal_Release_out(uint8_t _id)
    :base(_id)
  {  }
};

/**\brief CVMS_Signal_Connect - сигнал Connect
 * Принятие соединения. для SDL выходной. */
class CVMS_Signal_Connect: public SDL::CSDL_Signal_vms_connect_env {
  typedef SDL::CSDL_Signal_vms_connect_env base;
protected:
  ///мастер-объект
  CVoiceMailService& vms;
public:
  ///клонировать
  virtual SDL::ISDL_Signal* Clone(void* _sdl_sig) const {
    return new CVMS_Signal_Connect(vms, GetSignal(_sdl_sig));
  }

  ///Обработчик принятия
  virtual void OnReceive(void* data) {
    //Отбой соединения, по его номеру
    LOG1(TSTL1, "SDL Connect, id=%d\n", getId());
    vms.Connect(getId());
    return; //base::OnReceive(data);
  }

public:
  explicit CVMS_Signal_Connect(CVoiceMailService& _vms, void* _sdl_sig = NULL) :
      base(_sdl_sig), vms(_vms) {
  }
};

/**\brief CVMS_Signal_Connect - сигнал Connect
 * Принятие соединения. для SDL входной. */
class CVMS_Signal_Connect_out: public SDL::CSDL_Signal_vms_connect_env_in {
  typedef SDL::CSDL_Signal_vms_connect_env_in base;
public:
  explicit CVMS_Signal_Connect_out(uint8_t _id)
    :base(_id)
  {  }
};

/** \brief CVMS_Signal_Info - сигнал Info
 * Cообщения SIP-протокола, поступающие после установки соединения
 */
class CVMS_Signal_Info: public SDL::CSDL_Signal_vms_info_env {
  typedef SDL::CSDL_Signal_vms_info_env base;
protected:
  ///Мастер объект
  CVoiceMailService& vms;
public:
  ///клонировать
  virtual SDL::ISDL_Signal* Clone(void* _sdl_sig) const {
    return new CVMS_Signal_Info(vms, GetSignal(_sdl_sig));
  }

  ///Обработчик принятия, наследники реализуют свои обработчики
  virtual void OnReceive(void* data) {
    //Залогировать сигнал
    IFLOG(TSTL1){
      string str; toString(str);
      LOG1(TSTL1, " DEVL1 %s\n", str.c_str());
    }
    try{
      vms.putSIP_Info(getId(), getData1(), getData2(), getData3());
    }catch(P_EXC e){
      E_CATCH(e);
    }
    return; //base::OnReceive(data);
  }

public:
  explicit CVMS_Signal_Info(CVoiceMailService& _vms, void* _sdl_sig = NULL) :
      base(_sdl_sig), vms(_vms) {
  }
};

}

#endif /* CSERVICE_CALL_IN_H_ */
