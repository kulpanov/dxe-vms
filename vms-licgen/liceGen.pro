#-------------------------------------------------
#
# Project created by QtCreator 2014-11-04T15:50:11
#
#-------------------------------------------------

QT       += core gui xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app

SOURCES += src/main.cpp\
        src/dialog.cpp\
        src/CXMLReader.cpp

HEADERS  += src/dialog.h\
			src/CXMLReader.h\
			src/crypter.h

FORMS    += src/dialog.ui

QMAKE_CXXFLAGS += -std=c++11
