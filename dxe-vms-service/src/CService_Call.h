/**CService_Call_in.h
 * Объявления класса входящего вызова.
 *  Created on: 13.04.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */

#ifndef CSERVICE_CALL_IN_H_
#define CSERVICE_CALL_IN_H_

#include <string>
#include <unistd.h>
#include <vector>
#include <queue>

#include "define.h"
#include "utils/netutils.h"

#include "CVoiceMailService.h"

namespace AMT {

/** \brief Управляющий соединением.
 * Обработчик из пула соединений, обрабаытвает входящие и исходящие соединения.
 */
class CService_Call {

protected:
  ///мастер -объект
  CVoiceMailService& vms;
  ///номер соединения
  const int id;

protected:
  ///Входящий RTP-порт
  IP_PORT rtp_in;
  ///Исходящий RTP-порт
  IP_PORT rtp_out;
  ///адрес источника запроса на соединение
  IP rtp_gw;

protected:
  ///основной mutex
  pthread_mutex_t lock;
  ///основной condvar
  pthread_cond_t cond;
  ///стартовый барьер
  pthread_barrier_t start_barier;
  ///condvar локирования при setup
  pthread_cond_t setup_cond;
  ///condvar локирования при setRTP_dst
  pthread_cond_t rtpDest_cond;
protected:
  ///запись абонента
  CIniFileXML record_call;

protected:
  ///статус соединения @see EStatus
  int status;
public:
  ///статусы соединения
  enum EStatus {
    _idle, _busy, _record, _play
  };
protected:
  ///очередь входящиз rtp-пакетов
  tVoiceMsg q_rtp_in;
  ///счётчик RTP-пакетов, заполняет соотв поле внутри RTP-пакета
  int rtp_out_count;
  ///timeStamp RTP-пакетов, заполняет соотв поле внутри RTP-пакета
  int rtp_out_timeStamp;
protected:
  ///сокет для выходного RTP-потока
  int socket_rtp_out;
  ///поля сокета
  sockaddr_in addr_rtp_out;
  ///сокет для входного RTP-потока
  int socket_rtp_in;
  ///поля сокета
  sockaddr_in addr_rtp_in;
  ///condvar для управления слушателя RTP-потока
  pthread_cond_t socket_cond;
  ///mutex для управления слушателя RTP-потока
  pthread_mutex_t socket_mutex;


protected:
  ///команды в поток
  enum ECmd {
    _ok, _invite, _release, _connect, _setup, _rtp_cancel, _SIP_INFO
  };
  ///команды в поток
  struct SMailBox {
    int cmd;
    int id;
    std::string data1;
    std::string data2;
    uint32_t data_32;
  };
  SMailBox mailBox;

protected:
  ///поток слушающий RTP сообщения
  pthread_t rtp_listener;
protected:
  ///thread_RTP_listener
  static void* thread_RTP_listener(void* _service) {
    try {
      CService_Call* service = (CService_Call*) _service;
      service->RTP_listener();
    } catch (P_EXC e) {
      E_CATCH2(e);
    }
    CVoiceMailService::Exit(1);
    return NULL;
  }
  ///метод слушатель RTP-потока
  void* RTP_listener();

public:
  ///поток слушающий CMD сообщения
  pthread_t callIn_script;
protected:
  ///thread_release_script
  static void* thread_main(void* _service) {
    CService_Call* srv = (CService_Call*) ((_service));
    try {
      srv->main_thread();
    } catch (P_EXC e) {
      E_CATCH2(e);
    }
    srv->AfterCall(" ");
    pthread_cancel(srv->rtp_listener);

    CLOG1(ALRM, "VMS:CService_Call_in(%d):exit from thread\n", srv, srv->id);

    pthread_detach(pthread_self());
    pthread_exit(NULL);
    return NULL;
  }
  ///основной метод класса-потока
  void* main_thread();

  ///Обработать входящий вызов
  void processCallIn(const CString& _digiD, const CString& _digiN, const IP& _s_ip);

  ///Обработать входящий вызов
  void processCallOut(const CString& _digiD, const CString& _digiN, const IP& _s_ip);

  ///обработчик сценария "отбой"
  void release_script();

  ///обработчик сценария "записать"
  void record_script(const CString& digiN, const IP& _s_ip);

  ///обработчик сценария "прослушать"
  void play_script(const CString& digiN,  bool _notifyed = false);

  ///обработчик сценария "удалить прослушанные"
  void delete_script(const CString& digiN);

  ///обработчик сценария "очистить все"
  void clear_script(const CString& digiN);

  ///Уведомить абонента
  void notify_script(const CString& _digiN, const IP& _s_ip);
protected:

  ///Проиграть абоненту подготовленное сообщение
  /** Проигрывает заранее подготовленное сообщение
   * @param _qmsg подготовленное сообщение, в виде очередь RTP-пакетов.
   * @param _add_cmd доп обрабатываемые команды, zero-terminate array.
   * @return код возврата, <0 выход из звонка */
  int playVoiceMsg(tVoiceMsg & _qmsg, int _add_cmd = 0);
  ///записать входящий RTP-поток
  int recordCallMsg();
  ///Отибить звонок, выполнить конец звонка
  void doEndOfCall();

protected:
  ///очистить очередь RTP-потока
  void Clear_qmsg(tVoiceMsg& _qmsg);
  ///Сохранить очередь RTP-потока в файл
  void Save_qmsg(tVoiceMsg & _qmsg, char* fileName);
  ///Загрузить очередь RTP-потока из файла
  int Load_qmsg(tVoiceMsg & _qmsg, const char* fileName);

protected:
  /** \brief Обработка перед установкой разрешения
   * @param _digiN номер абонента
   * @return номер сценария или ошибки
   */
  void BeforeCall(const CString& _digiN);

  ///Обработка после окончания вызова
  void AfterCall(const CString& _digiN);

public:
  //Public methods, доступны из клиентского кода
  ///Id входящего соединения
  volatile int getId() {
    return id;
  }

/** Установить порт входящего потока
   * @param _rtp_in номер порта */
  void SetRTP_in(int _rtp_in) {
    LOCK(&lock);
    rtp_in = _rtp_in;
    UNLOCK(&lock);
  }

  /** \brief Установить RTP порт назначения исходящего потока.
   * vms передаёт данные сигнали потокам - обработчикам соединения.
   * @param _d_rtp - RTP-порт для исходящих соединений
   * @param _d_ip - IP для исходящих соединений   */
  void setRTP_dst(int _d_rtp, int _d_ip){
    LOCK(&lock);
    rtp_out = _d_rtp;
    rtp_gw  = _d_ip;
    LOG3(TSTL1, "VMS: setRTP_dst: id=%d, d_rtp=%d, d_ip=%x\n", getId(), _d_rtp, _d_ip);
    SIGNAL(&rtpDest_cond);
    UNLOCK(&lock);
  }

  /** \brief Подать SIP Info сигнал
   * Принять и обработать SIP INFO сигнал
   * @param _data1 данные
   */
  void putSIP_Info(int _data1, int _data2, int _data3);

  ///Получить тек статус-состояние объекта
  int GetStatus() {
    LOCK(&lock);
    int res = status;
    UNLOCK(&lock);
    return res;
  }

  /**Обработать входящие соединение
   * @param _digiD доп номер сцерания
   * @param _digiN номер абонента
   * @param _s_ip источник сигнала, IP-адрес
   */
  void Invite(const std::string & _digiD, const std::string & _digiN, const uint32_t _s_ip);

  /** Отбить входщие соединение
   */
  void Release();

  /** Принять входщие соединение
   */
  void Connect();

  /**Инициировать исходящее соединение
   * @param _digiD доп номер сцерания
   * @param _digiN номер абонента */
  void Setup(const std::string & _digiD, const std::string & _digiN, const uint32_t _s_ip);

public:
  ///конструктор
  CService_Call(CVoiceMailService& _vms, int _id);
  ///деструктор
  virtual ~CService_Call();
};

}

#endif /* CSERVICE_CALL_IN_H_ */
