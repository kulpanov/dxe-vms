/** rtp.cpp
 * Метод-слушатель входящего RTP-потока
 *  Created on: 23.03.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */
#include <string>
#include <unistd.h>
#include <queue>
#include "define.h"
#include "CVoiceMailService.h"
#include "CService_Call.h"

namespace AMT {
#define BUFLEN 2048

//RTP_listener - слушатель RTP-потока
void* CService_Call::RTP_listener() {
  uint8_t buf[BUFLEN];
  sockaddr_in rtp_other;
  int slen = sizeof(sockaddr_in);

  LOG1(TSTL1, "VMS:RTP_listener(%d) started\n", id);
  //синхронизировать старт потока с CService_Call_in
  pthread_barrier_wait(&start_barier);

  try {
    ///ждём остальных
    CPacket_RTP* pack;
    int len = 0;

    do{
      //ожидаем разрешение на прослушку от CService_Call_in
      ///@see CService_Call_in::BeforeCall
      LOCKandWAITSIGNAL(&socket_cond, &socket_mutex);
      //...
      REPLYandUNLOCK(&socket_cond, &socket_mutex);

//      if (WaitForSingleObject(hTimer, INFINITE) != WAIT_OBJECT_0){
//        LOG1(ALRM, "WaitForSingleObject failed (%d), %s\n", GetLastError());
//        throw E(E_UNEXPECTED, "WaitForSingleObject failed");
//      }
      bool run_loop = true;
      do {
        //цикл прослушивания RTPсообщений
        if (-1
            == (len = recvfrom(socket_rtp_in, (char*) buf, BUFLEN, 0,
                (sockaddr *) &rtp_other, &slen))) {
//          if ((len == WSAGetLastError()) == 1) {
//            //был вызван closesocket(), вернутся в верхний цикл
//            run_loop = false;
//            continue; //goto return!
//          } else
          //throw E(WSAGetLastError(), "VMS:SIP_listener:recvfrom");

          LOG2(INFO, "VMS:RTP_listener(%d): listening has been broken, %d\n", id, WSAGetLastError());
          //return NULL;
          run_loop = false;
          continue; //goto return!
        }

        if (_record == status) {
          pack = new CPacket_RTP(buf, len);
          //накапливаем входящие пакеты
          LOCK(&lock);
          q_rtp_in.push_back(pack);
          UNLOCK(&lock);
        }
      }while (run_loop);
    }while (1);

  } catch (P_EXC e) {
    LOG1(ALRM, "VMS:RTP_listener(%d) thread will be destroyed, reason:\n", id);
    E_CATCH(e);

    LOCK(&lock);
    mailBox.cmd = _rtp_cancel;
    SIGNAL(&cond);
    UNLOCK(&lock);
  }

  return NULL;
}

}
