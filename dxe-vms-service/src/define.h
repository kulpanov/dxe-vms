/** define.h
 * Общие настройки компиляции.
 *
 *  Created on: 21.12.2010
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */

#ifndef DEFINE_H_
#define DEFINE_H_

#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include "debug.h"
#include "utils/utils.h"

#define EOK 0
#define ASSERT assert
///Отладочный макро
#undef WITH_CMD_THREAD

///Номер и дата версии
#define VERSION_SHORT "DXE-VMS-K01.10"

#define VERSION VERSION_SHORT " " __DATE__ " " __TIME__

/** Сделать.
TODO: сделать, общее
 - vista, win7 - права администратора?

История.
см history.txt
*/

#endif /* DEFINE_H_ */
