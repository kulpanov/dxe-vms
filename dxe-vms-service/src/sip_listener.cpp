/** sip_listener.cpp
 * Слушатель SIP-сообщений
 *  Created on: 23.03.2011
 *      \author Alexander Kulpanov, post@kulpanov.ru
 */
#include <string>
#include <unistd.h>

#include "define.h"

#include "vms_signals.h"
#include "CVoiceMailService.h"

//extern SDL::CSDL_system vms;

namespace AMT {

#define BUFLEN 2048
#define NPACK 10

//Метод-слушатель SIP-сообщений
void* CVoiceMailService::SIP_listener() {
  //структуры сокетов, моего и отправителя
  struct sockaddr_in sip_me, sip_other;

  int s = -1, slen = sizeof(sip_other);
  uint8_t buf[BUFLEN];
  LOG(TSTL1, "VMS:SIP_listener started\n");
  try {
    //создать слушающий сокет
    if ((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
      throw E(WSAGetLastError(), "VMS::SIP_listener::socket");
    //заполнить поля сокета и связать их с сокетом
    memset((char *) &sip_me, 0, sizeof(sip_me));
    sip_me.sin_family = AF_INET;
    sip_me.sin_port = htons(sipPort_in);
//    sip_me.sin_addr.s_addr = htonl(INADDR_ANY);
    sip_me.sin_addr.s_addr = htonl(myIP.getIP());

    if (bind(s, (sockaddr *) &sip_me, sizeof(sip_me)) == -1)
      throw E(WSAGetLastError(), "VMS::SIP_listener::bind");
    LOG1(TSTL1, "VMS: for sip in I use %s\n", myIP.toString().c_str());

    ///ждём остальных @see CVoiceMailService::start(std::string _opts)
    pthread_barrier_wait(&start_barier);
    int len = 0;
    while (1) {
      //принять пакет
      if (-1 ==
         (len = recvfrom(s, (char*) buf, BUFLEN, 0, (sockaddr *) &sip_other,
                          &slen)))
        throw E(WSAGetLastError(), "VMS:SIP_listener:recvfrom");

//      printf("Received packet from %s:%d\nData:\n%s\n", inet_ntoa(sip_other.sin_addr),
//          ntohs(sip_other.sin_port), buf);
      //предобработка входящего invite
      {
    	  //затычка для factoryNumber
    	  const string str_buf = (char*) buf;
    	  if(str_buf.find("INVITE sip:") == 0){
    	  	LOG(DEVL1, "invite detect\n")
    		  auto it = str_buf.find("User-Agent: ") + 12;
    	  	auto endPos = str_buf.find("\n", it) - 1;
    		  auto str_pass = str_buf.substr(it, endPos-it);
    	  	LOG2(DEVL1, "useragent=%s:\"%s\"\n", str_pass.c_str(), crypt.crypt(str_pass).c_str());
    		  if(! crypt.match(str_pass)){
    		  	LOG1(ERRR, "Wrong License key, key=%s\n", str_pass.c_str());
//    			  it = str_buf.find("To: <sip:301@");
//    			  if(it != string::npos){
//    				  buf[it+9] = '4';//it get unknown scenario!
//    			  }
    		  	//ignore this invite
    		  	continue;
    		  }
    	  }

        //IP- брикета, что мне прислал пакет
        IP gwIP = ntohl(sip_other.sin_addr.S_un.S_addr);
        //создаём и отправляем в SDL-систему
        CVMS_Signal_UDPDI_long udp_packet(
#ifdef WITH_CMD_THREAD
            0xc0a8e902, 5060, 5060
#else
            gwIP, sipPort_in, sipPort_out
#endif
            , len, buf);
        send(&udp_packet);
      }
    }

  } catch (P_EXC e) {
    //в солучае искл ситуации логируем и выходим
    E_CATCH(e);
  }

  closesocket(s);
  CVoiceMailService::Exit(1);

  return NULL;
}

}
