// IniFile.cpp:  Implementation of the CIniFileXML class.
// Written by:   Adam Clauss
// Email: cabadam@houston.rr.com
// You may use this class/code as you wish in your programs.  Feel free to distribute it, and
// email suggested changes to me.
//
// Rewritten by: Shane Hill
// Date:         21/08/2001
// Email:        Shane.Hill@dsto.defence.gov.au
// Reason:       Remove dependancy on MFC. Code should compile on any
//               platform.
//////////////////////////////////////////////////////////////////////

// C++ Includes
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

// C Includes
#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>

// Local Includes
#include "../../define.h"
#include "iniFileXML.h"

#if defined(WIN32)
#define iniEOL endl
#else
#define iniEOL '\r' << endl
#endif

//int size_of_stream(fstream& f){
//    /* Save the current position. */
//    long save_pos = f.tellg( );
//    /* Jump to the end of the file. */
//    f.seekg(0, ios::end );
//    /* Get the end position. */
//    long size_of_file = f.tellg( );
//    /* Jump back to the original position. */
//    f.seekg( save_pos, ios::beg );
//    return( size_of_file );
//};

/*Создать с готовым именем
 */
CIniFileXML::CIniFileXML( string const iniPath)
{
  Path( iniPath);
  //caseInsensitive = true;
}

CIniFileXML::CIniFileXML( ){
  Path( "" );
  //caseInsensitive = true;
};

//Разобрать XML-документ
bool CIniFileXML::ReadFile(int minsize)
{
  Clear();
  if(doc.LoadFile(path.c_str()) == false){
    if(doc.Error())
        LOG2(ERRR, "Error in %s: %s\n", doc.Value(), doc.ErrorDesc() );
    return false;
  };
  return true;
}

//bool getline(string &_str, string &line){
//  if(_str.size()==0)  return false;
//  string::size_type pLeft=0, pRight = _str.find('\n');
//  line = _str.substr( pLeft, pRight );
//  _str = _str.erase(0,pRight+1);
//  return true;
//};

//Прочитать из строки XML-документ
bool CIniFileXML::ReadString(const char* ini_str)
{
  Clear();
  doc.Parse(ini_str);
  if(doc.Error()){
    LOG2(ERRR, "Error in %s: %s\n", doc.Value(), doc.ErrorDesc() );
    return false;
  };
  return true;
};

//Записать XML-документ по текущему заданному пути
bool CIniFileXML::WriteFile(int minsize)
{
  if(path.size() < 1) return false;
  return doc.SaveFile(path.c_str()); 
}

//Записать XML-документ по текущему заданному пути из данной XML-строки
bool CIniFileXML::WriteFile(string &ini_str){
  if(path.size() < 1) return false;
  if(ReadString(ini_str.c_str()))
    return doc.SaveFile(path.c_str());
  else
    return false;
};

//очистить текущее значения XML-документа
void CIniFileXML::Erase()
{
  doc.Clear();
};

//разбить строку, выбрать подстроку до символа /
string GetName(string& _path){
  string name = "";
  //проверяем на не пустой путь
  if(_path.size() < 1) return (name = "-1");  
  int k = _path.find("/");  
  if (k == (int)_path.npos){
    //вме элементы выбрали это последний
    name = _path;
    _path = "";
  }else name = _path.substr(0, k);
  //удаляем выбранный элемент
  _path.replace(0, k + 1, "");
  return name;
};

//Получить значение элемента
TiXmlElement* CIniFileXML::Get(const char* _elementPath){
  string path = _elementPath;
  string nextName = GetName(path);
  TiXmlElement* elem = doc.RootElement();
  elem = (TiXmlElement*)elem->FirstChild();
  string tagName;
  while(elem != NULL){
    //смотрим совпали ли имена
    tagName = elem->Value();
    if(tagName/*Attribute("name")*/ != nextName){
      while((elem = (TiXmlElement*)elem->NextSiblingElement()) != 0){
        //нашли тогда выходим
        tagName = elem->Value();
        if(tagName/*Attribute("name")*/ == nextName)  break;
      };
      //не нашли
      if(elem == 0) return NULL;
    }else{
      //ищем следующее имя
      nextName = GetName(path);
      //путь кончился возвращаем элемент
      if(nextName == "-1") break;
      //берем следующий элемент
      elem = (TiXmlElement*)elem->FirstChild();
    };
  };
  return elem;
};
//получить значение атрибута у заданного элемента
string CIniFileXML::GetAttribute(const char* _elementPath, const char* _attributeName, const char* _defValue){
  //ищем элемент
  string str = _defValue;
  TiXmlElement* elem =(TiXmlElement*)Get(_elementPath);
  if(elem == NULL){
    LOG1(WARN, "Cannot find element: %s attribute, initialized default value\n",_elementPath);
    return str;
  };  
  //ищем атрибут
  char* value = (char*)elem->Attribute(_attributeName);
  if(value == 0){
    LOG2(WARN, "Initialized default attribute value element: %s attribute name: %s\n",_elementPath,_attributeName);
    return str;
  }; 
  return value;
};

//получить описание элемента
string CIniFileXML::GetDescr(const char* _elementPath, const char* _defValue){
    return GetAttribute(_elementPath, "descr", _defValue);
};

//Получить значение атрибута max
 string CIniFileXML::GetMaxValue(const char* _elementPath, const char* _defValue){
    return GetAttribute(_elementPath, "max", _defValue);
 };

//Получить int-значение max элемента
int CIniFileXML::GetMaxValueI(const char* _elementPath, int _defValue){
  char svalue[0x100];
  sprintf( svalue, "%d", _defValue);
  return atoi(GetMaxValue(_elementPath, svalue).c_str());
};

//Получить double-значение max элемента
double CIniFileXML::GetMaxValueF(const char* _elementPath, double _defValue){
  char svalue[0x100];
  sprintf( svalue, "%f", _defValue);
  return atof(GetMaxValue(_elementPath, svalue).c_str());
};

//Получить значение атрибута min
  string CIniFileXML::GetMinValue(const char* _elementPath, const char* _defValue){
    return GetAttribute(_elementPath, "min", _defValue);
  };

//Получить int-значение min элемента
int CIniFileXML::GetMinValueI(const char* _elementPath, int _defValue){
  char svalue[0x100];
  sprintf( svalue, "%d", _defValue);
  return atoi(GetMinValue(_elementPath, svalue).c_str());
};

//Получить double-значение min элемента
double CIniFileXML::GetMinValueF(const char* _elementPath, double _defValue){
  char svalue[0x100];
  sprintf( svalue, "%f", _defValue);
  return atof(GetMinValue(_elementPath, svalue).c_str());
};


//Получить значение элемента
string CIniFileXML::GetValue(const char* _elementPath, const char* _defValue){
    return GetAttribute(_elementPath, "value", _defValue);
};

//Получить int-значение элемента
int CIniFileXML::GetValueI(const char* _elementPath, int _defValue){
  char svalue[0x100];
  sprintf( svalue, "%d", _defValue);
  return atoi(GetValue(_elementPath, svalue).c_str());
};

//Получить double-значение элемента
double CIniFileXML::GetValueF(const char* _elementPath, double _defValue){
  char svalue[0x100];
  sprintf( svalue, "%f", _defValue);
  return atof(GetValue(_elementPath, svalue).c_str());
};

//задать значение атрибута у заданного элемента
bool CIniFileXML::SetAttribute(const char* _elementPath, const char* _attributeName, const char* _attributeValue){
  //ищем элемент
  TiXmlElement* elem =(TiXmlElement*)Get(_elementPath);
  if(elem == NULL){
    LOG1(WARN, "Cannot find element: %s attribute, cannot write value\n",_elementPath);
    return false;
  };  
  //записываем значение атрибута
  elem->SetAttribute(_attributeName,_attributeValue); 
  return true;
};

//задать значение атрибута value у заданного элемента
bool CIniFileXML::SetValue(const char* _elementPath, const char* _attributeValue){
  return SetAttribute(_elementPath,"value",_attributeValue);
};

//задать значение атрибута value у заданного элемента
bool CIniFileXML::SetValueI(const char* _elementPath, int _attributeValue){
  char svalue[0x100];
//  if(GetMaxValueI(_elementPath) < _attributeValue) _attributeValue = GetMaxValueI(_elementPath);
//  else if(GetMinValueI(_elementPath) < _attributeValue) _attributeValue = GetMinValueI(_elementPath); 
  sprintf( svalue, "%d", _attributeValue);
  return SetValue(_elementPath,svalue);
};

//задать значение атрибута value у заданного элемента
bool CIniFileXML::SetValueF(const char* _elementPath, double& _attributeValue){
  char svalue[0x100];
  if(GetMaxValueF(_elementPath) < _attributeValue) _attributeValue = GetMaxValueF(_elementPath);
  else if(GetMinValueF(_elementPath) < _attributeValue) _attributeValue = GetMinValueF(_elementPath);
  sprintf( svalue, "%f", _attributeValue);
  return SetValue(_elementPath,svalue);
};

int CIniFileXML::CheckMailOnOff(const string &_number){
  TiXmlElement* elem = doc.RootElement();
  char* value = (char*)elem->Attribute("name");
  if(value == 0) return -1;
  if(strcmp(value , _number.c_str()) == 0)
    return -1;

  elem = (TiXmlElement*)elem->FirstChild();
  while(elem != NULL){
    value = (char*)elem->Attribute("number");
    if(value == 0) return -1;

    if(strcmp(value, _number.c_str()) == 0){
      return atoi(elem->Attribute("mail"));
    };
    elem = (TiXmlElement*)elem->NextSiblingElement();
  };
  return -2;
};

TiXmlElement* CIniFileXML::FindAbonent(const string &_number){
  TiXmlElement* elem = doc.RootElement();
  elem = (TiXmlElement*)elem->FirstChild();
  while(elem != NULL){
    //????????
    if(strcmp(elem->Attribute("number"),_number.c_str()) == 0)
      return elem;

    elem = (TiXmlElement*)elem->NextSiblingElement();
  };
  return 0;
};

int CIniFileXML::DeleteFirstMessage(const string &_number){
  TiXmlElement* elem = FindAbonent(_number);
  if(elem == 0) return -1;
  return DeleteFirstMessage(elem);
};

int CIniFileXML::DeleteFirstMessage(TiXmlElement* _abonent){
  if(NULL ==_abonent->FirstChild()) return -1;

  _abonent->RemoveChild(_abonent->FirstChild());
  return 0;
};

int CIniFileXML::DeleteAllHasReadMessages(const string &_number, vector<string> &_files){
  TiXmlElement* abonent = FindAbonent(_number);
  if(abonent == 0) return -1;
  TiXmlElement* child = ((TiXmlElement*)abonent->FirstChild());
//  int count_files = 0;
  while(child != NULL){
    char* has_read = (char*)child->Attribute("has_read");
    if(has_read == NULL) return -2;
    //проверяем прочитано или нет
    if(strcmp(has_read, "1") == 0){
      //сохраняем имя файла
      _files.push_back( child->Attribute("path") );
      //удаляем и смотрим далее
      TiXmlElement* tmp_child = child->NextSiblingElement();
      abonent->RemoveChild(child);
      child = tmp_child;
      continue;
    };
    child = child->NextSiblingElement();
  };
  return 0;
};

int CIniFileXML::DeleteAllMessages(const string &_number, vector<string> &_files){
  TiXmlElement* abonent = FindAbonent(_number);
  if(abonent == 0) return -1;
//  int count_files = 0;
  TiXmlElement* child = ((TiXmlElement*)abonent->FirstChild());
  while(child != NULL){
    //сохраняем имя файла
    _files.push_back(child->Attribute("path"));
    //удаляем и смотрим далее
    TiXmlElement* tmp_child = child->NextSiblingElement();
    abonent->RemoveChild(child);
    child = tmp_child;
  };
  return 0;
};


//
int CIniFileXML::AddElement(const string &_number, const string &_source, const string &_path, int _buf_size){
  TiXmlElement* elem = doc.RootElement();
  char* value = (char*)elem->Attribute("name");
  if(strcmp(value , "Mail") != 0) return -1;

  TiXmlElement* abonent = FindAbonent(_number);
  if(abonent == 0){

  }else{
    int count = 0;
    TiXmlElement* mess = (TiXmlElement*)abonent->FirstChild();
    while(mess != NULL){
      count++;
      mess = (TiXmlElement*)mess->NextSiblingElement();
    };
    if(count > _buf_size){
      DeleteFirstMessage(abonent);
    };

    TiXmlElement* new_mess = new TiXmlElement("Message");
    new_mess->SetAttribute("path",_path.c_str());
    new_mess->SetAttribute("source",_source.c_str());
    new_mess->SetAttribute("has_read", 0);
//    if(NULL == abonent->LastChild())
//      abonent->InsertAfterChild(abonent->LastChild(), *new_mess);
//    else
      abonent->InsertEndChild(*new_mess);
  };

  return 0;
};

int CIniFileXML::ResetMarks(const string &_number){
  TiXmlElement* abonent = FindAbonent(_number);
  if(NULL == abonent) return -2;

  TiXmlElement* child = ((TiXmlElement*)abonent->FirstChild());
  if(NULL == child) return 0;

  while(child != NULL){
    char* has_read = (char*)child->Attribute("has_read");
    if(has_read == NULL) return -2;
    child->SetAttribute("has_read", "0");
    child = child->NextSiblingElement();
  };
  return 0;
};

int CIniFileXML::MarkFirstMessage(const string &_number){
  TiXmlElement* abonent = FindAbonent(_number);
  if(NULL == abonent) return -2;

  TiXmlElement* child = ((TiXmlElement*)abonent->FirstChild());
  if(NULL == child) return -2;
  while(child != NULL){
    char* has_read = (char*)child->Attribute("has_read");
    if(has_read == NULL) return -2;
    //проверяем прочитано или нет
    if(strcmp(has_read, "0") == 0){
      child->SetAttribute("has_read","1");
      return 0;
    };
    child = child->NextSiblingElement();
  };
  return -1;
};

int CIniFileXML::GetFirstMessage(const string &_number, string& _source, string& _path){
  TiXmlElement* abonent = FindAbonent(_number);
  if(NULL == abonent) return -2;
  TiXmlElement* child = ((TiXmlElement*)abonent->FirstChild());
  if(NULL == child) return -2;
  _source = child->Attribute("source");
  _path = child->Attribute("path");
  return 0;
};

int CIniFileXML::GetFirstNotReadMessage(const string &_number, string& _source, string& _path){
  TiXmlElement* abonent = FindAbonent(_number);
  if(NULL == abonent) return -2;

  TiXmlElement* child = ((TiXmlElement*)abonent->FirstChild());
  if(NULL == child) return -2;
  while(child != NULL){
    char* has_read = (char*)child->Attribute("has_read");
    if(has_read == NULL) return -2;
    //проверяем прочитано или нет
    if(strcmp(has_read, "0") == 0){
      _source = child->Attribute("source");
      _path = child->Attribute("path");
      return 0;
    };
    child = child->NextSiblingElement();
  };

  return -1;
};


string CIniFileXML::CheckCase( string s) const
{
//  if ( caseInsensitive)
//    for ( string::size_type i = 0; i < s.length(); ++i)
//      s[i] = tolower(s[i]);
  return s;
}
